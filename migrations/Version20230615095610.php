<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230615095610 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE assault_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE cartrige_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE history_phrase_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE invite_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE log_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE phrase_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE refresh_tokens_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE "user_id_seq" INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE assault (id INT NOT NULL, cartrige_id INT DEFAULT NULL, updated_by_id INT DEFAULT NULL, created_by_id INT NOT NULL, global_type VARCHAR(255) NOT NULL, local_type VARCHAR(255) DEFAULT NULL, target_id VARCHAR(255) NOT NULL, settings JSON DEFAULT NULL, ts_created TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, ts_updated TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, status VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_CCE9D60C803D757C ON assault (cartrige_id)');
        $this->addSql('CREATE INDEX IDX_CCE9D60C896DBBDE ON assault (updated_by_id)');
        $this->addSql('CREATE INDEX IDX_CCE9D60CB03A8386 ON assault (created_by_id)');
        $this->addSql('CREATE TABLE cartrige (id INT NOT NULL, owner_id INT DEFAULT NULL, integration_type VARCHAR(255) NOT NULL, info JSON DEFAULT NULL, auth_data JSON NOT NULL, ts_created TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, ts_updated TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, is_up4k_warior BOOLEAN DEFAULT NULL, name VARCHAR(255) DEFAULT NULL, external_id VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_CD6B25AA7E3C61F9 ON cartrige (owner_id)');
        $this->addSql('CREATE TABLE history_phrase (id INT NOT NULL, phrase_id INT NOT NULL, before JSON DEFAULT NULL, after JSON DEFAULT NULL, ts_created TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_by VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_71C4AAFF8671F084 ON history_phrase (phrase_id)');
        $this->addSql('CREATE TABLE invite (id INT NOT NULL, owner_id INT NOT NULL, registered_by_id INT DEFAULT NULL, key UUID NOT NULL, ts_created TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_C7E210D78A90ABA9 ON invite (key)');
        $this->addSql('CREATE INDEX IDX_C7E210D77E3C61F9 ON invite (owner_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_C7E210D727E92E18 ON invite (registered_by_id)');
        $this->addSql('COMMENT ON COLUMN invite.key IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE log (id INT NOT NULL, assault_id INT NOT NULL, ts_created TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, type VARCHAR(255) NOT NULL, code VARCHAR(255) NOT NULL, context JSON DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_8F3F68C561EF04DD ON log (assault_id)');
        $this->addSql('CREATE TABLE phrase (id INT NOT NULL, text TEXT NOT NULL, sex INT NOT NULL, params JSON DEFAULT NULL, assessment JSON DEFAULT NULL, triggers JSON DEFAULT NULL, ts_created TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, ts_updated TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_by VARCHAR(255) DEFAULT NULL, tags JSON DEFAULT NULL, modules JSON DEFAULT NULL, author VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE refresh_tokens (id INT NOT NULL, refresh_token VARCHAR(128) NOT NULL, username VARCHAR(255) NOT NULL, valid TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_9BACE7E1C74F2195 ON refresh_tokens (refresh_token)');
        $this->addSql('CREATE TABLE "user" (id INT NOT NULL, name VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, ts_created TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, ts_updated TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, is_banned BOOLEAN DEFAULT NULL, roles JSON NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D6495E237E06 ON "user" (name)');
        $this->addSql('CREATE TABLE user_cartrige (user_id INT NOT NULL, cartrige_id INT NOT NULL, PRIMARY KEY(user_id, cartrige_id))');
        $this->addSql('CREATE INDEX IDX_2DE6C1AAA76ED395 ON user_cartrige (user_id)');
        $this->addSql('CREATE INDEX IDX_2DE6C1AA803D757C ON user_cartrige (cartrige_id)');
        $this->addSql('CREATE TABLE messenger_messages (id BIGSERIAL NOT NULL, body TEXT NOT NULL, headers TEXT NOT NULL, queue_name VARCHAR(190) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, available_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, delivered_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_75EA56E0FB7336F0 ON messenger_messages (queue_name)');
        $this->addSql('CREATE INDEX IDX_75EA56E0E3BD61CE ON messenger_messages (available_at)');
        $this->addSql('CREATE INDEX IDX_75EA56E016BA31DB ON messenger_messages (delivered_at)');
        $this->addSql('CREATE OR REPLACE FUNCTION notify_messenger_messages() RETURNS TRIGGER AS $$
            BEGIN
                PERFORM pg_notify(\'messenger_messages\', NEW.queue_name::text);
                RETURN NEW;
            END;
        $$ LANGUAGE plpgsql;');
        $this->addSql('DROP TRIGGER IF EXISTS notify_trigger ON messenger_messages;');
        $this->addSql('CREATE TRIGGER notify_trigger AFTER INSERT OR UPDATE ON messenger_messages FOR EACH ROW EXECUTE PROCEDURE notify_messenger_messages();');
        $this->addSql('ALTER TABLE assault ADD CONSTRAINT FK_CCE9D60C803D757C FOREIGN KEY (cartrige_id) REFERENCES cartrige (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE assault ADD CONSTRAINT FK_CCE9D60C896DBBDE FOREIGN KEY (updated_by_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE assault ADD CONSTRAINT FK_CCE9D60CB03A8386 FOREIGN KEY (created_by_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE cartrige ADD CONSTRAINT FK_CD6B25AA7E3C61F9 FOREIGN KEY (owner_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE history_phrase ADD CONSTRAINT FK_71C4AAFF8671F084 FOREIGN KEY (phrase_id) REFERENCES phrase (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE invite ADD CONSTRAINT FK_C7E210D77E3C61F9 FOREIGN KEY (owner_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE invite ADD CONSTRAINT FK_C7E210D727E92E18 FOREIGN KEY (registered_by_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE log ADD CONSTRAINT FK_8F3F68C561EF04DD FOREIGN KEY (assault_id) REFERENCES assault (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_cartrige ADD CONSTRAINT FK_2DE6C1AAA76ED395 FOREIGN KEY (user_id) REFERENCES "user" (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_cartrige ADD CONSTRAINT FK_2DE6C1AA803D757C FOREIGN KEY (cartrige_id) REFERENCES cartrige (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE assault_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE cartrige_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE history_phrase_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE invite_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE log_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE phrase_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE refresh_tokens_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE "user_id_seq" CASCADE');
        $this->addSql('ALTER TABLE assault DROP CONSTRAINT FK_CCE9D60C803D757C');
        $this->addSql('ALTER TABLE assault DROP CONSTRAINT FK_CCE9D60C896DBBDE');
        $this->addSql('ALTER TABLE assault DROP CONSTRAINT FK_CCE9D60CB03A8386');
        $this->addSql('ALTER TABLE cartrige DROP CONSTRAINT FK_CD6B25AA7E3C61F9');
        $this->addSql('ALTER TABLE history_phrase DROP CONSTRAINT FK_71C4AAFF8671F084');
        $this->addSql('ALTER TABLE invite DROP CONSTRAINT FK_C7E210D77E3C61F9');
        $this->addSql('ALTER TABLE invite DROP CONSTRAINT FK_C7E210D727E92E18');
        $this->addSql('ALTER TABLE log DROP CONSTRAINT FK_8F3F68C561EF04DD');
        $this->addSql('ALTER TABLE user_cartrige DROP CONSTRAINT FK_2DE6C1AAA76ED395');
        $this->addSql('ALTER TABLE user_cartrige DROP CONSTRAINT FK_2DE6C1AA803D757C');
        $this->addSql('DROP TABLE assault');
        $this->addSql('DROP TABLE cartrige');
        $this->addSql('DROP TABLE history_phrase');
        $this->addSql('DROP TABLE invite');
        $this->addSql('DROP TABLE log');
        $this->addSql('DROP TABLE phrase');
        $this->addSql('DROP TABLE refresh_tokens');
        $this->addSql('DROP TABLE "user"');
        $this->addSql('DROP TABLE user_cartrige');
        $this->addSql('DROP TABLE messenger_messages');
    }
}
