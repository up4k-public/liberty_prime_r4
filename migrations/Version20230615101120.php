<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230615101120 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    //Регистрация без инвайта невозможно, инвайт невозможен без юзера. Так что добавляем одного
    public function up(Schema $schema): void
    {
        //Goodnight, sweet prince :'(
        $sql = "INSERT INTO \"user\" VALUES (1, 'Asbrand Hagall', '$2y$13', '2023-04-10 16:49:21', '2023-04-10 16:49:21', false, '[]');";
        $this->addSql($sql);
        $this->addSql("INSERT INTO invite VALUES (1, 1, null, 'aaaaaaaa-bbbb-cccc-dddd-ffffffffffff', '2023-04-10 16:49:21')");
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql("DELETE FROM invite WHERE id = 1");
        $this->addSql("DELETE FROM \"user\" WHERE id = 1");

    }
}
