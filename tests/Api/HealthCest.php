<?php


namespace App\Tests\Api;

use App\Tests\ApiTester;
use Codeception\Util\HttpCode;

class HealthCest
{
    public function _before(ApiTester $I)
    {
        $I->setApiHeader();
    }

    // tests
    public function health(ApiTester $I)
    {
        $I->sendGet('/health');
        $I->seeStandardValidApiResponse();
    }
}
