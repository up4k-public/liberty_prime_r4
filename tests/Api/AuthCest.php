<?php


namespace App\Tests\Api;

use App\Tests\ApiTester;
use Codeception\Util\HttpCode;

class AuthCest
{
    public function _before(ApiTester $I)
    {
        $I->setApiHeader();
    }

    // tests
    public function checkLogin(ApiTester $I)
    {
        $I->sendPost('/api/auth/get-token', [
            'username' => $_ENV['TESTER_LOGIN'],
            'password' => $_ENV['TESTER_PASSWORD']
        ]);
        $I->seeStandardValidApiResponse();
        $I->seeResponseMatchesJsonType([
            'data' => ['token' => 'string', 'refresh_token' => 'string', 'refresh_token_expiration' => 'integer'],
        ]);
    }

    public function invalidLoginOrPassword(ApiTester $I)
    {
        $I->sendPost('/api/auth/get-token', [
            'username' => 'invalid',
            'password' => 'invalid'
        ]);
        $I->seeStandardErrorApiResponse();
        $I->seeErrorCode(0);
    }
}
