<?php


namespace App\Tests\Api;

use App\Tests\ApiTester;

class ServiceCest
{
    public function _before(ApiTester $I)
    {
        $I->setApiHeader();
        $I->amBearerAuthenticated($I->getToken());
    }

    // tests
    public function integrationList(ApiTester $I)
    {
        $I->sendPost('/api/service-info/integration-list', [
            'limit' => 20,
            'offset' => 0
        ]);
        $I->seeStandardValidApiResponse();
    }

    public function moduleList(ApiTester $I)
    {
        $I->sendPost('/api/service-info/module-list', [
            'limit' => 20,
            'offset' => 0
        ]);
        $I->seeStandardValidApiResponse();
    }

    public function scenariosList(ApiTester $I)
    {
        $I->sendPost('/api/service-info/scenarios-list', [
            'limit' => 20,
            'offset' => 0
        ]);
        $I->seeStandardValidApiResponse();
    }

}
