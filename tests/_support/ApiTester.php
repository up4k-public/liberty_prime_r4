<?php

declare(strict_types=1);

namespace App\Tests;

use Codeception\Util\HttpCode;

/**
 * Inherited Methods
 * @method void wantTo($text)
 * @method void wantToTest($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method void pause($vars = [])
 *
 * @SuppressWarnings(PHPMD)
*/
class ApiTester extends \Codeception\Actor
{
    use _generated\ApiTesterActions;

    /**
     * Define custom actions here
     */

    public function setApiHeader():void
    {
        $this->haveHttpHeader('Content-Type', 'application/json');
        $this->haveHttpHeader('User-Agent', 'codeception');
    }

    public function seeStandardValidApiResponse()
    {
        $this->seeResponseCodeIs(HttpCode::OK); // 200
        $this->seeResponseIsJson();
        $this->seeResponseMatchesJsonType([
            'success' => 'boolean',
            'data' => 'array',
            'error' => 'null',
            'errors' => 'null'
        ]);
        $this->seeResponseContainsJson([
            'success' => true
        ]);
    }

    public function seeStandardErrorApiResponse()
    {
        $this->seeResponseCodeIs(HttpCode::INTERNAL_SERVER_ERROR); // 500
        $this->seeResponseIsJson();
        $this->seeResponseMatchesJsonType([
            'success' => 'boolean',
            'data' => 'null',
            'error' => ['code' => 'integer', 'message' => 'string'],
            'errors' => 'array'
        ]);
        $this->seeResponseContainsJson([
            'success' => false
        ]);
    }

    public function seeErrorCode(int $code)
    {
        $this->seeResponseContainsJson([
            'error' => ['code' => $code]
        ]);
    }


}
