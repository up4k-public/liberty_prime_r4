<?php

declare(strict_types=1);

namespace App\Tests\Helper;

// here you can define custom actions
// all public methods declared in helper class will be available in $I

class Api extends \Codeception\Module
{
    private ?string $token = null;

    public function getToken(): string
    {
        if (!empty($this->token)) {
            return $this->token;
        }

        $client = new \GuzzleHttp\Client([
            'headers' => [
                'Content-Type' => 'application/json',
                'User-Agent' => 'codeception'
            ]
        ]);
        $res = ($client->post($_ENV['TEST_API_URL'] . 'api/auth/get-token',
            ['body' => json_encode(
                [
                    "username" => $_ENV['TESTER_LOGIN'],
                    "password" => $_ENV['TESTER_PASSWORD'],
                ]
            )])
        )->getBody()->getContents();
        $res = json_decode($res, true);
        $this->token = $res['data']['token'];
        return $this->token;
    }
}
