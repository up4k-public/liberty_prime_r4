<?php


namespace App\Core;

/**
 * Class Message
 * @package App\Core
 */
class Message
{
    private ?string $text;
    private ?string $author_id;
    private ?Author $author = null;
    private ?string $external_id;
    private ?array $external_ids = [];
    private ?int $internal_id;
    private ?bool $is_my = false;
    private ?array $attachments = [];
    private ?string $datetime;
    private ?bool $has_like = false;
    private ?string $target_external_id;
    private ?int $target_internal_id;
    private ?string $env_type;
    private ?array $additional_info;
    private ?array $actions = [];
    private ?bool $directed_to_me = null;
    private ?bool $is_service = false;
    private ?bool $can_forward = true;
    private ?array $service_info = [];

    /**
     * @return string|null
     */
    public function getText(): ?string
    {
        return $this->text;
    }

    /**
     * @param string|null $text
     * @return Message
     */
    public function setText(?string $text): Message
    {
        $this->text = $text;
        return $this;
    }

    public function addText(?string $new_text): Message
    {
        if (empty($new_text)) {
            return $this;
        }

        if (empty($this->text)) {
            $this->text = $new_text;
        } else {
            $this->text = $this->text . "\n" . $new_text;
        }
        return $this;
    }

    /**
     * @return string|null
     */
    public function getEnvType(): ?string
    {
        return $this->env_type;
    }

    /**
     * @param string|null $env_type
     * @return Message
     */
    public function setEnvType(?string $env_type): Message
    {
        $this->env_type = $env_type;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getAdditionalInfo(): ?array
    {
        return $this->additional_info;
    }

    /**
     * @param array|null $additional_info
     * @return Message
     */
    public function setAdditionalInfo(?array $additional_info): Message
    {
        $this->additional_info = $additional_info;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAuthorId(): ?string
    {
        return $this->author_id;
    }

    /**
     * @param string|null $author_id
     * @return Message
     */
    public function setAuthorId(?string $author_id): Message
    {
        $this->author_id = $author_id;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getIsMy(): ?bool
    {
        return $this->is_my;
    }

    /**
     * @param bool|null $is_my
     * @return Message
     */
    public function setIsMy(?bool $is_my): Message
    {
        $this->is_my = $is_my;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getAttachments(): ?array
    {
        return $this->attachments;
    }

    /**
     * @param array|null $attachments
     * @return Message
     */
    public function setAttachments(?array $attachments): Message
    {
        $this->attachments = $attachments;
        return $this;
    }

    /**
     * @param array|null $new_attachments
     * @return $this
     */
    public function addAttachments(?array $new_attachments): Message
    {
        if (empty($new_attachments)) {
            return $this;
        }
        $this->attachments = array_merge($this->getAttachments(), $new_attachments);
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDatetime(): ?string
    {
        return $this->datetime;
    }

    /**
     * @param string|null $datetime
     * @return Message
     */
    public function setDatetime(?string $datetime): Message
    {
        $this->datetime = $datetime;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getHasLike(): ?bool
    {
        return $this->has_like;
    }

    /**
     * @param bool|null $has_like
     * @return Message
     */
    public function setHasLike(?bool $has_like): Message
    {
        $this->has_like = $has_like;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getExternalId(): ?string
    {
        return $this->external_id;
    }

    /**
     * @param string|null $external_id
     * @return Message
     */
    public function setExternalId(?string $external_id): Message
    {
        $this->external_id = $external_id;
        $this->addExternalId($external_id);
        return $this;
    }

    /**
     * @return int|null
     */
    public function getInternalId(): ?int
    {
        return $this->internal_id;
    }

    /**
     * @param int|null $internal_id
     * @return Message
     */
    public function setInternalId(?int $internal_id): Message
    {
        $this->internal_id = $internal_id;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTargetExternalId(): ?string
    {
        return $this->target_external_id;
    }

    /**
     * @param string|null $target_external_id
     * @return Message
     */
    public function setTargetExternalId(?string $target_external_id): Message
    {
        $this->target_external_id = $target_external_id;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getTargetInternalId(): ?int
    {
        return $this->target_internal_id;
    }

    /**
     * @param int|null $target_internal_id
     * @return Message
     */
    public function setTargetInternalId(?int $target_internal_id): Message
    {
        $this->target_internal_id = $target_internal_id;
        return $this;
    }

    /**
     * @return Author|null
     */
    public function getAuthor(): ?Author
    {
        return $this->author;
    }

    /**
     * @param Author|null $author
     * @return Message
     */
    public function setAuthor(?Author $author): Message
    {
        $this->author = $author;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getActions(): ?array
    {
        return $this->actions;
    }

    /**
     * @param array|null $actions
     * @return Message
     */
    public function setActions(?array $actions): Message
    {
        $this->actions = $actions;
        return $this;
    }

    /**
     * @param array|null $new_actions
     * @return Message
     */
    public function addActions(?array $new_actions): Message
    {
        if (empty($new_actions)) {
            return $this;
        }
        $this->actions = array_merge($this->getAttachments(), $new_actions);
        return $this;
    }

    /**
     * @return array|null
     */
    public function getExternalIds(): ?array
    {
        return $this->external_ids;
    }

    /**
     * @param array|null $external_ids
     * @return Message
     */
    public function setExternalIds(?array $external_ids): Message
    {
        $this->external_ids = $external_ids;
        return $this;
    }

    public function addExternalId(?string $id)
    {
        $this->external_ids = array_unique(array_merge($this->external_ids, [$id]));
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getDirectedToMe(): ?bool
    {
        return $this->directed_to_me;
    }

    /**
     * @param bool|null $directed_to_me
     * @return Message
     */
    public function setDirectedToMe(?bool $directed_to_me): Message
    {
        $this->directed_to_me = $directed_to_me;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getIsService(): ?bool
    {
        return $this->is_service;
    }

    /**
     * @param bool|null $is_service
     * @return Message
     */
    public function setIsService(?bool $is_service): Message
    {
        $this->is_service = $is_service;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getServiceInfo(): ?array
    {
        return $this->service_info;
    }

    /**
     * @param array|null $service_info
     * @return Message
     */
    public function setServiceInfo(?array $service_info): Message
    {
        $this->service_info = $service_info;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getCanForward(): ?bool
    {
        return $this->can_forward;
    }

    /**
     * @param bool|null $can_forward
     * @return Message
     */
    public function setCanForward(?bool $can_forward): Message
    {
        $this->can_forward = $can_forward;
        return $this;
    }

}