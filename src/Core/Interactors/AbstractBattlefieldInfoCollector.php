<?php


namespace App\Core\Interactors;


use App\Core\BattleField;
use App\History\AssaultLog\Events\BaseAssaultLogEvent;
use App\History\AssaultLog\Events\ScanBattlefieldEvent;
use App\History\AssaultLog\EventsContext\ScanBattlefieldContext;

/**
 * Собирает информацию о месте, где ведётся активность
 * Class AbstractBattlefieldInfoCollector
 * @package App\Core\Interactors
 */
abstract class AbstractBattlefieldInfoCollector extends AbstractInteractors
{
    function run($data = null): BattleField
    {
        return $this->getInfo($data);
    }

    /**
     * @param string $target_id
     * @return BattleField
     */
    abstract public function getInfo(string $target_id): BattleField;

    /**
     * @param null $result
     * @param null $input
     * @return ScanBattlefieldEvent|null
     */
    public function generateEvent($result = null, $input = null): ?BaseAssaultLogEvent
    {
        return (new ScanBattlefieldEvent($this->getAssault(), new ScanBattlefieldContext($result, $input)));
    }
}