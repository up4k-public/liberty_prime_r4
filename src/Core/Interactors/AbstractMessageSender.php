<?php


namespace App\Core\Interactors;

use App\Core\MessageManager;
use App\Core\ResponseMessage;
use App\History\AssaultLog\Events\BaseAssaultLogEvent;
use App\History\AssaultLog\Events\ChoosingAnswerEvent;
use App\History\AssaultLog\Events\PostMessageEvent;
use App\History\AssaultLog\EventsContext\ChoosingAnswerContext;
use App\History\AssaultLog\EventsContext\PostMessageEventContext;

/**
 * Class MessageSender
 * @package App\Core
 */
abstract class AbstractMessageSender extends MessageManager
{
    function run($data = null): ?int
    {
        return $this->postMessage($data);
    }

    abstract public function postMessage(ResponseMessage $message): ?int;

    public function generateEvent($result = null, $input = null): ?BaseAssaultLogEvent
    {
        return (new PostMessageEvent($this->getAssault(), new PostMessageEventContext($result, $input)));
    }
}