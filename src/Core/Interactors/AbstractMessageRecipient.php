<?php


namespace App\Core\Interactors;

use App\Core\MessageManager;
use App\History\AssaultLog\Events\BaseAssaultLogEvent;
use App\History\AssaultLog\Events\GetMessageEvent;
use App\History\AssaultLog\EventsContext\GetMessageContext;

/**
 * Class MessageRecipient
 * @package App\Core
 */
abstract class AbstractMessageRecipient extends MessageManager
{
    function run($data = null): array
    {
        return $this->getMessage();
    }

    /**
     * @return array
     */
    abstract public function getMessage(): array;

    public function generateEvent($result = null, $input = null): ?BaseAssaultLogEvent
    {
        return (new GetMessageEvent($this->getAssault(), new GetMessageContext($result, $this->getTargetId())));
    }
}