<?php


namespace App\Core\Interactors;


use App\Core\Author;
use App\History\AssaultLog\Events\BaseAssaultLogEvent;
use App\History\AssaultLog\Events\ScanUserEvent;
use App\History\AssaultLog\EventsContext\ScanUserContext;
use App\History\EventLogger;

/**
 * Собриает информацию о юзере
 * Class AbstractUserinfoCollector
 * @package App\Core\Interactors
 */
abstract class AbstractUserinfoCollector extends AbstractInteractors
{
    function run($data = null): Author
    {
        return $this->getInfo($data);
    }

    /**
     * Если user_id=null, то получаем данные о своём текущем
     * @param string|null $user_id
     * @return Author
     */
    abstract public function getInfo(?string $user_id = null): Author;

    public function generateEvent($result = null, $input = null): ?BaseAssaultLogEvent
    {
        return (new ScanUserEvent($this->getAssault(), new ScanUserContext($result, $input)));
    }
}