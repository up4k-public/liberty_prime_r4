<?php


namespace App\Core\Interactors;

use App\History\AssaultLog\Events\BaseAssaultLogEvent;

/**
 * Class AbstractLiker
 * @package App\Core\Interactors
 */
abstract class AbstractLiker extends AbstractInteractors
{
    function run($data = null)
    {
        return $this->setLike($data);
    }

    abstract function setLike($message_id);

    public function generateEvent($result = null, $input = null): ?BaseAssaultLogEvent
    {
        return null;
    }
}