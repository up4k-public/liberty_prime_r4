<?php


namespace App\Core\Interactors;

use App\Core\BaseAdapter;
use App\Entity\Assault;
use App\History\AssaultLog\Events\BaseAssaultLogEvent;
use App\Integrations\Api;
use App\Repository\LogRepository;
use Exception;

/**
 * Class AbstractInteractors
 * @package App\Core\Interactors
 */
abstract class AbstractInteractors
{
    private Api $api;
    private ?BaseAdapter $adapter = null;
    private Assault $assault;
    private LogRepository $eventLogger;

    /**
     * Bot constructor.
     * @param LogRepository $eventLogger
     * @param Api $api
     * @param BaseAdapter|null $adapter
     */
    public function __construct(LogRepository $eventLogger, Api $api, BaseAdapter $adapter = null)
    {
        $this->eventLogger = $eventLogger;
        $this->api = $api;
        $this->adapter = $adapter;
    }

    abstract function run($data = null);


    /**
     * @param null $data
     * @return mixed
     * @throws Exception
     */
    public function runAndSaveLog($data = null)
    {
        $result = $this->run($data);
        $this->eventLogger->saveByEvent($this->generateEvent($result, $data), $this->getAssault());
        return $result;
    }

    /**
     * @return Api
     */
    public function getApi()
    {
        return $this->api;
    }

    /**
     * @return BaseAdapter|null
     */
    public function getAdapter(): ?BaseAdapter
    {
        return $this->adapter;
    }

    /**
     * @return Assault
     */
    public function getAssault(): Assault
    {
        return $this->assault;
    }

    /**
     * @param Assault $assault
     * @return AbstractInteractors
     */
    public function setAssault(Assault $assault): AbstractInteractors
    {
        $this->assault = $assault;
        return $this;
    }

    /**
     * @param null $result
     * @param null $input
     * @return BaseAssaultLogEvent|null
     */
     abstract public function generateEvent($result = null, $input = null):?BaseAssaultLogEvent;

}