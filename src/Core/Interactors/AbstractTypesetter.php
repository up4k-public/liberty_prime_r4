<?php


namespace App\Core\Interactors;

use App\Core\MessageManager;
use App\History\AssaultLog\Events\BaseAssaultLogEvent;
use App\History\AssaultLog\Events\TypesetEvent;
use App\History\AssaultLog\EventsContext\TypesetContext;

/**
 * Class AbstractTypesetter
 * @package App\Core\Interactors
 */
abstract class AbstractTypesetter extends MessageManager
{
    function run($data = null): bool
    {
        return $this->setActivity($data);
    }

    /**
     * Передает статус активности набора юзером текста
     * @param int $duration продолжительность в секундах
     * @return bool
     */
    abstract public function setActivity(int $duration): bool;

    public function generateEvent($result = null, $input = null): ?BaseAssaultLogEvent
    {
        return (new TypesetEvent($this->getAssault(), new TypesetContext($result, $input, $this->getTargetId())));
    }
}