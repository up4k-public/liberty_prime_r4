<?php


namespace App\Core\Interactors;

use App\Core\MessageManager;
use App\History\AssaultLog\Events\BaseAssaultLogEvent;
use App\History\AssaultLog\Events\MakeAsReadEvent;
use App\History\AssaultLog\EventsContext\MakeAsReadContext;

/**
 * Class AbstractReadMarker
 * Класс отвечающий за пометку сообщений, как прочитанные
 * @package App\Core\Interactors
 */
abstract class AbstractReadMarker extends MessageManager
{
    function run($data = null): bool
    {
        return $this->markAsRead($data);
    }

    /**
     * Помечаем, как прочитанное сообещния в чате\конфернции по идентифакатору или по списку сообщений
     * @param array|null $ids
     * @return bool
     */
    abstract function markAsRead(?array $ids = null): bool;

    public function generateEvent($result = null, $input = null): ?BaseAssaultLogEvent
    {
        return (new MakeAsReadEvent($this->getAssault(), new MakeAsReadContext($result, $input)));
    }
}