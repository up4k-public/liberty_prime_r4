<?php


namespace App\Core;


use DateTime;
use Exception;

/**
 * Class Author
 * @package App\Core
 */
class Author extends AbstractInterlocutor
{
    const SEX_MAN = true;
    const SEX_WOMEN = false;

    private ?int $internal_id = null;
    private ?string $external_id = null;
    private ?string $first_name = null;
    private ?string $middle_name = null;
    private ?string $last_name = null;
    private ?string $self_nickname = null;
    private ?string $domain = null;
    private ?string $internal_nick_name = null;
    private ?string $bdate = null;
    private ?array $all_names = [];
    private ?bool $sex = null;
    private ?string $city = null;
    private ?string $country = null;
    private ?bool $is_friend_current_avatar = null;
    private ?bool $is_up4k_warrior = null;
    private ?bool $is_bot_active = null;
    private ?bool $is_my = null;
    private ?array $interests = [];
    private ?array $additional_info = [];
    private ?string $env_type = null;
    private ?NameCases $first_name_cases = null;
    private ?bool $is_bot = false;

    /**
     * @return int|null
     */
    public function getInternalId(): ?int
    {
        return $this->internal_id;
    }

    /**
     * @param int|null $internal_id
     * @return Author
     */
    public function setInternalId(?int $internal_id): Author
    {
        $this->internal_id = $internal_id;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getExternalId(): ?string
    {
        return $this->external_id;
    }

    /**
     * @param string|null $external_id
     * @return Author
     */
    public function setExternalId(?string $external_id): Author
    {
        $this->external_id = $external_id;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getFirstName(): ?string
    {
        return $this->first_name;
    }

    /**
     * @param string|null $first_name
     * @return Author
     */
    public function setFirstName(?string $first_name): Author
    {
        $this->first_name = $first_name;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLastName(): ?string
    {
        return $this->last_name;
    }

    /**
     * @param string|null $last_name
     * @return Author
     */
    public function setLastName(?string $last_name): Author
    {
        $this->last_name = $last_name;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getAllNames(): ?array
    {
        return $this->all_names;
    }

    /**
     * @return string
     */
    public function getRandomNameFromAlterList(): string
    {
        $list = $this->getAllNames();

        if (($key = array_search($this->getFirstName(), $list)) !== false) {
            unset($list[$key]);
        }

        return array_values($list)[rand(0, count($list) - 1)];
    }

    /**
     * @param array|null $all_names
     * @return Author
     */
    public function setAllNames(?array $all_names): Author
    {
        $res = [];
        foreach ($all_names as $name) {
            if (!empty($name)) {
                $res[] = trim($name);
            }
        }
        $this->all_names = $res;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getSex(): ?bool
    {
        return $this->sex;
    }

    /**
     * @return bool
     */
    public function isMan(): bool
    {
        return ($this->sex === self::SEX_MAN);
    }

    /**
     * @return bool
     */
    public function isWomen(): bool
    {
        return ($this->sex === self::SEX_WOMEN);
    }

    /**
     * @param bool|null $sex
     * @return Author
     */
    public function setSex(?bool $sex): Author
    {
        $this->sex = $sex;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCity(): ?string
    {
        return $this->city;
    }

    /**
     * @param string|null $city
     * @return Author
     */
    public function setCity(?string $city): Author
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCountry(): ?string
    {
        return $this->country;
    }

    /**
     * @param string|null $country
     * @return Author
     */
    public function setCountry(?string $country): Author
    {
        $this->country = $country;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getIsFriendCurrentAvatar(): ?bool
    {
        return $this->is_friend_current_avatar;
    }

    /**
     * @param bool|null $is_friend_current_avatar
     * @return Author
     */
    public function setIsFriendCurrentAvatar(?bool $is_friend_current_avatar): Author
    {
        $this->is_friend_current_avatar = $is_friend_current_avatar;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getIsUp4kWarrior(): ?bool
    {
        return $this->is_up4k_warrior;
    }

    /**
     * @param bool|null $is_up4k_warrior
     * @return Author
     */
    public function setIsUp4kWarrior(?bool $is_up4k_warrior): Author
    {
        $this->is_up4k_warrior = $is_up4k_warrior;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getInterests(): ?array
    {
        return $this->interests;
    }

    /**
     * @param array|null $interests
     * @return Author
     */
    public function setInterests(?array $interests): Author
    {
        $this->interests = $interests;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getIsBotActive(): ?bool
    {
        return $this->is_bot_active;
    }

    /**
     * @param bool|null $is_bot_active
     * @return Author
     */
    public function setIsBotActive(?bool $is_bot_active): Author
    {
        $this->is_bot_active = $is_bot_active;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getIsMy(): ?bool
    {
        return $this->is_my;
    }

    /**
     * @param bool|null $is_my
     * @return Author
     */
    public function setIsMy(?bool $is_my): Author
    {
        $this->is_my = $is_my;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getAdditionalInfo(): ?array
    {
        return $this->additional_info;
    }

    /**
     * @param array|null $additional_info
     * @return Author
     */
    public function setAdditionalInfo(?array $additional_info): Author
    {
        $this->additional_info = $additional_info;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getEnvType(): ?string
    {
        return $this->env_type;
    }

    /**
     * @param string|null $env_type
     * @return Author
     */
    public function setEnvType(?string $env_type): Author
    {
        $this->env_type = $env_type;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getBdate(): ?string
    {
        return $this->bdate;
    }

    /**
     * @param string|null $bdate
     * @return Author
     */
    public function setBdate(?string $bdate): Author
    {
        $this->bdate = $bdate;
        return $this;
    }

    /**
     * @return NameCases|null
     */
    public function getFirstNameCases(): ?NameCases
    {
        return $this->first_name_cases;
    }

    /**
     * @param NameCases|null $first_name_cases
     * @return Author
     */
    public function setFirstNameCases(?NameCases $first_name_cases): Author
    {
        $this->first_name_cases = $first_name_cases;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getIsBot(): ?bool
    {
        return $this->is_bot;
    }

    /**
     * @param bool|null $is_bot
     * @return Author
     */
    public function setIsBot(?bool $is_bot): Author
    {
        $this->is_bot = $is_bot;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getInternalNickName(): ?string
    {
        return $this->internal_nick_name;
    }

    /**
     * @param string|null $internal_nick_name
     * @return Author
     */
    public function setInternalNickName(?string $internal_nick_name): Author
    {
        $this->internal_nick_name = $internal_nick_name;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getMiddleName(): ?string
    {
        return $this->middle_name;
    }

    /**
     * @param string|null $middle_name
     * @return Author
     */
    public function setMiddleName(?string $middle_name): Author
    {
        $this->middle_name = $middle_name;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSelfNickname(): ?string
    {
        return $this->self_nickname;
    }

    /**
     * @param string|null $self_nickname
     * @return Author
     */
    public function setSelfNickname(?string $self_nickname): Author
    {
        $this->self_nickname = $self_nickname;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDomain(): ?string
    {
        return $this->domain;
    }

    /**
     * @param string|null $domain
     * @return Author
     */
    public function setDomain(?string $domain): Author
    {
        $this->domain = $domain;
        return $this;
    }

    /**
     * Высчитываем возраст
     * @return int|null
     * @throws Exception
     */
    public function getAge(): ?int
    {
        if (empty($this->bdate)) {
            return null;
        }
        $bdate = new DateTime($this->bdate);
        return $bdate->diff(new DateTime())->y;
    }


    public function getFullName(): string
    {
        $name = '';
        if (!empty($this->first_name)) {
            $name .= $this->first_name;
        }
        if (!empty($this->middle_name)) {
            $name .= $this->middle_name;
        }
        if (!empty($this->last_name)) {
            $name .= $this->last_name;
        }
        return $name;
    }

}