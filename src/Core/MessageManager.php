<?php


namespace App\Core;

use App\Core\Interactors\AbstractInteractors;

/**
 * Class MessageManager
 * @package App\Core
 */
abstract class MessageManager extends AbstractInteractors
{
    /**
     * @var string|null
     */
    private ?string $target_id = null;

    /**
     * @return string|null
     */
    public function getTargetId(): ?string
    {
        return $this->target_id;
    }

    /**
     * @param string|null $target_id
     * @return MessageManager
     */
    public function setTargetId(?string $target_id): MessageManager
    {
        $this->target_id = $target_id;
        return $this;
    }

}