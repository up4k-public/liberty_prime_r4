<?php


namespace App\Core;

/**
 * Class BaseAdapter
 * @package App\Core
 */
abstract class BaseAdapter
{
   abstract function convert($data);
}