<?php


namespace App\Core;

/**
 * Class BattleField
 * Класс, хранящий данные о "поле боя"
 * @package App\Core
 */
class BattleField
{
    private ?string $integration_type; //vk_api
    private ?string $type; //сhat
    private ?string $name; //Ракочат
    private ?bool $i_am_admin = false;

    private ?array $admins;
    private ?string $loyalty_level;

    /**
     * @return string|null
     */
    public function getIntegrationType(): ?string
    {
        return $this->integration_type;
    }

    /**
     * @param string|null $integration_type
     * @return BattleField
     */
    public function setIntegrationType(?string $integration_type): BattleField
    {
        $this->integration_type = $integration_type;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string|null $type
     * @return BattleField
     */
    public function setType(?string $type): BattleField
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     * @return BattleField
     */
    public function setName(?string $name): BattleField
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getAdmins(): ?array
    {
        return $this->admins;
    }

    /**
     * @param array|null $admins
     * @return BattleField
     */
    public function setAdmins(?array $admins): BattleField
    {
        $this->admins = $admins;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLoyaltyLevel(): ?string
    {
        return $this->loyalty_level;
    }

    /**
     * @param string|null $loyalty_level
     * @return BattleField
     */
    public function setLoyaltyLevel(?string $loyalty_level): BattleField
    {
        $this->loyalty_level = $loyalty_level;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getIAmAdmin(): ?bool
    {
        return $this->i_am_admin;
    }

    /**
     * @param bool|null $i_am_admin
     * @return BattleField
     */
    public function setIAmAdmin(?bool $i_am_admin): BattleField
    {
        $this->i_am_admin = $i_am_admin;
        return $this;
    }

}