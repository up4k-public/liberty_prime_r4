<?php


namespace App\Core\DialogAction;

/**
 * Class ChatPinMessage
 * @package App\Core\DialogAction
 */
class ChatPinMessage extends BaseDialogAction
{
    const TRIGGER = 'complex:chat_pin_message';

    public ?string $text = null;

    /**
     * @return string|null
     */
    public function getText(): ?string
    {
        return $this->text;
    }

    /**
     * @param string|null $text
     * @return ChatPinMessage
     */
    public function setText(?string $text): ChatPinMessage
    {
        $this->text = $text;
        return $this;
    }

    public function getTrigger(): string
    {
        return self::TRIGGER;
    }

}