<?php


namespace App\Core\DialogAction;

/**
 * Class DialogAction
 * @package App\Core
 */
abstract class BaseDialogAction
{
    const TRIGGER = 'complex:chat_action';

    public ?string $processing_object_id = null;

    /**
     * @return string|null
     */
    public function getProcessingObjectId(): ?string
    {
        return $this->processing_object_id;
    }

    /**
     * @param string|null $processing_object_id
     * @return BaseDialogAction
     */
    public function setProcessingObjectId(?string $processing_object_id): BaseDialogAction
    {
        $this->processing_object_id = $processing_object_id;
        return $this;
    }

    /**
     * @return string
     */
    abstract public function getTrigger(): string;

}