<?php


namespace App\Core\DialogAction;


/**
 * Class ChatLeaveUser
 * @package App\Core\DialogAction
 */
class ChatLeaveUser extends ChatInviteUser
{
    const TRIGGER = 'complex:chat_leave_user';

    public function getTrigger(): string
    {
        return self::TRIGGER;
    }
}