<?php


namespace App\Core\DialogAction;

/**
 * Class ChatUnpinMessage
 * @package App\Core\DialogAction
 */
class ChatUnpinMessage extends BaseDialogAction
{
    const TRIGGER = 'complex:chat_unpin_message';

    public function getTrigger(): string
    {
        return self::TRIGGER;
    }
}