<?php


namespace App\Core\DialogAction;

use App\Core\Author;

/**
 * Class ChatKikUser
 * @package App\Core\DialogAction
 */
class ChatKikUser extends BaseDialogAction
{
    const TRIGGER = 'complex:chat_kik_user';

    public ?Author $processing_user;

    /**
     * @return Author|null
     */
    public function getProcessingUser(): ?Author
    {
        return $this->processing_user;
    }

    /**
     * @param Author|null $processing_user
     * @return ChatKikUser
     */
    public function setProcessingUser(?Author $processing_user): self
    {
        $this->processing_user = $processing_user;
        return $this;
    }

    public function getTrigger(): string
    {
        return self::TRIGGER;
    }
}