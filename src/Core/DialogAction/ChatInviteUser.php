<?php


namespace App\Core\DialogAction;

use App\Core\Author;

/**
 * Class ChatInviteUser
 * @package App\Core\DialogAction
 */
class ChatInviteUser extends BaseDialogAction
{
    const TRIGGER = 'complex:chat_invite_user';

    public ?Author $processing_user;

    /**
     * @return Author|null
     */
    public function getProcessingUser(): ?Author
    {
        return $this->processing_user;
    }

    /**
     * @param Author|null $processing_user
     * @return ChatInviteUser
     */
    public function setProcessingUser(?Author $processing_user): ChatInviteUser
    {
        $this->processing_user = $processing_user;
        return $this;
    }

    public function getTrigger(): string
    {
        return self::TRIGGER;
    }

}