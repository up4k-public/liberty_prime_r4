<?php


namespace App\Core\DialogAction;

/**
 * Class ChatPhotoUpdate
 * @package App\Core\DialogAction
 */
class ChatPhotoUpdate extends BaseDialogAction
{
    const TRIGGER = 'complex:chat_photo_update';

    public function getTrigger(): string
    {
        return self::TRIGGER;
    }
}