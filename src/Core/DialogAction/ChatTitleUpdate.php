<?php


namespace App\Core\DialogAction;

/**
 * Class ChatTitleUpdate
 * @package App\Core\DialogAction
 */
class ChatTitleUpdate extends BaseDialogAction
{
    const TRIGGER = 'complex:chat_title_update';

    public ?string $text = null;

    /**
     * @return string|null
     */
    public function getText(): ?string
    {
        return $this->text;
    }

    /**
     * @param string|null $text
     * @return ChatTitleUpdate
     */
    public function setText(?string $text): ChatTitleUpdate
    {
        $this->text = $text;
        return $this;
    }

    public function getTrigger(): string
    {
        return self::TRIGGER;
    }

}