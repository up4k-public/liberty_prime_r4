<?php


namespace App\Core\DialogAction;

/**
 * Class ChatReturnUser
 * @package App\Core\DialogAction
 */
class ChatReturnUser extends ChatInviteUser
{
    const TRIGGER = 'complex:chat_return_user';

    public function getTrigger(): string
    {
        return self::TRIGGER;
    }
}