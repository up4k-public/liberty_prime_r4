<?php


namespace App\Core;

use App\Entity\Phrase;

/**
 * Class ResponseMessage
 * @package App\Core
 */
class ResponseMessage
{
    private ?string $text = null;
    private ?bool $self_sex = null;
    private ?array $request_message_external_ids = [];
    private ?array $forward_message_external_ids = [];
    private ?Phrase $phrase = null;
    private ?array $attachment = [];
    private ?AbstractInterlocutor $interlocutor = null;
    private bool $ignore = false;

    /**
     * @return string|null
     */
    public function getText(): ?string
    {
        return $this->text;
    }

    /**
     * @param string|null $text
     * @return ResponseMessage
     */
    public function setText(?string $text): ResponseMessage
    {
        $this->text = $text;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getRequestMessageExternalIds(): ?array
    {
        return $this->request_message_external_ids;
    }

    /**
     * @param array|null $request_message_external_ids
     * @return ResponseMessage
     */
    public function setRequestMessageExternalIds(?array $request_message_external_ids): ResponseMessage
    {
        $this->request_message_external_ids = $request_message_external_ids;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getForwardMessageExternalIds(): ?array
    {
        return $this->forward_message_external_ids;
    }

    /**
     * @param array|null $forward_message_external_ids
     * @return ResponseMessage
     */
    public function setForwardMessageExternalIds(?array $forward_message_external_ids): ResponseMessage
    {
        $this->forward_message_external_ids = $forward_message_external_ids;
        return $this;
    }

    /**
     * @return Phrase|null
     */
    public function getPhrase(): ?Phrase
    {
        return $this->phrase;
    }

    /**
     * @param Phrase|null $phrase
     * @return ResponseMessage
     */
    public function setPhrase(?Phrase $phrase): ResponseMessage
    {
        $this->phrase = $phrase;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getAttachment(): ?array
    {
        return $this->attachment;
    }

    /**
     * @param array|null $attachment
     * @return ResponseMessage
     */
    public function setAttachment(?array $attachment): ResponseMessage
    {
        $this->attachment = $attachment;
        return $this;
    }

    /**
     * @return AbstractInterlocutor|null
     */
    public function getInterlocutor(): ?AbstractInterlocutor
    {
        return $this->interlocutor;
    }

    /**
     * @param AbstractInterlocutor|null $interlocutor
     * @return ResponseMessage
     */
    public function setInterlocutor(?AbstractInterlocutor $interlocutor): ResponseMessage
    {
        $this->interlocutor = $interlocutor;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getSelfSex(): ?bool
    {
        return $this->self_sex;
    }

    /**
     * @param bool|null $self_sex
     * @return ResponseMessage
     */
    public function setSelfSex(?bool $self_sex): ResponseMessage
    {
        $this->self_sex = $self_sex;
        return $this;
    }

    /**
     * @return bool
     */
    public function getIgnore(): bool
    {
        return $this->ignore;
    }

    /**
     * @param bool $ignore
     * @return ResponseMessage
     */
    public function setIgnore(bool $ignore): ResponseMessage
    {
        $this->ignore = $ignore;
        return $this;
    }

}