<?php


namespace App\Core;

/**
 * Class NameCases
 * @package App\Core
 */
class NameCases
{
    private ?string $nominativ;
    private ?string $genitive;
    private ?string $dative;
    private ?string $accusative;
    private ?string $ablative;
    private ?string $prepositional;
    private ?string $vocative;

    /**
     * @return string|null
     */
    public function getNominativ(): ?string
    {
        return $this->nominativ;
    }

    /**
     * @param string|null $nominativ
     * @return NameCases
     */
    public function setNominativ(?string $nominativ): NameCases
    {
        $this->nominativ = $nominativ;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getGenitive(): ?string
    {
        return $this->genitive;
    }

    /**
     * @param string|null $genitive
     * @return NameCases
     */
    public function setGenitive(?string $genitive): NameCases
    {
        $this->genitive = $genitive;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDative(): ?string
    {
        return $this->dative;
    }

    /**
     * @param string|null $dative
     * @return NameCases
     */
    public function setDative(?string $dative): NameCases
    {
        $this->dative = $dative;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAccusative(): ?string
    {
        return $this->accusative;
    }

    /**
     * @param string|null $accusative
     * @return NameCases
     */
    public function setAccusative(?string $accusative): NameCases
    {
        $this->accusative = $accusative;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAblative(): ?string
    {
        return $this->ablative;
    }

    /**
     * @param string|null $ablative
     * @return NameCases
     */
    public function setAblative(?string $ablative): NameCases
    {
        $this->ablative = $ablative;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPrepositional(): ?string
    {
        return $this->prepositional;
    }

    /**
     * @param string|null $prepositional
     * @return NameCases
     */
    public function setPrepositional(?string $prepositional): NameCases
    {
        $this->prepositional = $prepositional;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getVocative(): ?string
    {
        return $this->vocative;
    }

    /**
     * @param string|null $vocative
     * @return NameCases
     */
    public function setVocative(?string $vocative): NameCases
    {
        $this->vocative = $vocative;
        return $this;
    }

}