<?php


namespace App\Dto\Api;

use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Type;

/**
 * Class ValidationFieldsError
 * @package App\ApiDto\Api
 */
class ValidationFieldsError extends ApiDto
{
    #[Serializer\Type("string")]
    public string $property;

    #[Serializer\Type("string")]
    public string $message;

    #[Serializer\Type("integer")]
    public ?int $code = null;

    /**
     * @return string
     */
    public function getProperty(): string
    {
        return $this->property;
    }

    /**
     * @param string $property
     * @return ValidationFieldsError
     */
    public function setProperty(string $property): ValidationFieldsError
    {
        $this->property = $property;
        return $this;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param string $message
     * @return ValidationFieldsError
     */
    public function setMessage(string $message): ValidationFieldsError
    {
        $this->message = $message;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getCode(): ?int
    {
        return $this->code;
    }

    /**
     * @param int|null $code
     * @return ValidationFieldsError
     */
    public function setCode(?int $code): ValidationFieldsError
    {
        $this->code = $code;
        return $this;
    }

}