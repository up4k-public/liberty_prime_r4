<?php


namespace App\Dto\Api\User\Request;


use App\Dto\Api\ApiDto;
use App\Factories\IntegrationsFactory;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Type;
use OpenApi\Attributes as OA;

/**
 * Class AddCartrige
 * @package App\Dto\Api\User\Request
 */
class AddCartrige extends ApiDto
{
    #[Serializer\Type("string")]
    #[Assert\NotBlank]
    public string $integration_type = '';

    #[Serializer\Type("array")]
    #[Assert\NotBlank]
    public array $auth_data = [];

    #[Serializer\Type("array<integer>")]
    public array $sharing_user = [];

    #[Assert\IsTrue(message: 'Invalid integration type name.')]
    public function isValidIntegrationType(): bool
    {
        return in_array($this->integration_type, IntegrationsFactory::getAllNames());
    }

    /**
     * @return string
     */
    public function getIntegrationType(): string
    {
        return $this->integration_type;
    }

    /**
     * @param string $integration_type
     * @return AddCartrige
     */
    public function setIntegrationType(string $integration_type): AddCartrige
    {
        $this->integration_type = $integration_type;
        return $this;
    }

    /**
     * @return array
     */
    public function getAuthData(): array
    {
        return $this->auth_data;
    }

    /**
     * @param array $auth_data
     * @return AddCartrige
     */
    public function setAuthData(array $auth_data): AddCartrige
    {
        $this->auth_data = $auth_data;
        return $this;
    }

    /**
     * @return array
     */
    public function getSharingUser(): array
    {
        return $this->sharing_user;
    }

    /**
     * @param array $sharing_user
     * @return AddCartrige
     */
    public function setSharingUser(array $sharing_user): AddCartrige
    {
        $this->sharing_user = $sharing_user;
        return $this;
    }


    /**
     * Совсем немного говнокода
     * @return int|null
     */
    public function getId(): ?int
    {
        return null;
    }

}