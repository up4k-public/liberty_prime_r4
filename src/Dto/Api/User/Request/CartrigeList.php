<?php


namespace App\Dto\Api\User\Request;

use App\Dto\Api\ApiDto;
use App\Dto\Api\Request\Pagination;
use App\Dto\Api\Request\Sort;
use App\Entity\Cartrige;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Type;
use OpenApi\Attributes as OA;

/**
 * Class CartrigeList
 * @package App\Dto\Api\User\Request
 */
class CartrigeList extends ApiDto
{
    use Pagination;
    use Sort;

    #[Serializer\Type("array<string>")]
    public ?array $integration_types = null;

    #[Serializer\Type("array<integer>")]
    public ?array $owner_ids = null;

    #[Assert\IsTrue(message: 'Invalid Cartrige sort filed name.')]
    public function hasValidSortFieldName(): bool
    {
        if (is_null($this->getSortField())) {
            return true;
        }
        return in_array($this->getSortField(), Cartrige::AVAILABLE_SORT_FIELD);
    }

    /**
     * @return array|null
     */
    public function getIntegrationTypes(): ?array
    {
        return $this->integration_types;
    }

    /**
     * @param array|null $integration_types
     * @return CartrigeList
     */
    public function setIntegrationTypes(?array $integration_types): CartrigeList
    {
        $this->integration_types = $integration_types;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getOwnerIds(): ?array
    {
        return $this->owner_ids;
    }

    /**
     * @param array|null $owner_ids
     * @return CartrigeList
     */
    public function setOwnerIds(?array $owner_ids): CartrigeList
    {
        $this->owner_ids = $owner_ids;
        return $this;
    }

}