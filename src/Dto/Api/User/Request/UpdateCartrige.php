<?php


namespace App\Dto\Api\User\Request;

use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Type;

/**
 * Class UpdateCartrige
 * @package App\Dto\Api\User\Request
 */
class UpdateCartrige extends AddCartrige
{
    #[Serializer\Type("integer")]
    #[Assert\NotBlank]
    public int $id;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return UpdateCartrige
     */
    public function setId(int $id): UpdateCartrige
    {
        $this->id = $id;
        return $this;
    }

}