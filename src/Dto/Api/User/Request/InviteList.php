<?php


namespace App\Dto\Api\User\Request;


use App\Dto\Api\ApiDto;
use App\Dto\Api\Request\Pagination;
use App\Dto\Api\Request\Sort;
use App\Entity\Invite;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class InviteList
 * @package App\Dto\Api\User\Request
 */
class InviteList extends ApiDto
{
    use Pagination;
    use Sort;

    #[Assert\IsTrue(message: 'Invalid invite sort filed name.')]
    public function hasValidSortFieldName(): bool
    {
        if (is_null($this->getSortField())) {
            return true;
        }
        return in_array($this->getSortField(), Invite::AVAILABLE_SORT_FIELD);
    }
}