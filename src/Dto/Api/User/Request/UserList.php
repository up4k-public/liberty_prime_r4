<?php


namespace App\Dto\Api\User\Request;

use App\Dto\Api\ApiDto;
use App\Dto\Api\Request\Pagination;
use App\Dto\Api\Request\Sort;
use App\Entity\User;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class UserList
 * @package App\Dto\Api\User\Request
 */
class UserList extends ApiDto
{
    use Pagination;
    use Sort;

    #[Assert\IsTrue(message: 'Invalid user sort filed name.')]
    public function hasValidSortFieldName(): bool
    {
        if (is_null($this->getSortField())) {
            return true;
        }
        return in_array($this->getSortField(), User::AVAILABLE_SORT_FIELD);
    }
}