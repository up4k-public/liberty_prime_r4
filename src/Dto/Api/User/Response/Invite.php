<?php


namespace App\Dto\Api\User\Response;

use App\Dto\Api\ApiDto;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Type;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Invite
 * @package App\Dto\Api\User\Response
 */
class Invite
{
    #[Serializer\Type("string")]
    public string $key;

    #[Serializer\Type("string")]
    public string $ts_created;

    #[Serializer\Type("boolean")]
    public bool $is_use;

    #[Serializer\Type("App\Dto\Api\User\Response\Patron")]
    public ?Patron $protege;

    /**
     * @return string
     */
    public function getKey(): string
    {
        return $this->key;
    }

    /**
     * @param string $key
     * @return Invite
     */
    public function setKey(string $key): Invite
    {
        $this->key = $key;
        return $this;
    }

    /**
     * @return string
     */
    public function getTsCreated(): string
    {
        return $this->ts_created;
    }

    /**
     * @param string $ts_created
     * @return Invite
     */
    public function setTsCreated(string $ts_created): Invite
    {
        $this->ts_created = $ts_created;
        return $this;
    }

    /**
     * @return bool
     */
    public function isIsUse(): bool
    {
        return $this->is_use;
    }

    /**
     * @param bool $is_use
     * @return Invite
     */
    public function setIsUse(bool $is_use): Invite
    {
        $this->is_use = $is_use;
        return $this;
    }

    /**
     * @return Patron|null
     */
    public function getProtege(): ?Patron
    {
        return $this->protege;
    }

    /**
     * @param Patron|null $protege
     * @return Invite
     */
    public function setProtege(?Patron $protege): Invite
    {
        $this->protege = $protege;
        return $this;
    }

}