<?php


namespace App\Dto\Api\User\Response;


use App\Dto\Api\BaseList;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Type;
use Symfony\Component\Validator\Constraints as Assert;
use OpenApi\Attributes as OA;

/**
 * Class CartrigeList
 * @package App\Dto\Api\User\Response
 */
class CartrigeList extends BaseList
{
    #[Serializer\Type("array<App\Dto\Api\User\Response\AddCartrige>")]
    public array $items = [];
}