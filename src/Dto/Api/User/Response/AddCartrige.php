<?php


namespace App\Dto\Api\User\Response;

use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Type;
use Symfony\Component\Validator\Constraints as Assert;
use OpenApi\Attributes as OA;

/**
 * Class AddCartrige
 * @package App\Dto\Api\User\Response
 */
#[OA\Schema(description:  "Add cartrige Response")]
class AddCartrige extends ShortCartrigeInfo
{
    #[Serializer\Type("string")]
    #[Assert\NotBlank]
    #[OA\Property(description: 'The unique identifier of the integration (vk, tg...).')]
    public string $integration_type;

    #[Serializer\Type("array<App\Dto\Api\User\Response\ShortUserInfo>")]
    #[Assert\NotBlank]
    #[OA\Property(description: 'List of users who can manage this cartridge')]
    public array $operators;

    /**
     * @return string
     */
    public function getIntegrationType(): string
    {
        return $this->integration_type;
    }

    /**
     * @param string $integration_type
     * @return AddCartrige
     */
    public function setIntegrationType(string $integration_type): AddCartrige
    {
        $this->integration_type = $integration_type;
        return $this;
    }

    /**
     * @return array
     */
    public function getOperators(): array
    {
        return $this->operators;
    }

    /**
     * @param array $operators
     * @return AddCartrige
     */
    public function setOperators(array $operators): AddCartrige
    {
        $this->operators = $operators;
        return $this;
    }


}