<?php


namespace App\Dto\Api\User\Response;

use App\Dto\Api\ApiDto;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Type;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Patron
 * @package App\Dto\Api\User\Response
 */
class Patron extends ApiDto
{
    #[Serializer\Type("integer")]
    public ?int $id;

    #[Serializer\Type("string")]
    public ?string $name;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return Patron
     */
    public function setId(?int $id): Patron
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     * @return Patron
     */
    public function setName(?string $name): Patron
    {
        $this->name = $name;
        return $this;
    }

}