<?php


namespace App\Dto\Api\User\Response;

use App\Dto\Api\ApiDto;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Type;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class User
 * @package App\Dto\Api\User\Response
 */
class User extends ApiDto
{
    #[Serializer\Type("integer")]
    public int $id;

    #[Serializer\Type("string")]
    #[Assert\NotBlank]
    public string $name;

    #[Serializer\Type("string")]
    #[Assert\NotBlank]
    public string $ts_created;

    #[Serializer\Type("App\Dto\Api\User\Response\Patron")]
    public ?Patron $patron;

    #[Serializer\Type("bool")]
    #[Assert\NotBlank]
    public ?bool $is_banned = false;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return User
     */
    public function setId(int $id): User
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return User
     */
    public function setName(string $name): User
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getTsCreated(): string
    {
        return $this->ts_created;
    }

    /**
     * @param string $ts_created
     * @return User
     */
    public function setTsCreated(string $ts_created): User
    {
        $this->ts_created = $ts_created;
        return $this;
    }

    /**
     * @return Patron|null
     */
    public function getPatron(): ?Patron
    {
        return $this->patron;
    }

    /**
     * @param Patron|null $patron
     * @return User
     */
    public function setPatron(?Patron $patron): User
    {
        $this->patron = $patron;
        return $this;
    }



    /**
     * @return bool|null
     */
    public function getIsBanned(): ?bool
    {
        return $this->is_banned;
    }

    /**
     * @param bool|null $is_banned
     * @return User
     */
    public function setIsBanned(?bool $is_banned): User
    {
        $this->is_banned = $is_banned;
        return $this;
    }

}