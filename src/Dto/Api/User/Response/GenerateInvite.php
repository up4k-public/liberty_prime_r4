<?php


namespace App\Dto\Api\User\Response;

use App\Dto\Api\ApiDto;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Type;

/**
 * Class GenerateInvite
 * @package App\ApiDto\Api\Account\Response
 */
class GenerateInvite extends ApiDto
{
   public string $invite;

    /**
     * @return string
     */
    public function getInvite(): string
    {
        return $this->invite;
    }

    /**
     * @param string $invite
     * @return GenerateInvite
     */
    public function setInvite(string $invite): GenerateInvite
    {
        $this->invite = $invite;
        return $this;
    }

}