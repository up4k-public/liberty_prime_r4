<?php


namespace App\Dto\Api\User\Response;

use App\Core\Author;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Type;
use Symfony\Component\Validator\Constraints as Assert;
use OpenApi\Attributes as OA;

/**
 * Class ShortCartrigeInfo
 * @package App\Dto\Api\User\Response
 */
class ShortCartrigeInfo
{
    #[Serializer\Type("string")]
    #[Assert\NotBlank]
    #[OA\Property(description: 'The unique external identifier')]
    public string $external_id;

    #[Serializer\Type("string")]
    #[Assert\NotBlank]
    #[OA\Property(description: 'Cartridge name in external system')]
    public string $name;

    #[Serializer\Type("array")]
    #[OA\Property(description: 'Custom info by external system')]
    public array $info;

    #[Serializer\Type("integer")]
    #[Assert\NotBlank]
    #[OA\Property(description: 'The unique internal identifier by LibertyPrime system')]
    public int $id;

    /**
     * @return string
     */
    public function getExternalId(): string
    {
        return $this->external_id;
    }

    /**
     * @param string $external_id
     * @return ShortCartrigeInfo
     */
    public function setExternalId(string $external_id): ShortCartrigeInfo
    {
        $this->external_id = $external_id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return ShortCartrigeInfo
     */
    public function setName(string $name): ShortCartrigeInfo
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return array
     */
    public function getInfo(): array
    {
        return $this->info;
    }

    /**
     * @param Author $info
     * @return ShortCartrigeInfo
     */
    public function setInfo(Author $info): ShortCartrigeInfo
    {
        $this->info = json_decode(json_encode($info), true);
        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return ShortCartrigeInfo
     */
    public function setId(int $id): ShortCartrigeInfo
    {
        $this->id = $id;
        return $this;
    }

}