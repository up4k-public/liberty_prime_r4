<?php


namespace App\Dto\Api\User\Response;

use App\Dto\Api\ApiDto;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Type;
use Symfony\Component\Validator\Constraints as Assert;
use OpenApi\Attributes as OA;

/**
 * Class ShortUserInfo
 * @package App\Dto\Api\User\Response
 */
class ShortUserInfo extends ApiDto
{
    #[Serializer\Type("integer")]
    #[Assert\NotBlank]
    #[OA\Property(description: 'The unique identifier of user')]
    public int $id;

    #[Serializer\Type("string")]
    #[OA\Property(description: 'Name of user')]
    public string $username;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return ShortUserInfo
     */
    public function setId(int $id): ShortUserInfo
    {
        $this->id = $id;
        return $this;
    }


    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @param string $username
     * @return ShortUserInfo
     */
    public function setUsername(string $username): ShortUserInfo
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @return RegisterNew
     */
    public function mothToRegisterNew():RegisterNew
    {
        return (new RegisterNew())
            ->setId($this->getId())
            ->setUsername($this->getUsername());
    }

}