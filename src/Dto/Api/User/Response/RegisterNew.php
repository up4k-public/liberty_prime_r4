<?php


namespace App\Dto\Api\User\Response;

use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Type;
use OpenApi\Attributes as OA;

/**
 * Class RegisterNew
 * @package App\Dto\Api\User\Response
 */
class RegisterNew extends ShortUserInfo
{
    #[Serializer\Type("string")]
    #[OA\Property(description: 'The name of user who issued the invitation')]
    public string $patron_name;

    /**
     * @return string
     */
    public function getPatronName(): string
    {
        return $this->patron_name;
    }

    /**
     * @param string $patron_name
     * @return RegisterNew
     */
    public function setPatronName(string $patron_name): RegisterNew
    {
        $this->patron_name = $patron_name;
        return $this;
    }

}