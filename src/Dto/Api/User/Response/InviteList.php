<?php


namespace App\Dto\Api\User\Response;

use App\Dto\Api\BaseList;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\Type;
use OpenApi\Attributes as OA;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class InviteList
 * @package App\Dto\Api\User\Response
 */
class InviteList extends BaseList
{
    #[Serializer\Type("array<App\Dto\Api\User\Response\Invite>")]
    #[Assert\NotBlank]
    public array $items = [];

    /**
     * @return array
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * @param array $items
     * @return InviteList
     */
    public function setItems(array $items): InviteList
    {
        $this->items = $items;
        return $this;
    }
}