<?php


namespace App\Dto\Api;

use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Type;

/**
 * Class Error
 * @package App\ApiDto\Api
 */
class Error extends ApiDto
{
    #[Serializer\Type("integer")]
    public int $code;

    #[Serializer\Type("string")]
    public string $message;

    #[Serializer\Type("string")]
    public string $line;

    #[Serializer\Type("string")]
    public string $file;

    /**
     * @return int
     */
    public function getCode(): int
    {
        return $this->code;
    }

    /**
     * @param int $code
     * @return Error
     */
    public function setCode(int $code): Error
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param string $message
     * @return Error
     */
    public function setMessage(string $message): Error
    {
        $this->message = $message;
        return $this;
    }

    /**
     * @return string
     */
    public function getLine(): string
    {
        return $this->line;
    }

    /**
     * @param string $line
     * @return Error
     */
    public function setLine(string $line): Error
    {
        $this->line = $line;
        return $this;
    }

    /**
     * @return string
     */
    public function getFile(): string
    {
        return $this->file;
    }

    /**
     * @param string $file
     * @return Error
     */
    public function setFile(string $file): Error
    {
        $this->file = $file;
        return $this;
    }

}