<?php


namespace App\Dto\Api\Request;

use App\Dto\Api\ApiDto;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Type;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Filter
 * @package App\ApiDto\Api\Request
 */
class Filter extends ApiDto
{
    #[Serializer\Type("string")]
    #[Assert\NotBlank]
    public string $name;

    #[Serializer\Type("array")]
    #[Assert\NotBlank]
    public array $value;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Filter
     */
    public function setName(string $name): Filter
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return array
     */
    public function getValue(): array
    {
        return $this->value;
    }

    /**
     * @param array $value
     * @return Filter
     */
    public function setValue(array $value): Filter
    {
        $this->value = $value;
        return $this;
    }

}