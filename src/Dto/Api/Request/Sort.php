<?php


namespace App\Dto\Api\Request;

use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Type;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Trait Sort
 * @package App\Dto\Api\Request
 */
trait Sort
{
    #[Serializer\Type("string")]
    public ?string $sort_field = null;

    #[Serializer\Type("string")]
    public string $sort_by = 'ASC';

    #[Assert\IsTrue(message: 'The sort_by must be ASC or DESC.')]
    public function isSortBySafe(): bool
    {
        return in_array($this->sort_by, ['ASC', 'DESC']);
    }

    /**
     * @return string|null
     */
    public function getSortField(): ?string
    {
        return $this->sort_field;
    }

    /**
     * @param string|null $sort_field
     * @return Sort
     */
    public function setSortField(?string $sort_field): Sort
    {
        $this->sort_field = $sort_field;
        return $this;
    }

    /**
     * @return string
     */
    public function getSortBy(): string
    {
        return $this->sort_by;
    }

    /**
     * @param string $sort_by
     * @return Sort
     */
    public function setSortBy(string $sort_by): Sort
    {
        $this->sort_by = $sort_by;
        return $this;
    }
}