<?php


namespace App\Dto\Api\Request;

use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Type;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Trait Pagination
 * @package App\ApiDto\Api\Request
 */
trait Pagination
{
    #[Serializer\Type("integer")]
    #[Assert\NotBlank]
    public int $limit = 20;

    #[Serializer\Type("integer")]
    #[Assert\NotBlank]
    public int $offset = 0;

    /**
     * @return int
     */
    public function getLimit(): int
    {
        return $this->limit;
    }

    /**
     * @param int $limit
     * @return Pagination
     */
    public function setLimit(int $limit): Pagination
    {
        $this->limit = $limit;
        return $this;
    }

    /**
     * @return int
     */
    public function getOffset(): int
    {
        return $this->offset;
    }

    /**
     * @param int $offset
     * @return Pagination
     */
    public function setOffset(int $offset): Pagination
    {
        $this->offset = $offset;
        return $this;
    }

}