<?php


namespace App\Dto\Api\Request;

use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Type;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Trait Filters
 * @package App\ApiDto\Api\Request
 */
trait Filters
{
    #[Serializer\Type("array<App\Dto\Api\Request\Filter>")]
    public ?array $filters = [];

    /**
     * @return array|null
     */
    public function getFilters(): ?array
    {
        return $this->filters;
    }

    /**
     * @param array|null $filters
     * @return Filters
     */
    public function setFilters(?array $filters): Filters
    {
        $this->filters = $filters;
        return $this;
    }

}