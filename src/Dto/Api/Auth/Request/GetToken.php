<?php


namespace App\Dto\Api\Auth\Request;

use App\Dto\Api\ApiDto;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Type;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class GetToken
 * @package App\Dto\Api\Auth\Request
 */
class GetToken extends ApiDto
{
    #[Serializer\Type("string")]
    #[Assert\NotBlank]
    public ?string $username;

    #[Serializer\Type("string")]
    #[Assert\NotBlank]
    public ?string $password;

    /**
     * @return string|null
     */
    public function getUsername(): ?string
    {
        return $this->username;
    }

    /**
     * @param string|null $username
     * @return GetToken
     */
    public function setUsername(?string $username): GetToken
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * @param string|null $password
     * @return GetToken
     */
    public function setPassword(?string $password): GetToken
    {
        $this->password = $password;
        return $this;
    }

}