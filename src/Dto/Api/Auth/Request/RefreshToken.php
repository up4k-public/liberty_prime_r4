<?php


namespace App\Dto\Api\Auth\Request;

use App\Dto\Api\ApiDto;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Type;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class RefreshToken
 * @package App\Dto\Api\Auth\Request
 */
class RefreshToken extends ApiDto
{
    #[Serializer\Type("string")]
    #[Assert\NotBlank]
    public ?string $refresh_token;

    /**
     * @return string|null
     */
    public function getRefreshToken(): ?string
    {
        return $this->refresh_token;
    }

    /**
     * @param string|null $refresh_token
     * @return RefreshToken
     */
    public function setRefreshToken(?string $refresh_token): RefreshToken
    {
        $this->refresh_token = $refresh_token;
        return $this;
    }

}