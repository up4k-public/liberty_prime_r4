<?php


namespace App\Dto\Api\Auth\Response;

use App\Dto\Api\ApiDto;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Type;
use phpDocumentor\Reflection\Types\Integer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class GetToken
 * @package App\Dto\Api\Auth\Response
 */
class GetToken extends ApiDto
{
    #[Serializer\Type("string")]
    #[Assert\NotBlank]
    public string $token;

    #[Serializer\Type("string")]
    #[Assert\NotBlank]
    public string $refresh_token;

    #[Serializer\Type("integer")]
    #[Assert\NotBlank]
    public int $refresh_token_expiration;

    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }

    /**
     * @param string $token
     * @return GetToken
     */
    public function setToken(string $token): GetToken
    {
        $this->token = $token;
        return $this;
    }

    /**
     * @return string
     */
    public function getRefreshToken(): string
    {
        return $this->refreshToken;
    }

    /**
     * @param string $refreshToken
     * @return GetToken
     */
    public function setRefreshToken(string $refreshToken): GetToken
    {
        $this->refreshToken = $refreshToken;
        return $this;
    }

    /**
     * @return int
     */
    public function getRefreshTokenExpiration(): int
    {
        return $this->refresh_token_expiration;
    }

    /**
     * @param int $refresh_token_expiration
     * @return GetToken
     */
    public function setRefreshTokenExpiration(int $refresh_token_expiration): GetToken
    {
        $this->refresh_token_expiration = $refresh_token_expiration;
        return $this;
    }

}