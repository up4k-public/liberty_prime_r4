<?php


namespace App\Dto\Api\Assault\Response;


use App\Dto\Api\BaseList;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Type;
use OpenApi\Attributes as OA;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class AssaultList
 * @package App\Dto\Api\Assault\Response
 */
class AssaultList extends BaseList
{
    #[Serializer\Type("array<App\Dto\Api\Assault\Assault>")]
    #[Assert\NotBlank]
    public array $items = [];

    /**
     * @return array
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * @param array $items
     * @return AssaultList
     */
    public function setItems(array $items): AssaultList
    {
        $this->items = $items;
        return $this;
    }
}