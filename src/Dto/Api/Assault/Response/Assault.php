<?php


namespace App\Dto\Api\Assault\Response;

use App\Dto\Api\Assault\Request\CreateAssault;
use App\Dto\Api\User\Response\ShortCartrigeInfo;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Type;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Assault
 * @package App\Dto\Api\Assault\Response
 */
class Assault extends CreateAssault
{
    #[Serializer\Type("integer")]
    #[Assert\NotBlank]
    public int $id;

    #[Serializer\Type("string")]
    #[Assert\NotBlank]
    public string $status;

    #[Serializer\Type("App\Dto\Api\User\Response\ShortCartrigeInfo")]
    #[Assert\NotBlank]
    public ShortCartrigeInfo $cartrige;

    #[Serializer\Type("array")]
    public array $actions = [];

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Assault
     */
    public function setId(int $id): Assault
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return Assault
     */
    public function setStatus(string $status): Assault
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return ShortCartrigeInfo
     */
    public function getCartrige(): ShortCartrigeInfo
    {
        return $this->cartrige;
    }

    /**
     * @param ShortCartrigeInfo $cartrige
     * @return Assault
     */
    public function setCartrige(ShortCartrigeInfo $cartrige): Assault
    {
        $this->cartrige = $cartrige;
        return $this;
    }


}