<?php


namespace App\Dto\Api\Assault\Request;

use App\Dto\Api\ApiDto;
use App\Dto\Api\Request\Filter;
use App\Dto\Api\Request\Filters;
use App\Dto\Api\Request\Pagination;
use App\Dto\Api\Request\Sort;
use App\Entity\Assault;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Type;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class AssaultList
 * @package App\Dto\Api\Assault\Request
 */
class AssaultList extends ApiDto
{
    public array $statuses = [Assault::STATUS_NEW, Assault::STATUS_IN_PROCESS];

    use Pagination;
    use Sort;
    use Filters;

    #[Assert\IsTrue(message: 'Invalid assault-filters name.')]
    public function hasValidFiltersName(): bool
    {
        /** @var Filter $filter */
        foreach ($this->getFilters() as $filter) {
            if (!in_array($filter->getName(), Assault::FILTER_NAME)) {
                return false;
            }
        }
        return true;
    }

    #[Assert\IsTrue(message: 'Invalid assault status names.')]
    public function hasStatusList(): bool
    {
        foreach ($this->statuses as $status) {
            if (!in_array($status, [Assault::STATUS_NEW, Assault::STATUS_IN_PROCESS, Assault::STATUS_CANCELLED, Assault::STATUS_ERROR])) {
                return false;
            }
        }
        return true;
    }

    /**
     * @return array
     */
    public function getStatuses(): array
    {
        return $this->statuses;
    }

    /**
     * @param array $statuses
     * @return AssaultList
     */
    public function setStatuses(array $statuses): AssaultList
    {
        $this->statuses = $statuses;
        return $this;
    }

}