<?php


namespace App\Dto\Api\Assault\Request;


use App\Dto\Api\ApiDto;
use App\Settings\Settings;

use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Type;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Assault
 * @package App\Dto\Api\Assault\Request
 */
class CreateAssault extends ApiDto
{
    #[Serializer\Type("string")]
    #[Assert\NotBlank]
    public string $integration_type;

    #[Serializer\Type("string")]
    #[Assert\NotBlank]
    public string $local_type;

    #[Serializer\Type("string")]
    #[Assert\NotBlank]
    public string $target_id;

    #[Serializer\Type("string")]
    #[Assert\NotBlank]
    public string $cartrige_id;

    #[Serializer\Type("App\Settings\Settings")]
    #[Assert\NotBlank]
    public Settings $settings;

    /**
     * @return string
     */
    public function getIntegrationType(): string
    {
        return $this->integration_type;
    }

    /**
     * @param string $integration_type
     * @return CreateAssault
     */
    public function setIntegrationType(string $integration_type): CreateAssault
    {
        $this->integration_type = $integration_type;
        return $this;
    }

    /**
     * @return string
     */
    public function getLocalType(): string
    {
        return $this->local_type;
    }

    /**
     * @param string $local_type
     * @return CreateAssault
     */
    public function setLocalType(string $local_type): CreateAssault
    {
        $this->local_type = $local_type;
        return $this;
    }

    /**
     * @return string
     */
    public function getTargetId(): string
    {
        return $this->target_id;
    }

    /**
     * @param string $target_id
     * @return CreateAssault
     */
    public function setTargetId(string $target_id): CreateAssault
    {
        $this->target_id = $target_id;
        return $this;
    }

    /**
     * @return string
     */
    public function getCartrigeId(): string
    {
        return $this->cartrige_id;
    }

    /**
     * @param string $cartrige_id
     * @return CreateAssault
     */
    public function setCartrigeId(string $cartrige_id): CreateAssault
    {
        $this->cartrige_id = $cartrige_id;
        return $this;
    }

    /**
     * @return Settings
     */
    public function getSettings(): Settings
    {
        return $this->settings;
    }

    /**
     * @param Settings $settings
     * @return CreateAssault
     */
    public function setSettings(Settings $settings): CreateAssault
    {
        $this->settings = $settings;
        return $this;
    }

}