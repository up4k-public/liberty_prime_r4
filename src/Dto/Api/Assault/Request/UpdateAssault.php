<?php


namespace App\Dto\Api\Assault\Request;

use App\Dto\Api\ApiDto;
use App\Settings\Settings;

use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Type;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class UpdateAssault
 * @package App\Dto\Api\Assault\Request
 */
class UpdateAssault extends GetOneAssault
{
    #[Serializer\Type("string")]
    public string $cartrige_id;

    #[Serializer\Type("App\Settings")]
    #[Assert\NotBlank]
    public Settings $settings;

    /**
     * @return string
     */
    public function getCartrigeId(): string
    {
        return $this->cartrige_id;
    }

    /**
     * @param string $cartrige_id
     * @return UpdateAssault
     */
    public function setCartrigeId(string $cartrige_id): UpdateAssault
    {
        $this->cartrige_id = $cartrige_id;
        return $this;
    }

    /**
     * @return Settings
     */
    public function getSettings(): Settings
    {
        return $this->settings;
    }

    /**
     * @param Settings $settings
     * @return UpdateAssault
     */
    public function setSettings(Settings $settings): UpdateAssault
    {
        $this->settings = $settings;
        return $this;
    }

}