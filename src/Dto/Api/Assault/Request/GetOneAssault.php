<?php


namespace App\Dto\Api\Assault\Request;

use App\Dto\Api\ApiDto;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Type;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class GetOneAssault
 * @package App\Dto\Api\Assault\Request
 */
class GetOneAssault extends ApiDto
{
    #[Serializer\Type("integer")]
    #[Assert\NotBlank]
    public int $id;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return GetOneAssault
     */
    public function setId(int $id): GetOneAssault
    {
        $this->id = $id;
        return $this;
    }

}