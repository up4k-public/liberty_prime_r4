<?php


namespace App\Dto\Api\Assault\Request;

use App\Dto\Api\ApiDto;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Type;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class ChangeStatus
 * @package App\Dto\Api\Assault\Request
 */
class ChangeStatus extends ApiDto
{
    #[Serializer\Type("string")]
    #[Assert\NotBlank]
   public int $id;

    #[Serializer\Type("string")]
   public string $status;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return ChangeStatus
     */
    public function setId(int $id): ChangeStatus
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return ChangeStatus
     */
    public function setStatus(string $status): ChangeStatus
    {
        $this->status = $status;
        return $this;
    }

}