<?php


namespace App\Dto\Api;

use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Type;

/**
 * Class BaseResponseFormat
 * @package App\ApiDto\Api
 */
class BaseResponseFormat extends ApiDto
{
    #[Serializer\Type("boolean")]
    public bool $success;

    #[Serializer\Type("array")]
    public $data;

    #[Serializer\Type("App\Dto\Api\Error")]
    public ?Error $error = null;

    #[Serializer\Type("array")]
    public ?array $errors = null;


    #[Serializer\Type("string")]
    public string $api_version;

    /**
     * @return bool
     */
    public function isSuccess(): bool
    {
        return $this->success;
    }

    /**
     * @param bool $success
     * @return BaseResponseFormat
     */
    public function setSuccess(bool $success): BaseResponseFormat
    {
        $this->success = $success;
        return $this;
    }

    /**
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param $data
     * @return BaseResponseFormat
     */
    public function setData($data): BaseResponseFormat
    {
        $this->data = $data;
        return $this;
    }

    /**
     * @return array
     */
    public function getErrors(): ?array
    {
        return $this->errors;
    }

    /**
     * @param array $errors
     * @return BaseResponseFormat
     */
    public function setErrors(array $errors): BaseResponseFormat
    {
        $this->errors = $errors;
        if (empty($this->error)) {
            $this->error = $errors[array_key_first($errors)];
        }
        return $this;
    }

    /**
     * @return string
     */
    public function getApiVersion(): string
    {
        return $this->api_version;
    }

    /**
     * @param string $api_version
     * @return BaseResponseFormat
     */
    public function setApiVersion(string $api_version): BaseResponseFormat
    {
        $this->api_version = $api_version;
        return $this;
    }

    /**
     * @return ?Error
     */
    public function getError(): ?Error
    {
        return $this->error;
    }

    /**
     * @param Error $error
     * @return BaseResponseFormat
     */
    public function setError(Error $error): BaseResponseFormat
    {
        $this->error = $error;
        if (empty($this->errors)) {
            $this->errors[] = $error;
        }
        return $this;
    }

}