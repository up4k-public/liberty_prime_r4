<?php


namespace App\Dto\Api;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\Type;
use OpenApi\Attributes as OA;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class BaseList
 * @package App\ApiDto\Api
 */
class BaseList extends ApiDto
{
    #[Serializer\Type("array")]
    #[Assert\NotBlank]
    public array $items = [];

    #[Serializer\Type("integer")]
    #[Assert\NotBlank]
    public int $count = 0;

    /**
     * @return array
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * @param array $items
     * @return BaseList
     */
    public function setItems(array $items): BaseList
    {
        $this->items = $items;
        return $this;
    }

    /**
     * @return int
     */
    public function getCount(): int
    {
        return $this->count;
    }

    /**
     * @param int $count
     * @return BaseList
     */
    public function setCount(int $count): BaseList
    {
        $this->count = $count;
        return $this;
    }

}