<?php


namespace App\Dto\Api\Register;

use App\Dto\Api\ApiDto;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Type;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class RegisterNewUser
 * @package App\ApiDto\Api\Register
 */
class RegisterNewUser extends ApiDto
{
    #[Serializer\Type("string")]
    #[Assert\NotBlank]
    #[Assert\Length(min: 3)]
    public string $username;

    #[Serializer\Type("string")]
    #[Assert\NotBlank]
    #[Assert\Length(min: 7)]
    public string $password;

    #[Serializer\Type("string")]
    #[Assert\Uuid]
    #[Assert\NotBlank]
    public string $invite;

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @param string $username
     * @return RegisterNewUser
     */
    public function setUsername(string $username): RegisterNewUser
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return RegisterNewUser
     */
    public function setPassword(string $password): RegisterNewUser
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return string
     */
    public function getInvite(): string
    {
        return $this->invite;
    }

    /**
     * @param string $invite
     * @return RegisterNewUser
     */
    public function setInvite(string $invite): RegisterNewUser
    {
        $this->invite = $invite;
        return $this;
    }

    #[Assert\IsTrue(message: 'The password cannot match your username.')]
    public function isPasswordSafe()
    {
        return $this->username !== $this->password;
    }

}