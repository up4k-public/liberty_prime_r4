<?php


namespace App\Dto\Api\Service\Response;

use App\Dto\Api\BaseList;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\Type;
use OpenApi\Attributes as OA;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class ServiceList
 * @package App\Dto\Api\Service\Response
 */
class ServiceList extends BaseList
{
    #[Serializer\Type("array<string>")]
    #[Assert\NotBlank]
    public array $items = [];
}