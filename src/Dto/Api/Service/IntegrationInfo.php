<?php


namespace App\Dto\Api\Service;


use App\Dto\Api\ApiDto;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Type;

/**
 * Class IntegrationInfo
 * @package App\ApiDto\Api\Service
 */
class IntegrationInfo extends ApiDto
{
    #[Serializer\Type("string")]
    public string $name;

    #[Serializer\Type("array")]
    public ?array $sub_names = null;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return IntegrationInfo
     */
    public function setName(string $name): IntegrationInfo
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getSubNames(): ?array
    {
        return $this->sub_names;
    }

    /**
     * @param array|null $sub_names
     * @return IntegrationInfo
     */
    public function setSubNames(?array $sub_names): IntegrationInfo
    {
        $this->sub_names = $sub_names;
        return $this;
    }

}