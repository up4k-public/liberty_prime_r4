<?php


namespace App\Dto\Api\Service;


use App\Dto\Api\ApiDto;
use App\Dto\Api\Request\Pagination;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Type;

/**
 * Class ServiceList
 * @package App\ApiDto\Api\Service
 */
class ServiceList extends ApiDto
{
     use Pagination;
}