<?php


namespace App\Dto\Api\Phrase\Response;


use App\Dto\Api\BaseList;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Type;
use OpenApi\Attributes as OA;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class PhraseList
 * @package App\ApiDto\Api\Phrase
 */
class PhraseList extends BaseList
{
    #[Serializer\Type("array<App\Dto\Api\Phrase\Phrase>")]
    #[Assert\NotBlank]
    public array $items = [];

    /**
     * @return array
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * @param array $items
     * @return PhraseList
     */
    public function setItems(array $items): PhraseList
    {
        $this->items = $items;
        return $this;
    }

}