<?php


namespace App\Dto\Api\Phrase\Response;


use App\Dto\Api\Phrase\Phrase;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Type;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class GetOne
 * @package App\ApiDto\Api\Phrase\Response
 */
class GetOne extends Phrase
{
    #[Serializer\Type("array<App\Dto\Api\Phrase\Phrase>")]
    public ?array $history = [];

    #[Serializer\Type("integer")]
    public int $history_count = 0;

    /**
     * @return array|null
     */
    public function getHistory(): ?array
    {
        return $this->history;
    }

    /**
     * @param array|null $history
     * @return GetOne
     */
    public function setHistory(?array $history): GetOne
    {
        $this->history = $history;
        return $this;
    }

    /**
     * @return int
     */
    public function getHistoryCount(): int
    {
        return $this->history_count;
    }

    /**
     * @param int $history_count
     * @return GetOne
     */
    public function setHistoryCount(int $history_count): GetOne
    {
        $this->history_count = $history_count;
        return $this;
    }

}