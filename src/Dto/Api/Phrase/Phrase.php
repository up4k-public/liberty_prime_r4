<?php


namespace App\Dto\Api\Phrase;

use App\Dto\Api\ApiDto;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Type;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Phrase
 * @package App\ApiDto\Api\Phrase
 */
class Phrase extends ApiDto
{
    #[Serializer\Type("integer")]
    public ?int $id = null;

    #[Serializer\Type("string")]
    #[Assert\NotBlank]
    public ?string $text = null;

    #[Serializer\Type("string")]
    public ?string $author = null;

    #[Serializer\Type("array")]
    public ?array $params = [];

    #[Serializer\Type("array")]
    public ?array $assessment = [];

    #[Serializer\Type("array")]
    public ?array $triggers = [];

    #[Serializer\Type("array")]
    public ?array $tags = [];

    #[Serializer\Type("array")]
    public ?array $modules = [];

    #[Serializer\Type("string")]
    public ?string $updated_by = null;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return Phrase
     */
    public function setId(?int $id): Phrase
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getText(): ?string
    {
        return $this->text;
    }

    /**
     * @param string|null $text
     * @return Phrase
     */
    public function setText(?string $text): Phrase
    {
        $this->text = $text;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getParams(): ?array
    {
        return $this->params;
    }

    /**
     * @param array|null $params
     * @return Phrase
     */
    public function setParams(?array $params): Phrase
    {
        $this->params = $params;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getAssessment(): ?array
    {
        return $this->assessment;
    }

    /**
     * @param array|null $assessment
     * @return Phrase
     */
    public function setAssessment(?array $assessment): Phrase
    {
        $this->assessment = $assessment;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getTriggers(): ?array
    {
        return $this->triggers;
    }

    /**
     * @param array|null $triggers
     * @return Phrase
     */
    public function setTriggers(?array $triggers): Phrase
    {
        $this->triggers = $triggers;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getTags(): ?array
    {
        return $this->tags;
    }

    /**
     * @param array|null $tags
     * @return Phrase
     */
    public function setTags(?array $tags): Phrase
    {
        $this->tags = $tags;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getModules(): ?array
    {
        return $this->modules;
    }

    /**
     * @param array|null $modules
     * @return Phrase
     */
    public function setModules(?array $modules): Phrase
    {
        $this->modules = $modules;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAuthor(): ?string
    {
        return $this->author;
    }

    /**
     * @param string|null $author
     * @return Phrase
     */
    public function setAuthor(?string $author): Phrase
    {
        $this->author = $author;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getUpdatedBy(): ?string
    {
        return $this->updated_by;
    }

    /**
     * @param string|null $updated_by
     * @return Phrase
     */
    public function setUpdatedBy(?string $updated_by): Phrase
    {
        $this->updated_by = $updated_by;
        return $this;
    }

}