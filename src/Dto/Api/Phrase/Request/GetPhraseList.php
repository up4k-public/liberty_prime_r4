<?php


namespace App\Dto\Api\Phrase\Request;

use App\Dto\Api\ApiDto;
use App\Dto\Api\Request\Filter;
use App\Dto\Api\Request\Sort;
use App\Entity\Phrase;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Type;

use App\Dto\Api\Request\Filters;
use App\Dto\Api\Request\Pagination;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class GetPhraseList
 * @package App\ApiDto\Api\Phrase\Request
 */
class GetPhraseList extends ApiDto
{
    use Filters;
    use Pagination;
    use Sort;

    #[Assert\IsTrue(message: 'Invalid phrases-filters name.')]
    public function hasValidFiltersName():bool
    {
        /** @var Filter $filter */
        foreach ($this->getFilters() as $filter) {
            if (!in_array($filter->getName(), Phrase::FILTER_NAME)) {
                return false;
            }
        }
        return true;
    }
}