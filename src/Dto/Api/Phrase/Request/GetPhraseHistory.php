<?php


namespace App\Dto\Api\Phrase\Request;

use App\Dto\Api\ApiDto;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\Type;

/**
 * Class GetPhraseHistory
 * @package App\ApiDto\Api\Phrase\Request
 */
class GetPhraseHistory extends ApiDto
{
    #[Serializer\Type("integer")]
    #[Assert\NotBlank]
    public ?int $id = null;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return GetPhraseHistory
     */
    public function setId(?int $id): GetPhraseHistory
    {
        $this->id = $id;
        return $this;
    }

}