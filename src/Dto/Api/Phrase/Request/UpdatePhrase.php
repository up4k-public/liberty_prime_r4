<?php


namespace App\Dto\Api\Phrase\Request;


use App\Dto\Api\Phrase\Phrase;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Type;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class UpdatePhrase
 * @package App\ApiDto\Api\Phrase\Request
 */
class UpdatePhrase extends Phrase
{
    #[Serializer\Type("integer")]
    #[Assert\NotBlank]
    public ?int $id = null;

}