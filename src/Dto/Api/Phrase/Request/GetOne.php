<?php


namespace App\Dto\Api\Phrase\Request;

use App\Dto\Api\ApiDto;
use App\Dto\Api\Request\Pagination;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\Type;

/**
 * Class GetOne
 * @package App\ApiDto\Api\Phrase\Request
 */
class GetOne extends ApiDto
{
    use Pagination;

    #[Serializer\Type("integer")]
    #[Assert\NotBlank]
    public ?int $id = null;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return GetOne
     */
    public function setId(?int $id): GetOne
    {
        $this->id = $id;
        return $this;
    }

}