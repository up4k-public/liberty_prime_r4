<?php


namespace App\Helpers;

/**
 * Class TextConverterHelper
 * @package App\Helpers
 */
class TextConverterHelper
{
    public static function firstLetterToLowerCase(string $str): string
    {
        $char = mb_strtolower(substr($str, 0, 1));
        $str[0] = $char;
        return $str;
    }
}