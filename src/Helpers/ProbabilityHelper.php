<?php


namespace App\Helpers;

/**
 * Class ProbabilityHelper
 * @package App\Helpers
 */
class ProbabilityHelper
{
    const ACCURACY = 100; //10^2=100 два знака после запятой

    /**
     * Подбрасываем монетку
     * @param float $probability
     * @return bool
     */
   public static function roll(float $probability):bool
   {
       if ($probability == 0) {
           return false;
       }
       if ($probability >= 100) {
           return true;
       }
       $probability = round($probability * self::ACCURACY);
       $random = rand(0, 100 * self::ACCURACY);
       if ($random <= $probability) {
           return true;
       }
       return false;
   }
}