<?php


namespace App\Settings;

use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Type;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class AppealSettings
 * Класс с настройками обращение к собеседнику:имя, фамилия, варианты имени, прозвище, ник
 * @package App\Settings
 */
class AppealSettings extends AbstractSettings
{
    #[Serializer\Type("float")]
    public float $probability_giving_nickname = 0.5;

    #[Serializer\Type("integer")]
    public int $min_threshold_msg_for_issuance_of_nickname = 150;

    #[Serializer\Type("boolean")]
    public bool $always_use_nickname = false;

    #[Serializer\Type("float")]
    public float $probability_use_alter_name = 25.0;

    #[Serializer\Type("boolean")]
    public bool $use_full_name = true; //Петя Иванов

    /**
     * @return float
     */
    public function getProbabilityGivingNickname(): float
    {
        return $this->probability_giving_nickname;
    }

    /**
     * @param float $probability_giving_nickname
     * @return AppealSettings
     */
    public function setProbabilityGivingNickname(float $probability_giving_nickname): AppealSettings
    {
        $this->probability_giving_nickname = $probability_giving_nickname;
        return $this;
    }

    /**
     * @return int
     */
    public function getMinThresholdMsgForIssuanceOfNickname(): int
    {
        return $this->min_threshold_msg_for_issuance_of_nickname;
    }

    /**
     * @param int $min_threshold_msg_for_issuance_of_nickname
     * @return AppealSettings
     */
    public function setMinThresholdMsgForIssuanceOfNickname(int $min_threshold_msg_for_issuance_of_nickname): AppealSettings
    {
        $this->min_threshold_msg_for_issuance_of_nickname = $min_threshold_msg_for_issuance_of_nickname;
        return $this;
    }

    /**
     * @return bool
     */
    public function isAlwaysUseNickname(): bool
    {
        return $this->always_use_nickname;
    }

    /**
     * @param bool $always_use_nickname
     * @return AppealSettings
     */
    public function setAlwaysUseNickname(bool $always_use_nickname): AppealSettings
    {
        $this->always_use_nickname = $always_use_nickname;
        return $this;
    }

    /**
     * @return float
     */
    public function getProbabilityUseAlterName(): float
    {
        return $this->probability_use_alter_name;
    }

    /**
     * @param float $probability_use_alter_name
     * @return AppealSettings
     */
    public function setProbabilityUseAlterName(float $probability_use_alter_name): AppealSettings
    {
        $this->probability_use_alter_name = $probability_use_alter_name;
        return $this;
    }

    /**
     * @return bool
     */
    public function isUseFullName(): bool
    {
        return $this->use_full_name;
    }

    /**
     * @param bool $use_full_name
     * @return AppealSettings
     */
    public function setUseFullName(bool $use_full_name): AppealSettings
    {
        $this->use_full_name = $use_full_name;
        return $this;
    }

}