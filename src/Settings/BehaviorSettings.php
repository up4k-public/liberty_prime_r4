<?php


namespace App\Settings;

use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Type;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class BehaviorSettings
 * @package App\Settings
 */
class BehaviorSettings extends AbstractSettings
{
    #[Serializer\Type("string")]
    public string $scenarios = 'DefaultScenario';

    #[Serializer\Type("boolean")]
    public bool $request_messages_between_posts = true;

    /**
     * @return string
     */
    public function getScenarios(): string
    {
        return $this->scenarios;
    }

    /**
     * @param string $scenarios
     *
     * @return BehaviorSettings
     */
    public function setScenarios(string $scenarios): BehaviorSettings
    {
        $this->scenarios = $scenarios;
        return $this;
    }

    /**
     * @return bool
     */
    public function isRequestMessagesBetweenPosts(): bool
    {
        return $this->request_messages_between_posts;
    }

    /**
     * @param bool $request_messages_between_posts
     * @return BehaviorSettings
     */
    public function setRequestMessagesBetweenPosts(bool $request_messages_between_posts): BehaviorSettings
    {
        $this->request_messages_between_posts = $request_messages_between_posts;
        return $this;
    }

}