<?php


namespace App\Settings;

use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Type;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Абстрактрые настройки модуля вычисляющего фразу, настройки конкрентного моудля лежат в папке Solver/UnitsSettings
 * рядом с самими модулями
 * Class AbstractSolversModuleSettings
 * @package App\Settings
 */
abstract class AbstractSolversModuleSettings extends AbstractSettings
{
    #[Serializer\Type("boolean")]
    public bool $use_module = true;

    #[Serializer\Type("integer")]
    public int $number_of_instances = 1;

    #[Serializer\Type("float")]
    public float $modifier_weight_of_final_choice = 1;

    #[Serializer\Type("integer")]
    public int $base_quality_of_choice = 20;

    #[Serializer\Type("integer")]
    public int $min_quality_of_choice_after_check_history = 5;

    #[Serializer\Type("integer")]
    public int $reuse_rate_by_quality_of_choice_in_dialog = 5;

    #[Serializer\Type("integer")]
    public int $history_depth_of_assessment = 4;

    #[Serializer\Type("integer")]
    public int $history_depth_of_assessment_with_interlocutor = 5;

    #[Serializer\Type("integer")]
    public int $reuse_not_earlier_than_after_n_steps = 0;

    #[Serializer\Type("integer")]
    public int $reuse_with_interlocutor_not_earlier_than_after_n_steps = 1;


    /**
     * @return bool
     */
    public function isUseModule(): bool
    {
        return $this->use_module;
    }

    /**
     * @param bool $use_module
     * @return AbstractSolversModuleSettings
     */
    public function setUseModule(bool $use_module): AbstractSolversModuleSettings
    {
        $this->use_module = $use_module;
        return $this;
    }

    /**
     * @return int
     */
    public function getNumberOfInstances(): int
    {
        return $this->number_of_instances;
    }

    /**
     * @param int $number_of_instances
     * @return AbstractSolversModuleSettings
     */
    public function setNumberOfInstances(int $number_of_instances): AbstractSolversModuleSettings
    {
        $this->number_of_instances = $number_of_instances;
        return $this;
    }

    /**
     * @return float|int
     */
    public function getModifierWeightOfFinalChoice(): float|int
    {
        return $this->modifier_weight_of_final_choice;
    }

    /**
     * @param float|int $modifier_weight_of_final_choice
     * @return AbstractSolversModuleSettings
     */
    public function setModifierWeightOfFinalChoice(float|int $modifier_weight_of_final_choice): AbstractSolversModuleSettings
    {
        $this->modifier_weight_of_final_choice = $modifier_weight_of_final_choice;
        return $this;
    }

    /**
     * @return int
     */
    public function getBaseQualityOfChoice(): int
    {
        return $this->base_quality_of_choice;
    }

    /**
     * @param int $base_quality_of_choice
     * @return AbstractSolversModuleSettings
     */
    public function setBaseQualityOfChoice(int $base_quality_of_choice): AbstractSolversModuleSettings
    {
        $this->base_quality_of_choice = $base_quality_of_choice;
        return $this;
    }

    /**
     * @return int
     */
    public function getMinQualityOfChoiceAfterCheckHistory(): int
    {
        return $this->min_quality_of_choice_after_check_history;
    }

    /**
     * @param int $min_quality_of_choice_after_check_history
     * @return AbstractSolversModuleSettings
     */
    public function setMinQualityOfChoiceAfterCheckHistory(int $min_quality_of_choice_after_check_history): AbstractSolversModuleSettings
    {
        $this->min_quality_of_choice_after_check_history = $min_quality_of_choice_after_check_history;
        return $this;
    }

    /**
     * @return int
     */
    public function getReuseRateByQualityOfChoiceInDialog(): int
    {
        return $this->reuse_rate_by_quality_of_choice_in_dialog;
    }

    /**
     * @param int $reuse_rate_by_quality_of_choice_in_dialog
     * @return AbstractSolversModuleSettings
     */
    public function setReuseRateByQualityOfChoiceInDialog(int $reuse_rate_by_quality_of_choice_in_dialog): AbstractSolversModuleSettings
    {
        $this->reuse_rate_by_quality_of_choice_in_dialog = $reuse_rate_by_quality_of_choice_in_dialog;
        return $this;
    }


    /**
     * @return int
     */
    public function getHistoryDepthOfAssessment(): int
    {
        return $this->history_depth_of_assessment;
    }

    /**
     * @param int $history_depth_of_assessment
     * @return AbstractSolversModuleSettings
     */
    public function setHistoryDepthOfAssessment(int $history_depth_of_assessment): AbstractSolversModuleSettings
    {
        $this->history_depth_of_assessment = $history_depth_of_assessment;
        return $this;
    }

    /**
     * @return int
     */
    public function getHistoryDepthOfAssessmentWithInterlocutor(): int
    {
        return $this->history_depth_of_assessment_with_interlocutor;
    }

    /**
     * @param int $history_depth_of_assessment_with_interlocutor
     * @return AbstractSolversModuleSettings
     */
    public function setHistoryDepthOfAssessmentWithInterlocutor(int $history_depth_of_assessment_with_interlocutor): AbstractSolversModuleSettings
    {
        $this->history_depth_of_assessment_with_interlocutor = $history_depth_of_assessment_with_interlocutor;
        return $this;
    }

    /**
     * @return int
     */
    public function getReuseNotEarlierThanAfterNSteps(): int
    {
        return $this->reuse_not_earlier_than_after_n_steps;
    }

    /**
     * @param int $reuse_not_earlier_than_after_n_steps
     * @return AbstractSolversModuleSettings
     */
    public function setReuseNotEarlierThanAfterNSteps(int $reuse_not_earlier_than_after_n_steps): AbstractSolversModuleSettings
    {
        $this->reuse_not_earlier_than_after_n_steps = $reuse_not_earlier_than_after_n_steps;
        return $this;
    }

    /**
     * @return int
     */
    public function getReuseWithInterlocutorNotEarlierThanAfterNSteps(): int
    {
        return $this->reuse_with_interlocutor_not_earlier_than_after_n_steps;
    }

    /**
     * @param int $reuse_with_interlocutor_not_earlier_than_after_n_steps
     * @return AbstractSolversModuleSettings
     */
    public function setReuseWithInterlocutorNotEarlierThanAfterNSteps(int $reuse_with_interlocutor_not_earlier_than_after_n_steps): AbstractSolversModuleSettings
    {
        $this->reuse_with_interlocutor_not_earlier_than_after_n_steps = $reuse_with_interlocutor_not_earlier_than_after_n_steps;
        return $this;
    }

}