<?php


namespace App\Settings;

use App\Dto\Api\ApiDto;
use App\Factories\UnitsSettingsFactory;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Type;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Settings
 * Класс агрегатор со всеми настройками; Просто потому, что мне так удобно
 * @package App\Settings
 */
class Settings extends ApiDto
{
    #[Serializer\Type("App\Settings\IgnoreSettings")]
    public IgnoreSettings $ignore_settings;

    #[Serializer\Type("App\Settings\AppealSettings")]
    public AppealSettings $appeal_settings;

    #[Serializer\Type("App\Settings\BehaviorSettings")]
    public BehaviorSettings $behavior_settings;

    #[Serializer\Type("App\Settings\DelaySettings")]
    public DelaySettings $delay_settings;

    #[Serializer\Type("App\Settings\HistorySettings")]
    public HistorySettings $history_settings;

    #[Serializer\Type("App\Settings\RequirementsForInterlocutorSettings")]
    public RequirementsForInterlocutorSettings $requirements_for_interlocutor_settings;

    #[Serializer\Type("array")]
    public ?array $module_settings = [];


    /**
     * @return IgnoreSettings
     */
    public function getIgnoreSettings(): IgnoreSettings
    {
        return $this->ignore_settings;
    }

    /**
     * @param IgnoreSettings $ignore_settings
     * @return Settings
     */
    public function setIgnoreSettings(IgnoreSettings $ignore_settings): Settings
    {
        $this->ignore_settings = $ignore_settings;
        return $this;
    }

    /**
     * @return AppealSettings
     */
    public function getAppealSettings(): AppealSettings
    {
        return $this->appeal_settings;
    }

    /**
     * @param AppealSettings $appeal_settings
     * @return Settings
     */
    public function setAppealSettings(AppealSettings $appeal_settings): Settings
    {
        $this->appeal_settings = $appeal_settings;
        return $this;
    }

    /**
     * @return BehaviorSettings
     */
    public function getBehaviorSettings(): BehaviorSettings
    {
        return $this->behavior_settings;
    }

    /**
     * @param BehaviorSettings $behavior_settings
     * @return Settings
     */
    public function setBehaviorSettings(BehaviorSettings $behavior_settings): Settings
    {
        $this->behavior_settings = $behavior_settings;
        return $this;
    }

    /**
     * @return DelaySettings
     */
    public function getDelaySettings(): DelaySettings
    {
        return $this->delay_settings;
    }

    /**
     * @param DelaySettings $delay_settings
     * @return Settings
     */
    public function setDelaySettings(DelaySettings $delay_settings): Settings
    {
        $this->delay_settings = $delay_settings;
        return $this;
    }

    /**
     * @return HistorySettings
     */
    public function getHistorySettings(): HistorySettings
    {
        return $this->history_settings;
    }

    /**
     * @param HistorySettings $history_settings
     * @return Settings
     */
    public function setHistorySettings(HistorySettings $history_settings): Settings
    {
        $this->history_settings = $history_settings;
        return $this;
    }

    /**
     * @return RequirementsForInterlocutorSettings
     */
    public function getRequirementsForInterlocutorSettings(): RequirementsForInterlocutorSettings
    {
        return $this->requirements_for_interlocutor_settings;
    }

    /**
     * @param RequirementsForInterlocutorSettings $requirements_for_interlocutor_settings
     * @return Settings
     */
    public function setRequirementsForInterlocutorSettings(RequirementsForInterlocutorSettings $requirements_for_interlocutor_settings): Settings
    {
        $this->requirements_for_interlocutor_settings = $requirements_for_interlocutor_settings;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getModuleSettings(): ?array
    {
        return $this->module_settings;
    }

    /**
     * @param array|null $module_settings
     * @return Settings
     */
    public function setModuleSettings(?array $module_settings): Settings
    {
        $this->module_settings = $module_settings;
        return $this;
    }

    /**
     * @param string $class_name
     * @return mixed
     */
    public function getModuleSettingsByName(string $class_name): AbstractSolversModuleSettings
    {
        //todo исправить кемел кейс входного json совпадающий с названием класса настроек конкретного модуля
        return $this->getModuleSettings()[UnitsSettingsFactory::generateKey($class_name) . UnitsSettingsFactory::POSTFIX];
    }

}