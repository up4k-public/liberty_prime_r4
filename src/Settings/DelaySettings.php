<?php


namespace App\Settings;

use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Type;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class DelaySettings
 * @package App\Settings
 */
class DelaySettings extends AbstractSettings
{
    #[Serializer\Type("integer")]
    public int $min_time_typing_message = 2;

    #[Serializer\Type("integer")]
    public int $max_time_typing_message = 25;

    #[Serializer\Type("integer")]
    public int $speed_typing_per_min = 440;

    #[Serializer\Type("integer")]
    public int $lag_between_post = 5;

    #[Serializer\Type("boolean")]
    public bool $cumulative_effect_of_delay_in_waiting_the_next_cycle = true;

    #[Serializer\Type("integer")]
    public int $max_delay_in_waiting_the_next_cycle = 15;

    #[Serializer\Type("integer")]
    public int $delay_in_waiting_the_next_cycle = 5;

    /**
     * @return int
     */
    public function getSpeedTypingPerMin(): int
    {
        return $this->speed_typing_per_min;
    }

    /**
     * @param int $speed_typing_per_min
     * @return DelaySettings
     */
    public function setSpeedTypingPerMin(int $speed_typing_per_min): DelaySettings
    {
        $this->speed_typing_per_min = $speed_typing_per_min;
        return $this;
    }

    /**
     * @return int
     */
    public function getMinTimeTypingMessage(): int
    {
        return $this->min_time_typing_message;
    }

    /**
     * @param int $min_time_typing_message
     * @return DelaySettings
     */
    public function setMinTimeTypingMessage(int $min_time_typing_message): DelaySettings
    {
        $this->min_time_typing_message = $min_time_typing_message;
        return $this;
    }

    /**
     * @return int
     */
    public function getMaxTimeTypingMessage(): int
    {
        return $this->max_time_typing_message;
    }

    /**
     * @param int $max_time_typing_message
     * @return DelaySettings
     */
    public function setMaxTimeTypingMessage(int $max_time_typing_message): DelaySettings
    {
        $this->max_time_typing_message = $max_time_typing_message;
        return $this;
    }

    /**
     * @return int
     */
    public function getLagBetweenPost(): int
    {
        return $this->lag_between_post;
    }

    /**
     * @param int $lag_between_post
     * @return DelaySettings
     */
    public function setLagBetweenPost(int $lag_between_post): DelaySettings
    {
        $this->lag_between_post = $lag_between_post;
        return $this;
    }

    /**
     * @return bool
     */
    public function isCumulativeEffectOfDelayInWaitingTheNextCycle(): bool
    {
        return $this->cumulative_effect_of_delay_in_waiting_the_next_cycle;
    }

    /**
     * @param bool $cumulative_effect_of_delay_in_waiting_the_next_cycle
     * @return DelaySettings
     */
    public function setCumulativeEffectOfDelayInWaitingTheNextCycle(bool $cumulative_effect_of_delay_in_waiting_the_next_cycle): DelaySettings
    {
        $this->cumulative_effect_of_delay_in_waiting_the_next_cycle = $cumulative_effect_of_delay_in_waiting_the_next_cycle;
        return $this;
    }

    /**
     * @return int
     */
    public function getMaxDelayInWaitingTheNextCycle(): int
    {
        return $this->max_delay_in_waiting_the_next_cycle;
    }

    /**
     * @param int $max_delay_in_waiting_the_next_cycle
     * @return DelaySettings
     */
    public function setMaxDelayInWaitingTheNextCycle(int $max_delay_in_waiting_the_next_cycle): DelaySettings
    {
        $this->max_delay_in_waiting_the_next_cycle = $max_delay_in_waiting_the_next_cycle;
        return $this;
    }

    /**
     * @return int
     */
    public function getDelayInWaitingTheNextCycle(): int
    {
        return $this->delay_in_waiting_the_next_cycle;
    }

    /**
     * @param int $delay_in_waiting_the_next_cycle
     * @return DelaySettings
     */
    public function setDelayInWaitingTheNextCycle(int $delay_in_waiting_the_next_cycle): DelaySettings
    {
        $this->delay_in_waiting_the_next_cycle = $delay_in_waiting_the_next_cycle;
        return $this;
    }

}