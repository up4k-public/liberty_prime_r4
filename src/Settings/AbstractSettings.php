<?php


namespace App\Settings;

use App\Dto\Api\ApiDto;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Type;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class AbstractSettings
 * @package App\Settings
 */
abstract class AbstractSettings extends ApiDto
{

}