<?php


namespace App\Settings;

use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Type;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class HistorySettings
 * @package App\Settings
 */
class HistorySettings extends AbstractSettings
{
    #[Serializer\Type("integer")]
    public int $max_count_of_message_in_dialog_for_analysis = 500;

    #[Serializer\Type("integer")]
    public int $number_of_our_recent_posts = 1000;

    /**
     * @return int
     */
    public function getMaxCountOfMessageInDialogForAnalysis(): int
    {
        return $this->max_count_of_message_in_dialog_for_analysis;
    }

    /**
     * @param int $max_count_of_message_in_dialog_for_analysis
     * @return HistorySettings
     */
    public function setMaxCountOfMessageInDialogForAnalysis(int $max_count_of_message_in_dialog_for_analysis): HistorySettings
    {
        $this->max_count_of_message_in_dialog_for_analysis = $max_count_of_message_in_dialog_for_analysis;
        return $this;
    }

    /**
     * @return int
     */
    public function getNumberOfOurRecentPosts(): int
    {
        return $this->number_of_our_recent_posts;
    }

    /**
     * @param int $number_of_our_recent_posts
     * @return HistorySettings
     */
    public function setNumberOfOurRecentPosts(int $number_of_our_recent_posts): HistorySettings
    {
        $this->number_of_our_recent_posts = $number_of_our_recent_posts;
        return $this;
    }

}