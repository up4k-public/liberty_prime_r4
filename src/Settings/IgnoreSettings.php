<?php


namespace App\Settings;

use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Type;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class IgnoreSettings
 * Эти настройки отвечают за то, чтобы в больших и активных диалогах бот не писал всем подряд
 * @package App\Settings
 */
class IgnoreSettings extends AbstractSettings
{
    #[Serializer\Type("integer")]
    public ?int $min_msg_will_never_be_ignored = 2;

    #[Serializer\Type("integer")]
    public ?int $max_msg_will_be_processed = 5;

    #[Serializer\Type("float")]
    public ?float $basic_probability_of_ignoring_when_exceeding_the_limit = 20.0;

    #[Serializer\Type("float")]
    public ?float $coefficient_by_increment_of_probability = 20.0;

    #[Serializer\Type("boolean")]
    public ?bool $always_respond_to_requests = false;

    #[Serializer\Type("float")]
    public float $probability_add_forward = 0.35;

    /**
     * @return int|null
     */
    public function getMinMsgWillNeverBeIgnored(): ?int
    {
        return $this->min_msg_will_never_be_ignored;
    }

    /**
     * @param int|null $min_msg_will_never_be_ignored
     * @return IgnoreSettings
     */
    public function setMinMsgWillNeverBeIgnored(?int $min_msg_will_never_be_ignored): IgnoreSettings
    {
        $this->min_msg_will_never_be_ignored = $min_msg_will_never_be_ignored;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getMaxMsgWillBeProcessed(): ?int
    {
        return $this->max_msg_will_be_processed;
    }

    /**
     * @param int|null $max_msg_will_be_processed
     * @return IgnoreSettings
     */
    public function setMaxMsgWillBeProcessed(?int $max_msg_will_be_processed): IgnoreSettings
    {
        $this->max_msg_will_be_processed = $max_msg_will_be_processed;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getBasicProbabilityOfIgnoringWhenExceedingTheLimit(): ?float
    {
        return $this->basic_probability_of_ignoring_when_exceeding_the_limit;
    }

    /**
     * @param float|null $basic_probability_of_ignoring_when_exceeding_the_limit
     * @return IgnoreSettings
     */
    public function setBasicProbabilityOfIgnoringWhenExceedingTheLimit(?float $basic_probability_of_ignoring_when_exceeding_the_limit): IgnoreSettings
    {
        $this->basic_probability_of_ignoring_when_exceeding_the_limit = $basic_probability_of_ignoring_when_exceeding_the_limit;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getCoefficientByIncrementOfProbability(): ?float
    {
        return $this->coefficient_by_increment_of_probability;
    }

    /**
     * @param float|null $coefficient_by_increment_of_probability
     * @return IgnoreSettings
     */
    public function setCoefficientByIncrementOfProbability(?float $coefficient_by_increment_of_probability): IgnoreSettings
    {
        $this->coefficient_by_increment_of_probability = $coefficient_by_increment_of_probability;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getAlwaysRespondToRequests(): ?bool
    {
        return $this->always_respond_to_requests;
    }

    /**
     * @param bool|null $always_respond_to_requests
     * @return IgnoreSettings
     */
    public function setAlwaysRespondToRequests(?bool $always_respond_to_requests): IgnoreSettings
    {
        $this->always_respond_to_requests = $always_respond_to_requests;
        return $this;
    }

    /**
     * @return float
     */
    public function getProbabilityAddForward(): float
    {
        return $this->probability_add_forward;
    }

    /**
     * @param float $probability_add_forward
     * @return IgnoreSettings
     */
    public function setProbabilityAddForward(float $probability_add_forward): IgnoreSettings
    {
        $this->probability_add_forward = $probability_add_forward;
        return $this;
    }



}