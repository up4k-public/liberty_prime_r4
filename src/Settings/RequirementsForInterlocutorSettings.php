<?php


namespace App\Settings;

use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Type;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Настройки ограничивающие круг собеседников для бота
 * Class RequirementsForInterlocutorSettings
 * @package App\Settings
 */
class RequirementsForInterlocutorSettings extends AbstractSettings
{
    const CHILD_AGE = 18; //todo вынести в найстройку 16...21?
    const OLD_MAN_AGE = 60; //todo вынести в настройку(?)

    #[Serializer\Type("boolean")]
    #[Assert\IsTrue(message: 'Ignore child settings must be true. No way')]
    public bool $ignore_child = true;

    #[Serializer\Type("boolean")]
    public bool $ignore_unknown_age = false;

    #[Serializer\Type("boolean")]
    public bool $ignore_women = false;

    #[Serializer\Type("boolean")]
    public bool $ignore_our_friends = true;

    #[Serializer\Type("boolean")]
    public bool $ignore_old_man = false;

    #[Serializer\Type("boolean")]
    public bool $use_white_list = false;

    #[Serializer\Type("boolean")]
    public bool $use_black_list = false;

    #[Serializer\Type("array<string>")]
    public array $white_list_external_ids = [];

    #[Serializer\Type("array<string>")]
    public array $black_list_external_ids = [];

    #[Assert\IsTrue(message: 'Can not use black and white list at the same time.')]
    public function isNotUseBlackAndWhiteLists(): bool
    {
        if ($this->use_black_list && $this->use_white_list) {
            return false;
        }
        return true;
    }

    /**
     * @return bool
     */
    public function isIgnoreChild(): bool
    {
        return $this->ignore_child;
    }

    /**
     * @param bool $ignore_child
     * @return RequirementsForInterlocutorSettings
     */
    public function setIgnoreChild(bool $ignore_child): RequirementsForInterlocutorSettings
    {
        $this->ignore_child = $ignore_child;
        return $this;
    }

    /**
     * @return bool
     */
    public function isIgnoreUnknownAge(): bool
    {
        return $this->ignore_unknown_age;
    }

    /**
     * @param bool $ignore_unknown_age
     * @return RequirementsForInterlocutorSettings
     */
    public function setIgnoreUnknownAge(bool $ignore_unknown_age): RequirementsForInterlocutorSettings
    {
        $this->ignore_unknown_age = $ignore_unknown_age;
        return $this;
    }

    /**
     * @return bool
     */
    public function isIgnoreWomen(): bool
    {
        return $this->ignore_women;
    }

    /**
     * @param bool $ignore_women
     * @return RequirementsForInterlocutorSettings
     */
    public function setIgnoreWomen(bool $ignore_women): RequirementsForInterlocutorSettings
    {
        $this->ignore_women = $ignore_women;
        return $this;
    }

    /**
     * @return bool
     */
    public function isIgnoreOurFriends(): bool
    {
        return $this->ignore_our_friends;
    }

    /**
     * @param bool $ignore_our_friends
     * @return RequirementsForInterlocutorSettings
     */
    public function setIgnoreOurFriends(bool $ignore_our_friends): RequirementsForInterlocutorSettings
    {
        $this->ignore_our_friends = $ignore_our_friends;
        return $this;
    }

    /**
     * @return bool
     */
    public function isIgnoreOldMan(): bool
    {
        return $this->ignore_old_man;
    }

    /**
     * @param bool $ignore_old_man
     * @return RequirementsForInterlocutorSettings
     */
    public function setIgnoreOldMan(bool $ignore_old_man): RequirementsForInterlocutorSettings
    {
        $this->ignore_old_man = $ignore_old_man;
        return $this;
    }

    /**
     * @return bool
     */
    public function isUseWhiteList(): bool
    {
        return $this->use_white_list;
    }

    /**
     * @param bool $use_white_list
     * @return RequirementsForInterlocutorSettings
     */
    public function setUseWhiteList(bool $use_white_list): RequirementsForInterlocutorSettings
    {
        $this->use_white_list = $use_white_list;
        return $this;
    }

    /**
     * @return bool
     */
    public function isUseBlackList(): bool
    {
        return $this->use_black_list;
    }

    /**
     * @param bool $use_black_list
     * @return RequirementsForInterlocutorSettings
     */
    public function setUseBlackList(bool $use_black_list): RequirementsForInterlocutorSettings
    {
        $this->use_black_list = $use_black_list;
        return $this;
    }

    /**
     * @return array
     */
    public function getWhiteListExternalIds(): array
    {
        return $this->white_list_external_ids;
    }

    /**
     * @param array $white_list_external_ids
     * @return RequirementsForInterlocutorSettings
     */
    public function setWhiteListExternalIds(array $white_list_external_ids): RequirementsForInterlocutorSettings
    {
        $this->white_list_external_ids = $white_list_external_ids;
        return $this;
    }

    /**
     * @return array
     */
    public function getBlackListExternalIds(): array
    {
        return $this->black_list_external_ids;
    }

    /**
     * @param array $black_list_external_ids
     * @return RequirementsForInterlocutorSettings
     */
    public function setBlackListExternalIds(array $black_list_external_ids): RequirementsForInterlocutorSettings
    {
        $this->black_list_external_ids = $black_list_external_ids;
        return $this;
    }

}