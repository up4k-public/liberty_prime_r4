<?php

namespace App\EventListeners;

use App\Dto\Api\BaseResponseFormat;
use App\Dto\Api\Error;
use App\Dto\Api\ValidationFieldsError;
use App\Exceptions\ApiValidationFieldsException;
use JMS\Serializer\ArrayTransformerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;

/**
 * Class ControllerResponseSubscriber
 * @package App\EventListeners
 */
class ControllerResponseSubscriber implements EventSubscriberInterface
{
    public function __construct(private ArrayTransformerInterface $transformer)
    {
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => 'success',
            KernelEvents::EXCEPTION => 'error'
        ];
    }

    /**
     * @param ViewEvent $event
     */
    public function success(ViewEvent $event)
    {

        $responseData = (new BaseResponseFormat())
            ->setSuccess(true)
            ->setData($event->getControllerResult());

        $responseData = $this->transformer->toArray($responseData);

        $event->setResponse(new JsonResponse($responseData));
    }

    /**
     * Cюда не доходят Lexik\\Bundle\\JWTAuthenticationBundle\\Exception\\InvalidTokenException
     * @param ExceptionEvent $event
     */
    public function error(ExceptionEvent $event)
    {
        $exception = $event->getThrowable();
        $responseData = (new BaseResponseFormat())
            ->setSuccess(false)
            ->setError((new Error())
                ->setMessage($exception->getMessage())
                ->setCode($exception->getCode())
                ->setFile($exception->getFile())
                ->setLine($exception->getLine()));

        if ($exception instanceof ApiValidationFieldsException) {
            /** @var ValidationFieldsError $fieldError */
            foreach ($exception->getFields() as &$fieldError) {
                $fieldError->setCode(0); //todo коды для перевода на фронте
            }
            unset($fieldError);
            $responseData->setErrors($exception->getFields());
        }

        $responseData = $this->transformer->toArray($responseData);

        $event->setResponse(new JsonResponse($responseData));
    }

}
