<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<User>
 *
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    public function save(User $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(User $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @param string $name
     * @return User|null
     */
    public function getByName(string $name): ?User
    {
        return $this->findOneBy(['name' => $name]);
    }

    /**
     * @param int $limit
     * @param int $offset
     * @param string|null $sort_field
     * @param string $sort_by
     * @return array
     */
    public function getList(int $limit, int $offset, ?string $sort_field, string $sort_by):array
    {
        $query = $this->createQueryBuilder('u')
            ->setFirstResult($offset)
            ->setMaxResults($limit);
        if (!empty($sort_field)) {
            $query->orderBy('u.' . $sort_field, $sort_by);
        }

        $data = $query->getQuery()
            ->getResult();

        if (empty($data)) {
            return [];
        }
        return $data;
    }

    /**
     * @return int
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function getTotalCount(): int
    {
        return $this->createQueryBuilder('u')
            ->select('count(u.id)')
            ->getQuery()->getSingleScalarResult();
    }


}
