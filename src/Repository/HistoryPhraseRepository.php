<?php

namespace App\Repository;

use App\Entity\HistoryPhrase;
use App\Exceptions\Phrase\PhraseHistoryNotFoundException;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<HistoryPhrase>
 *
 * @method HistoryPhrase|null find($id, $lockMode = null, $lockVersion = null)
 * @method HistoryPhrase|null findOneBy(array $criteria, array $orderBy = null)
 * @method HistoryPhrase[]    findAll()
 * @method HistoryPhrase[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HistoryPhraseRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, HistoryPhrase::class);
    }

    public function save(HistoryPhrase $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(HistoryPhrase $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @param int $id
     * @return HistoryPhrase
     * @throws PhraseHistoryNotFoundException
     */
    public function getById(int $id):HistoryPhrase
    {
        $history = $this->find($id);
        if (empty($history)) {
            throw new PhraseHistoryNotFoundException();
        }
        return $history;
    }

}
