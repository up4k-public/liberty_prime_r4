<?php

namespace App\Repository;

use App\Dto\Api\Phrase\Request\GetPhraseList;
use App\Dto\Api\Request\Filter;
use App\Entity\Phrase;
use App\Exceptions\Phrase\PhraseNotFoundException;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Phrase>
 *
 * @method Phrase|null find($id, $lockMode = null, $lockVersion = null)
 * @method Phrase|null findOneBy(array $criteria, array $orderBy = null)
 * @method Phrase[]    findAll()
 * @method Phrase[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PhraseRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Phrase::class);
    }

    public function save(Phrase $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Phrase $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @param int $id
     * @return Phrase
     * @throws PhraseNotFoundException
     */
    public function getById(int $id): Phrase
    {
        $phrase = $this->find($id);
        if (empty($phrase)) {
            throw new PhraseNotFoundException();
        }
        return $phrase;
    }

    /**
     * @param string $text
     * @return Phrase|null
     */
    public function getByText(string $text):?Phrase
    {
        $phrase = $this->findOneBy(['text' => $text]);
        return $phrase;
    }

    /**
     * Получение списка фраз по тегам, без учёта тех, что уже использовали
     * Поле tags это jsonb. Да мы по нему ищем. Нет, тем кто на stackoverflow пишет, что невозможно подружить Symfony ORM и Postgres:jsonb врут
     * @param string $module
     * @param array|null $tags
     * @param array|null $were_used_ids
     * @return mixed
     */
    public function getByModulesAndTagsBesidesId(string $module, ?array $tags = null, ?array $were_used_ids = null)
    {
        //Без вот этого sql не работает, есть в миграциях. Очень сильное колдунство
        //CREATE OPERATOR ~@| (LEFTARG = jsonb, RIGHTARG = text[], PROCEDURE = jsonb_exists_any);
        $sql = "SELECT * FROM phrase as p WHERE p.modules::jsonb ~@| array[?]";

        $rsm = $this->getResultSetMapping();

        if (!empty($tags)) {
            $sql .= " AND p.tags::jsonb ~@| array[?]";
        }

        if (!empty($were_used_ids)) {
            $sql .= " AND p.id not in (?)";
        }

        $query = $this->_em->createNativeQuery($sql, $rsm);

        $i = 2;
        $query->setParameter(1, [$module]);
        if (!empty($tags)) {
            $query->setParameter($i, $tags);
            $i++;
        }
        if (!empty($were_used_ids)) {
            $query->setParameter($i, $were_used_ids);
        }

        return $query->getResult();
    }


    /**
     * Возвращает записи, отфильрованных по jsonb полям, обычным полям с использованием native-sql
     * См addAndWhereByFiltersNativeSql()
     * @param GetPhraseList $input
     * @return array
     */
    public function getListWithFilters(GetPhraseList $input): array
    {
        $sql = $this->addAndWhereByFiltersNativeSql($this->getBaseSqlForList(), $input->getFilters());
        $sql = $this->addOrderByNativeSql($sql, $input->getSortField(), $input->getSortBy());
        $sql = $this->addLimitOffsetNativeSql($sql, $input->getLimit(), $input->getOffset());

        $query = $this->_em->createNativeQuery($sql, $this->getResultSetMapping());

        /** @var Filter $filter */
        foreach ($input->getFilters() as $key => $filter) {
            $query->setParameter($key, $filter->getValue());
        }

        return $query
            ->getResult();
    }


    /**
     * Возвращает кол-во записей, отфильрованных по jsonb полям, обычным полям с использованием native-sql
     * См addAndWhereByFiltersNativeSql()
     *
     * TODO КРИТИЧНО
     * TODO Боль, страдние и ады, если с getListWithFilters проблем нет, то засунуть в native-sql count(*) as cnt не получается.
     * TODO Точнее получается, но тогда фильтры в IN (?) и ::jsonb ~@| array[?]  лезет только стринга, никаких массивов или уязвимость к sql инъекциям
     * TODO ??? может сделать Entity с полем cnt и мапить в него ???
     *
     * @param GetPhraseList $input
     * @return int
     */
    public function getCountWithFilter(GetPhraseList $input): int
    {
        $sql = $this->addAndWhereByFiltersNativeSql($this->getBaseSqlForList(), $input->getFilters());

        $query = $this->_em->createNativeQuery($sql, $this->getResultSetMapping());
        /** @var Filter $filter */
        foreach ($input->getFilters() as $key => $filter) {
            $query->setParameter($key, $filter->getValue());
        }
        $data = $query
            ->getResult();
        if (empty($data)) {
            return 0;
        }
        return count($data);
    }

    /**
     * @param array $filters
     * @return QueryBuilder
     */
    private function makeQueryWithFilter(array $filters): QueryBuilder
    {
        $query = $this->createQueryBuilder('p');
        /** @var Filter $filter */
        foreach ($filters as $key => $filter) {
            $query
                ->andWhere("p.{$filter->getName()} IN (:param{$key})")
                ->setParameter("param" . $key, $filter->getValue());
        }

        return $query;
    }

    public function getAllWithTriggersBesidesId(?array $were_used_ids = null)
    {
        $query = $this->createQueryBuilder('p')
            ->andWhere('p.triggers is not NULL');

        if (!empty($were_used_ids)) {
            $query
                ->andWhere('p.id not in (:ids)')
                ->setParameter('ids', $were_used_ids);
        }

        return $query
            ->getQuery()
            ->getResult();
    }

    public function getAllWithoutTriggersBesidesId(?array $were_used_ids = null)
    {
        $query = $this->createQueryBuilder('p')
            ->andWhere('p.triggers is NULL');

        if (!empty($were_used_ids)) {
            $query
                ->andWhere('p.id not in (:ids)')
                ->setParameter('ids', $were_used_ids);
        }

        return $query
            ->getQuery()
            ->getResult();
    }

    /**
     * Без мапера тут никак, точно никак. Зуб даю -- никак, я пробовал.
     * https://www.doctrine-project.org/projects/doctrine-orm/en/current/reference/native-sql.html#examples
     * @return ResultSetMapping
     */
    private function getResultSetMapping()
    {
        $rsm = new ResultSetMapping();
        $rsm->addEntityResult(Phrase::class, 'p');

        $rsm->addFieldResult('p', 'id', 'id');
        $rsm->addFieldResult('p', 'text', 'text');
        $rsm->addFieldResult('p', 'sex', 'sex');
        $rsm->addFieldResult('p', 'params', 'params');
        $rsm->addFieldResult('p', 'assessment', 'assessment');
        $rsm->addFieldResult('p', 'triggers', 'triggers');
        $rsm->addFieldResult('p', 'ts_created', 'ts_created');
        $rsm->addFieldResult('p', 'ts_updated', 'ts_updated');
        $rsm->addFieldResult('p', 'updated_by', 'updated_by');
        $rsm->addFieldResult('p', 'modules', 'modules');
        $rsm->addFieldResult('p', 'tags', 'tags');
        return $rsm;
    }


    /**
     * Возвращаем строку SQL-запроса для фильтрации фраз по полям, включая поля типа jsonb для native-sql
     *
     * https://www.doctrine-project.org/projects/doctrine-orm/en/current/reference/native-sql.html#examples
     * Поля tags,modules,triggers это jsonb. Да мы по нему ищем. Нет, тем кто на stackoverflow пишет, что невозможно подружить Symfony ORM и Postgres:jsonb врут
     * Без вот этого sql не работает, есть в миграциях. Очень сильное колдунство -- "CREATE OPERATOR ~@| (LEFTARG = jsonb, RIGHTARG = text[], PROCEDURE = jsonb_exists_any)";
     * @param array $filters
     * @return string
     */
    private function addAndWhereByFiltersNativeSql(string $sql, array $filters): string
    {
        if (empty($filters)) {
            return $sql;
        }
        $flag_use_where = false;
        /** @var Filter $filter */
        foreach ($filters as $filter) {

            if ($flag_use_where) {
                $sql .= ' AND ';
            } else {
                $sql .= ' WHERE ';
                $flag_use_where = true;
            }

            if (in_array($filter->getName(), ['tags', 'modules', 'triggers'])) {
                $sql .= "p.{$filter->getName()}::jsonb ~@| array[?]";
            } else {
                $sql .= "p.{$filter->getName()} IN (?)";
            }

        }
        return $sql;
    }

    /**
     * @param string $sql
     * @param int $limit
     * @param int $offset
     * @return string
     */
    private function addLimitOffsetNativeSql(string $sql, int $limit, int $offset): string
    {
        return $sql . " LIMIT {$limit} OFFSET {$offset}";
    }

    private function addOrderByNativeSql(string $sql, string $field, string $sort)
    {
        return $sql . " ORDER BY {$field} {$sort}";
    }

    private function getBaseSqlForList(): string
    {
        return "SELECT * FROM phrase as p";
    }

    private function getBaseSqlForCount(): string
    {
        return "SELECT count(*) as cnt FROM phrase as p";
    }

}
