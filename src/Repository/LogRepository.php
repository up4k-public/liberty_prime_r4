<?php

namespace App\Repository;

use App\Entity\Assault;
use App\Entity\Log;
use App\History\AssaultLog\Events\BaseAssaultLogEvent;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Exception;

/**
 * @extends ServiceEntityRepository<Log>
 *
 * @method Log|null find($id, $lockMode = null, $lockVersion = null)
 * @method Log|null findOneBy(array $criteria, array $orderBy = null)
 * @method Log[]    findAll()
 * @method Log[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LogRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Log::class);
    }

    public function save(Log $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Log $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @param BaseAssaultLogEvent|null $event
     * @param Assault $assault
     * @throws Exception
     */
    public function saveByEvent(?BaseAssaultLogEvent $event, Assault $assault)
    {
        if (empty($event)) {
            return;
        }

        $log = (new Log())
            ->setAssault($assault)
            ->setType($event->getMessage())
            ->setCode($event->getCode())
            ->setContext(json_decode(json_encode($event->getContext()), true))
            ->setTsCreated(new DateTime($event->getTsCreated()));
        $this->getEntityManager()->persist($log);
        $this->save($log, true);
    }

}
