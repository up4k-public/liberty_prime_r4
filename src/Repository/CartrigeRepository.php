<?php

namespace App\Repository;

use App\Entity\Cartrige;
use App\Entity\User;
use App\Exceptions\User\CartrigeIsExistException;
use App\Exceptions\User\NotFoundCartrigeById;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Cartrige>
 *
 * @method Cartrige|null find($id, $lockMode = null, $lockVersion = null)
 * @method Cartrige|null findOneBy(array $criteria, array $orderBy = null)
 * @method Cartrige[]    findAll()
 * @method Cartrige[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CartrigeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Cartrige::class);
    }

    public function save(Cartrige $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Cartrige $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function refresh(Cartrige $entity): Cartrige
    {
        $this->getEntityManager()->refresh($entity);
        return $entity;
    }

    public function getByExternalIdAndType(string $externalId, string $integrationType): ?Cartrige
    {
        return $this->findOneBy(['external_id' => $externalId, 'integration_type' => $integrationType]);
    }

    /**
     * @param int $id
     * @return Cartrige
     * @throws NotFoundCartrigeById
     */
    public function getById(int $id): Cartrige
    {
        $model = $this->find($id);
        if (empty($model)) {
            throw new NotFoundCartrigeById();
        }
        return $model;
    }

    /**
     * @param string $externalId
     * @param string $integrationType
     * @return bool
     * @throws CartrigeIsExistException
     */
    public function checkIsExistByExternalIdAndType(string $externalId, string $integrationType): bool
    {
        if (!empty($this->getByExternalIdAndType($externalId, $integrationType))) {
            throw new CartrigeIsExistException();
        }
        return false;
    }

}
