<?php

namespace App\Repository;

use App\Entity\Assault;
use App\Entity\Cartrige;
use App\Exceptions\Assault\AssaultNotFound;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Assault>
 *
 * @method Assault|null find($id, $lockMode = null, $lockVersion = null)
 * @method Assault|null findOneBy(array $criteria, array $orderBy = null)
 * @method Assault[]    findAll()
 * @method Assault[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AssaultRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Assault::class);
    }

    public function save(Assault $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Assault $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @param int $id
     * @return Assault
     * @throws AssaultNotFound
     */
    public function getById(int $id): Assault
    {
        $model = $this->find($id);
        if (empty($model)) {
            throw new AssaultNotFound();
        }
        return $model;
    }

    /**
     * @param Collection $cartridges
     * @param array|null $statuses
     * @param string|null $sort_field
     * @param string $sort_by
     * @param int $limit
     * @param int $offset
     * @return Assault[]
     */
    public function getByStatusAndCartridges(Collection $cartridges, ?array $statuses, ?string $sort_field, string $sort_by, int $limit, int $offset): array
    {
        $query = $this->getQueryByStatusAndCartridges($cartridges, $statuses);

        $query->setFirstResult($offset)
            ->setMaxResults($limit);

        if (!empty($sort_field)) {
            $query->orderBy('a.' . $sort_field, $sort_by);
        }

        $data = $query->getQuery()
            ->getResult();

        if (empty($data)) {
            return [];
        }
        return $data;
    }

    /**
     * @param Collection $cartridges
     * @param array|null $statuses
     * @return int|mixed|string
     * @throws NoResultException|NonUniqueResultException
     */
    public function getCountByStatusAndCartridges(Collection $cartridges, ?array $statuses)
    {
        $query = $this->getQueryByStatusAndCartridges($cartridges, $statuses);
        return $query
            ->select($query->expr()->count('a.id'))
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * @param Collection $cartridges
     * @param array|null $statuses
     * @return QueryBuilder
     */
    private function getQueryByStatusAndCartridges(Collection $cartridges, ?array $statuses):QueryBuilder
    {
        $cartridge_ids = [];
        /** @var Cartrige $cartridge */
        foreach ($cartridges as $cartridge) {
            $cartridge_ids[] = $cartridge->getId();
        }
        $params['cartrige'] = $cartridge_ids;

        $query = $this->createQueryBuilder('a')
            ->andWhere('a.cartrige in (:cartridges)')
            ->setParameter('cartridges', $cartridge_ids);

        if (!empty($statuses)) {
            $query
                ->andWhere('a.status in (:statuses)')
                ->setParameter('statuses', $statuses);
        }

        return $query;
    }
}
