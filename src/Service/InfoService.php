<?php


namespace App\Service;

use App\Dto\Api\BaseList;
use App\Dto\Api\Service\IntegrationInfo;
use App\Dto\Api\Service\ServiceList;
use App\Dto\Api\Service\Response\ServiceList as ResponseServiceList;
use App\Exceptions\Core\ScenarioNotFound;
use App\Exceptions\Core\SolverUnitsNotFound;
use App\Factories\IntegrationsFactory;
use App\Factories\ScenariosFactory;
use App\Solvers\UnitsFactory;

/**
 * Class InfoService
 * @package App\Service
 */
class InfoService
{
    /**
     * InfoService constructor.
     * @param UnitsFactory $unitsFactory
     * @param ScenariosFactory $scenariosFactory
     */
    public function __construct(
        private UnitsFactory $unitsFactory,
        private ScenariosFactory  $scenariosFactory
    )
    {
    }

    /**
     * @param ServiceList $input
     * @return ResponseServiceList
     */
    public function getIntegrationList(ServiceList $input): ResponseServiceList
    {
        $integrationInfos = [];
        $integrationNames = IntegrationsFactory::getAllNames();
        foreach ($integrationNames as $integrationName) {
            $integrationInfos[] = (new IntegrationInfo())
                ->setName($integrationName)
                ->setSubNames(IntegrationsFactory::getIntegrationSubNames($integrationName));
        }

        return (new ResponseServiceList())
            ->setCount(count(IntegrationsFactory::getAllNames()))
            ->setItems(array_slice($integrationInfos, $input->getOffset(), $input->getLimit()));
    }

    /**
     * @param ServiceList $input
     * @return ResponseServiceList
     * @throws SolverUnitsNotFound
     */
    public function getModuleList(ServiceList $input): ResponseServiceList
    {
        return (new ResponseServiceList())
            ->setCount(count($this->unitsFactory->getAllNames(true)))
            ->setItems(array_slice($this->unitsFactory->getAllNames(true), $input->getOffset(), $input->getLimit()));
    }

    /**
     * @param ServiceList $input
     * @return ResponseServiceList
     * @throws ScenarioNotFound
     */
    public function getScenariosList(ServiceList $input):ResponseServiceList
    {
        return (new ResponseServiceList())
            ->setCount(count($this->scenariosFactory->getAllNames(true)))
            ->setItems(array_slice($this->scenariosFactory->getAllNames(true), $input->getOffset(), $input->getLimit()));
    }
}