<?php


namespace App\Service;

use App\Core\Author;
use App\Core\Message;
use App\Core\ResponseMessage;
use App\History\History;
use App\Settings\DelaySettings;
use App\Settings\HistorySettings;
use App\Settings\IgnoreSettings;
use App\Settings\RequirementsForInterlocutorSettings;
use App\Settings\Settings;
use Exception;

/**
 * Class MessageService
 * @package App\Service
 */
class MessageService
{
    /**
     * @param array $messages
     * @return array
     */
    public function getExternalIds(array $messages)
    {
        $result = [];
        /** @var Message $message */
        foreach ($messages as $message) {
            $result = array_merge($result, $message->getExternalIds());
        }
        return $result;
    }

    /**
     * Берём сообщение в явном виде адресованный нашему аккаунта
     * @param array $messages
     * @param IgnoreSettings $settings
     * @return array
     */
    private function getMessageDirectToMe(array $messages, IgnoreSettings $settings): array
    {
        $result = [];
        /** @var Message $message */
        foreach ($messages as $message) {
            if ($settings->getAlwaysRespondToRequests() && $message->getDirectedToMe()) {
                $result[] = $message;
            }
        }
        return $result;
    }

    /**
     * Из списка годных к ответу сообщений выбираем несколько(согласно настроек) на которые будем отвечать.
     * @param array $messages
     * @param IgnoreSettings $settings
     * @return array
     */
    public function cutMessageByMaxLimiting(array $messages, IgnoreSettings $settings): array
    {
        $messages = array_reverse($messages);
        $result = [];
        while ((count($result) <= $settings->getMaxMsgWillBeProcessed()) && !empty($messages)) {
            $result[] = array_shift($messages);
        }
        return array_reverse($result);
    }

    /**
     * Считаем время печати (username typing...; юзернайм печатет....) с учётом размера текста
     * TODO учитывать кол-во аттачей
     * @param ResponseMessage $message
     * @param DelaySettings $delaySettings
     * @return int
     */
    public function calculateTypingTime(ResponseMessage $message, DelaySettings $delaySettings): int
    {
        $time = round(strlen($message->getText()) / ($delaySettings->getSpeedTypingPerMin() / 60));
        if ($time < $delaySettings->getMinTimeTypingMessage()) {
            return $delaySettings->getMinTimeTypingMessage();
        }

        if ($time > $delaySettings->getMaxTimeTypingMessage()) {
            return $delaySettings->getMaxTimeTypingMessage();
        }

        return $time;
    }

    /**
     * Из списка новых сообщений откидываем все, что до последнего нашего и схлопываем, применяем ограничения в соот. с настройками
     * @param array $messages
     * @param Author $selfAvatar
     * @return array
     * @throws Exception
     */
    public function getNewBatchWithPreProcessing(array $messages, Settings $settings, Author $selfAvatar): array
    {
        $messages = $this->cutOlderMyLastOne($messages, $selfAvatar);
        $messages = $this->spliceByAuthor($messages);
        $messages = $this->cutByRequirementsForInterlocutor($messages, $settings->getRequirementsForInterlocutorSettings());
        $messages = $this->cutMessageByMaxLimiting($messages, $settings->getIgnoreSettings());
        return $messages;
    }

    /**
     * Дозапрашиваем сообщения между постингом наших, тут нельзя отбросить то, что было до последнего нашего
     * Берём все не наше, кроме того не нашего, что взяли раньше (как только наткнулись на знакомое -- дальще не берём)
     * @param array $messages
     * @param Author $selfAvatar
     * @param History $history
     * @return array
     */
    public function requeryNewBatchWithPreProcessing(array $messages, Settings $settings, Author $selfAvatar, History $history): array
    {
        $messages = $this->cutByExternalIdOlderFirst($messages, $history);
        $messages = $this->cutOldMessageByHistoryId($messages, $history);
        $messages = $this->cutSelfMessage($messages, $selfAvatar);
        $messages = $this->spliceByAuthor($messages);
        $messages = $this->cutByRequirementsForInterlocutor($messages, $settings->getRequirementsForInterlocutorSettings());
        $messages = $this->cutMessageByMaxLimiting($messages, $settings->getIgnoreSettings());

        return $messages;
    }

    /**
     * Обтрасываем сообщения тех собеседников, которые не подходят нам по настройкам
     * @param array $messages
     * @param RequirementsForInterlocutorSettings $settings
     * @return array
     * @throws Exception
     */
    private function cutByRequirementsForInterlocutor(array $messages, RequirementsForInterlocutorSettings $settings): array
    {
        $result = [];
        /** @var Message $message */
        foreach ($messages as $message) {
            if ($settings->isIgnoreWomen() && $message->getAuthor()->isWomen()) {
                continue;
            }
            if ($settings->isIgnoreUnknownAge() && empty($message->getAuthor()->getBdate())) {
                continue;
            }
            if ($settings->isIgnoreChild() && $message->getAuthor()->getAge() < RequirementsForInterlocutorSettings::CHILD_AGE) {
                continue;
            }
            if ($settings->isIgnoreOldMan() && $message->getAuthor()->getAge() >= RequirementsForInterlocutorSettings::OLD_MAN_AGE) {
                continue;
            }
            if ($settings->isIgnoreUnknownAge() && empty($message->getAuthor()->getAge())) {
                continue;
            }
            if ($settings->isUseWhiteList() && !in_array($message->getAuthor()->getExternalId(), $settings->getWhiteListExternalIds())) {
                continue;
            }
            if ($settings->isUseBlackList() && in_array($message->getAuthor()->getExternalId(), $settings->getBlackListExternalIds())) {
                continue;
            }

            $result[] = $message;
        }
        return $result;
    }

    /**
     * Откидываем все сообщения, что уже были в поле нашего внимания согласно записям в Истории
     * @param array $messages
     * @param History $history
     * @return array
     */
    private function cutOldMessageByHistoryId(array $messages, History $history): array
    {
        $result = [];
        /** @var Message $message */
        foreach ($messages as $message) {
            if (!in_array($message->getExternalId(), $history->getExternalIds())) {
                $result[] = $message;
            }
        }
        return $result;
    }

    /**
     * Откидываем все сообщения, что старше сообщения, которое мы считаем стартовым
     * TODO когда появится сервис-цель с иначе упорядоченными id, то отсекать придётся по времени
     * @param array $messages
     * @param History $history
     * @return array
     */
    private function cutByExternalIdOlderFirst(array $messages, History $history): array
    {
        $result = [];
        /** @var Message $message */
        foreach ($messages as $message) {
            if ($message->getExternalId() > $history->getFirstExternalId()) {
                $result[] = $message;
            }
        }
        return $result;
    }

    /**
     * Выкидываем все сообщение от аккаунта, что сейчас заряжен
     * @param array $messages
     * @param Author $selfAvatar
     * @return array
     */
    private function cutSelfMessage(array $messages, Author $selfAvatar): array
    {
        $result = [];
        /** @var Message $message */
        foreach ($messages as $message) {
            if ($message->getAuthor()->getExternalId() != $selfAvatar->getExternalId()) {
                $result[] = $message;
            }
        }
        return $result;
    }

    /**
     * Откидываем все сообщения что были до последнего поста с текущего аккаунта
     * @param array $messages
     * @param Author $self_avatar
     * @return array
     */
    private function cutOlderMyLastOne(array $messages, Author $self_avatar): array
    {
        $result = [];
        /** @var Message $message */
        foreach ($messages as $message) {
            if ($message->getAuthorId() == $self_avatar->getExternalId()) {
                break;
            }
            $result[] = $message;
        }
        return $result;
    }

    /**
     * Несколько сообщения от одного автора подряд воспринимаем как одно (кроме сервисных)
     * @param array $messages
     * @return array
     */
    private function spliceByAuthor(array $messages): array
    {
        $result = [];
        $messages = array_reverse($messages);
        while (!empty($messages)) {
            /** @var Message $new_message */
            $new_message = array_shift($messages);
            if (empty($current_message)) {
                //первый раз нашли сообщение автора
                $current_message = $new_message;
            } elseif (($current_message->getAuthorId() === $new_message->getAuthorId()) &&
                $new_message->getIsService() === false) {
                //два подряд сообщения одного автора; сервисные сообщения -- всегда отдельные
                $current_message
                    ->addText($new_message->getText())
                    ->addActions($new_message->getActions())
                    ->addExternalId($new_message->getExternalId())
                    ->addAttachments($new_message->getAttachments());
            } else {
                //сообщение нового автора, или сервисное
                $result[] = $current_message;
                $current_message = $new_message;
            }
        }
        if (!empty($current_message)) {
            $result[] = $current_message;
        }
        return $result;
    }

}