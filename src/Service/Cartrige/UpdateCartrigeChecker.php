<?php


namespace App\Service\Cartrige;


use App\Core\Author;
use App\Dto\Api\User\Request\AddCartrige;
use App\Entity\Cartrige;
use App\Exceptions\User\AttemptToUpdateAnotherAccount;
use App\Exceptions\User\NotFoundCartrigeById;
use App\Repository\CartrigeRepository;

/**
 * Class UpdateCartrigeChecker
 * @package App\Service\Cartrige
 */
class UpdateCartrigeChecker implements CartrigeCheckInterface
{
    /**
     * CreateCartrigeChecker constructor.
     * @param CartrigeRepository $cartrigeRepository
     */
    public function __construct(
        private CartrigeRepository $cartrigeRepository
    )
    {
    }

    /**
     * @param Author $externalInfo
     * @param AddCartrige $input
     * @return bool
     * @throws AttemptToUpdateAnotherAccount
     * @throws NotFoundCartrigeById
     */
    public function check(Author $externalInfo, AddCartrige $input): bool
    {
        /** UpdateCartrige $input */
        if ($this->getCartrige($input->getId())->getExternalId() != $externalInfo->getExternalId()) {
            throw new AttemptToUpdateAnotherAccount();
        }
        return true;
    }

    /**
     * @param int|null $id
     * @return Cartrige
     * @throws NotFoundCartrigeById
     */
    public function getCartrige(?int $id = null): Cartrige
    {
        return $this->cartrigeRepository->getById($id);
    }

}