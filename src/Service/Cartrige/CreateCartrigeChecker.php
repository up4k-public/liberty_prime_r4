<?php


namespace App\Service\Cartrige;


use App\Core\Author;
use App\Dto\Api\User\Request\AddCartrige;
use App\Entity\Cartrige;
use App\Exceptions\User\CartrigeIsExistException;
use App\Repository\CartrigeRepository;

/**
 * Class CreateCartrigeChecker
 * @package App\Service\Cartrige
 */
class CreateCartrigeChecker implements CartrigeCheckInterface
{
    /**
     * CreateCartrigeChecker constructor.
     * @param CartrigeRepository $cartrigeRepository
     */
    public function __construct(
        private CartrigeRepository $cartrigeRepository
    )
    {
    }

    /**
     * @param Author $externalInfo
     * @param AddCartrige $input
     * @return bool
     * @throws CartrigeIsExistException
     */
    public function check(Author $externalInfo, AddCartrige $input):bool
    {
        return $this->cartrigeRepository->checkIsExistByExternalIdAndType($externalInfo->getExternalId(), $input->getIntegrationType());
    }

    /**
     * @param int|null $id
     * @return Cartrige
     */
    public function getCartrige(?int $id = null): Cartrige
    {
       return new Cartrige();
    }

}