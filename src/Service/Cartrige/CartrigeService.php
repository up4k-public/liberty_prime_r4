<?php


namespace App\Service\Cartrige;


use App\Core\Author;
use App\Core\Interactors\AbstractUserinfoCollector;
use App\Dto\Api\User\Request\AddCartrige;
use App\Dto\Api\User\Request\CartrigeList;
use App\Dto\Api\User\Request\UpdateCartrige;
use App\Dto\Api\User\Response\AddCartrige as AddCartrigeResponseDto;
use App\Dto\Api\User\Response\CartrigeList as ResponseCartrigeList;
use App\Dto\Api\User\Response\ShortUserInfo;
use App\Entity\Cartrige;
use App\Entity\User;
use App\Exceptions\Core\BaseCoreException;
use App\Exceptions\Integration\InvalidAuthData;
use App\Exceptions\User\AttemptToUpdateAnotherAccount;
use App\Exceptions\User\CartrigeIsExistException;
use App\Exceptions\User\NotFoundCartrigeById;
use App\Factories\AdapterFactory;
use App\Factories\IntegrationsFactory;
use App\Factories\InteractorsFactory;
use App\Integrations\Api;
use App\Repository\CartrigeRepository;
use App\Repository\UserRepository;
use App\Service\ApiService;
use DateTime;
use JMS\Serializer\SerializerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

/**
 * Class CartrigeService
 * @package App\Service
 */
class CartrigeService extends ApiService
{
    /**
     * CartrigeService constructor.
     * @param SerializerInterface $serializer
     * @param UserRepository $userRepository
     * @param IntegrationsFactory $integrationsFactory
     * @param InteractorsFactory $interactorsFactory
     * @param AdapterFactory $adapterFactory
     * @param CartrigeRepository $cartrigeRepository
     * @param TokenStorage|null $user
     */
    public function __construct(
        private SerializerInterface $serializer,
        private UserRepository $userRepository,
        private IntegrationsFactory $integrationsFactory,
        private InteractorsFactory $interactorsFactory,
        private AdapterFactory $adapterFactory,
        private CartrigeRepository $cartrigeRepository,
        protected ?TokenStorage $user
    )
    {
        parent::__construct($user);
    }

    /**
     * @param AddCartrige $input
     * @param CartrigeCheckInterface $checker
     * @return AddCartrigeResponseDto
     * @throws InvalidAuthData|NotFoundCartrigeById|CartrigeIsExistException|BaseCoreException|AttemptToUpdateAnotherAccount
     */
    public function addCartrige(AddCartrige $input, CartrigeCheckInterface $checker): AddCartrigeResponseDto
    {
        $cartrige = $checker->getCartrige($input->getId());
        $externalInfo = $this->getCartrigeAccountInfo(
            $this->integrationsFactory->create($input->getIntegrationType(), $input->getAuthData()),
            $input->getIntegrationType());
        $checker->check($externalInfo, $input);

        $users = $this->makeCartrigeOwners($input->getSharingUser());
        $cartrige = $this->fillingCartrige($input->getIntegrationType(), $input->getAuthData(),
            $users, $externalInfo, $cartrige);
        $this->cartrigeRepository->save($cartrige, true);

        return $this->makeAddCartrigeResponseDto($cartrige);
    }

    /**
     * @param CartrigeList $input
     * @return ResponseCartrigeList
     */
    public function cartrigeList(CartrigeList $input): ResponseCartrigeList
    {
        $cartridges = [];
        /** @var Cartrige $cartrige */
        foreach ($this->getCurrentUser()->getCartriges() as $cartrige) {
            if (!(!empty($input->getIntegrationTypes() &&
                in_array($cartrige->getIntegrationType(), $input->getIntegrationTypes())))) {
                continue;
            }

            if (!(!empty($input->getOwnerIds() &&
                in_array($cartrige->getOwner()->getId(), $input->getOwnerIds())))) {
                continue;
            }
            $cartridges[] = $this->makeAddCartrigeResponseDto($cartrige);
        }
        //TODO сортировка

        return (new ResponseCartrigeList())
            ->setCount(count($cartridges))
            ->setItems(array_slice($cartridges, $input->getOffset(), $input->getLimit()));
    }

    /**
     * Получаем информацию об заряженном аккаунте через интеграцию из внешней системы
     * @param Api $api
     * @param string $integration_type
     * @return Author
     * @throws BaseCoreException
     */
    private function getCartrigeAccountInfo(Api $api, string $integration_type): Author
    {
        //TODO похоронить вызов adapterFactory внутри interactorsFactory
        /** @var AbstractUserinfoCollector $userInfoCollector */
        $userInfoCollector = $this->interactorsFactory->create($api, 'UserinfoCollector',
            $integration_type, null,
            $this->adapterFactory->create('UserinfoCollector', $integration_type, null));
        return $userInfoCollector->getInfo();
    }


    /**
     * @param $sharing_users_ids
     * @return array
     */
    private function makeCartrigeOwners($sharing_users_ids): array
    {
        $users = $this->userRepository->findBy(['id' => $sharing_users_ids]);
        $users[] = $this->user->getToken()->getUser();
        return $users;
    }

    /**
     * @param string $integrationType
     * @param array $authData
     * @param array $owners
     * @param Author $externalInfo
     * @param Cartrige $cartrige
     * @return Cartrige
     */
    private function fillingCartrige(string $integrationType, array $authData, array $owners, Author $externalInfo, Cartrige $cartrige): Cartrige
    {
        return $cartrige
            ->setTsCreated(new DateTime())
            ->setTsUpdated(new DateTime())//TODO
            ->addUsers($owners)
            ->setIntegrationType($integrationType)
            ->setAuthData($authData)
            ->setOwner($this->getCurrentUser())
            ->setInfo(json_decode(json_encode($externalInfo), true))
            ->setExternalId($externalInfo->getExternalId())
            ->setName($externalInfo->getFullName());
    }

    /**
     * @param Cartrige $cartrige
     * @return AddCartrigeResponseDto
     */
    private function makeAddCartrigeResponseDto(Cartrige $cartrige): AddCartrigeResponseDto
    {
        $operators = [];
        foreach ($cartrige->getUsers() as $user) {
            $operators[] = $this->makeShortUserInfoByResponse($user);
        }

        return (new AddCartrigeResponseDto())
            ->setId($cartrige->getId())
            ->setExternalId($cartrige->getExternalId())
            ->setName($cartrige->getName())
            ->setIntegrationType($cartrige->getIntegrationType())
            ->setOperators($operators);
    }

    /**
     * TODO копипаст из UserService
     * @param User $user
     * @return ShortUserInfo
     */
    private function makeShortUserInfoByResponse(User $user): ShortUserInfo
    {
        return (new ShortUserInfo())
            ->setId($user->getId())
            ->setUsername($user->getName());
    }
}