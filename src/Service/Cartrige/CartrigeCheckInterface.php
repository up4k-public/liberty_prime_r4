<?php


namespace App\Service\Cartrige;


use App\Core\Author;
use App\Dto\Api\User\Request\AddCartrige;
use App\Entity\Cartrige;
use App\Exceptions\User\AttemptToUpdateAnotherAccount;
use App\Exceptions\User\CartrigeIsExistException;
use App\Exceptions\User\NotFoundCartrigeById;

/**
 * Interface CartrigeCheckInterface
 * @package App\Service\Cartrige
 */
interface CartrigeCheckInterface
{
    /**
     * Проверки. Для нового, что не добавляли ранее. Для обновления, что обнвляется тот же ак, что и был до этого, что существует
     * @param Author $externalInfo
     * @param AddCartrige $input
     * @throws CartrigeIsExistException|NotFoundCartrigeById|AttemptToUpdateAnotherAccount
     * @return bool
     */
    public function check(Author $externalInfo, AddCartrige $input):bool;

    /**
     * Получение пустого объекта для нового, и заполненного для обновления
     * @param int|null $id
     * @return Cartrige
     */
    public function getCartrige(?int $id = null):Cartrige;
}