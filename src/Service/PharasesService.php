<?php


namespace App\Service;

use App\Core\Author;
use App\Core\ResponseMessage;
use App\Helpers\ProbabilityHelper;
use App\Settings\Settings;

/**
 * Class PharasesService
 * Преобразовывает текст ответа из заготовки в готовый к публикации текст:имя автора, спряжение, учёт пола и тд
 * @package App\Service
 */
class PharasesService
{

    const REGEX_INTERLOCUTOR_SEX_M = "~@[^<>]+?\)~";
    const REGEX_INTERLOCUTOR_SEX_F = "~\([^<>]+?@~";
    const REGEX_SELF_SEX_M = "~@@[^<>]+?\)~";
    const REGEX_SELF_SEX_F = "~\([^<>]+?@@~";

    const AUTHOR_NOMINATIVE = '{author}';
    const AUTHOR_GENITIVE = '{author:R}';
    const AUTHOR_DATIVE = '{author:D}';
    const AUTHOR_ACCUSATIVE = '{author:V}';
    const AUTHOR_ABLATIVE = '{author:T}';
    const AUTHOR_PREPOSITIONAL = '{author:P}';
    const AUTHOR_VOCATIVE = '{author:Z}';

    const COMPLEX_IS_EXPIRE = 'complex:is_expire';

    private Settings $settings;
    private Author $self_avatar;

    /**
     * @param Settings $settings
     * @return PharasesService
     */
    public function setSettings(Settings $settings): PharasesService
    {
        $this->settings = $settings;
        return $this;
    }

    /**
     * @param Author $self_avatar
     * @return PharasesService
     */
    public function setSelfAvatar(Author $self_avatar): PharasesService
    {
        $this->self_avatar = $self_avatar;
        return $this;
    }


    /**
     * @param ResponseMessage $message
     * @return ResponseMessage
     */
    public function convertSyntax(ResponseMessage $message): ResponseMessage
    {
        $message = $this->convertSex($message);
        $message = $this->convertName($message);
        $message = $this->convertNameWithCases($message);
        return $message;
    }

    /**
     * @param ResponseMessage $message
     * @return ResponseMessage
     */
    private function convertNameWithCases(ResponseMessage $message):ResponseMessage
    {
        /** @var Author $author */
        $author = $message->getInterlocutor();
        $message->setText(str_replace(self::AUTHOR_GENITIVE, $author->getFirstNameCases()?->getGenitive(), $message->getText()));
        $message->setText(str_replace(self::AUTHOR_DATIVE, $author->getFirstNameCases()?->getDative(), $message->getText()));
        $message->setText(str_replace(self::AUTHOR_ACCUSATIVE, $author->getFirstNameCases()?->getAccusative(), $message->getText()));
        $message->setText(str_replace(self::AUTHOR_ABLATIVE, $author->getFirstNameCases()?->getAblative(), $message->getText()));
        $message->setText(str_replace(self::AUTHOR_PREPOSITIONAL, $author->getFirstNameCases()?->getPrepositional(), $message->getText()));
        $message->setText(str_replace(self::AUTHOR_VOCATIVE, $author->getFirstNameCases()?->getVocative(), $message->getText()));
        return $message;
    }

    private function convertSex(ResponseMessage $message)
    {
        /** @var Author $author */
        $author = $message->getInterlocutor();

        if ($this->self_avatar->isMan()) {
            $message->setText(preg_replace(self::REGEX_SELF_SEX_M, '', $message->getText()));
        } else {
            $message->setText(preg_replace(self::REGEX_SELF_SEX_F, '', $message->getText()));
        }

        if ($author->isMan()) {
            $message->setText(preg_replace(self::REGEX_INTERLOCUTOR_SEX_M, '', $message->getText()));
        } else {
            $message->setText(preg_replace(self::REGEX_INTERLOCUTOR_SEX_F, '', $message->getText()));
        }

        $message->setText(str_replace(")", '', $message->getText()));
        $message->setText(str_replace("(", '', $message->getText()));
        return $message;
    }

    /**
     * @param ResponseMessage $message
     * @return ResponseMessage
     */
    private function convertName(ResponseMessage $message):ResponseMessage
    {
        /** @var Author $author */
        $author = $message->getInterlocutor();
        $text = $message->getText();
        $name = $author->getFirstName();

        if ($this->settings->getAppealSettings()->isAlwaysUseNickname() && !empty($author->getInternalNickName())) {
            $name = $author->getInternalNickName();
        }

        if (ProbabilityHelper::roll($this->settings->getAppealSettings()->getProbabilityUseAlterName())) {
            $name = $author->getRandomNameFromAlterList();
        }

        $text = str_replace(self::AUTHOR_NOMINATIVE, $name, $text);
        return $message->setText($text);
    }

}