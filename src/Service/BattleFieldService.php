<?php


namespace App\Service;

use App\Core\Author;
use App\Core\BattleField;

/**
 * Class BattleFieldService
 * @package App\Service
 */
class BattleFieldService
{
    /**
     * @param Author $profile
     * @param BattleField $battlefield
     * @return bool
     */
   private function checkIamAdmin(Author $profile, BattleField $battlefield):bool
   {
      if (in_array($profile->getExternalId(), $battlefield->getAdmins())) {
          return true;
      }
      return false;
   }

    /**
     * @param Author $profile
     * @param BattleField $battlefield
     * @return BattleField
     */
   public function setIamAdmin(Author $profile, BattleField $battlefield):BattleField
   {
       if ($this->checkIamAdmin($profile, $battlefield)) {
           $battlefield->setIAmAdmin(true);
       }
       return $battlefield;
   }

    /**
     * Метод, рассчитывающий уровнь лояльности администарции к наличию бота.
     * Связанно с историей, если банили других ботов или нас ранее, то понижаем.
     * Если мы админы или админы в нашей базе УПЧК или запускали нашего бота, то выше
     * @param BattleField $battlefield
     * @return BattleField
     */
   public function calculateLoyaltyLevel(BattleField $battlefield):BattleField
   {
       //todo реализовать
       return $battlefield;
   }
}