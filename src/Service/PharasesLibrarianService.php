<?php

namespace App\Service;

use App\Dto\Api\Phrase\Phrase as PhraseDto;
use App\Dto\Api\Phrase\Request\GetOne as PhraseGetOneDto;
use App\Dto\Api\Phrase\Request\GetPhraseHistory;
use App\Dto\Api\Phrase\Request\GetPhraseList;
use App\Dto\Api\Phrase\Response\PhraseList;
use App\Entity\HistoryPhrase;
use App\Entity\Phrase;
use App\Exceptions\Phrase\ImpossibleRevertPhaseAtHistory;
use App\Exceptions\Phrase\NotHaveDiffForUpdate;
use App\Exceptions\Phrase\PhraseHistoryNotFoundException;
use App\Exceptions\Phrase\PhraseIsExistException;
use App\Exceptions\Phrase\PhraseNotFoundException;
use App\Repository\HistoryPhraseRepository;
use App\Repository\PhraseRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use JMS\Serializer\SerializerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

/**
 * Class PharasesLibrarianService
 * @package App\Service
 */
class PharasesLibrarianService extends ApiService
{
    /**
     * PharasesLibrarianService constructor.
     * @param EntityManagerInterface $entityManager
     * @param SerializerInterface $serializer
     * @param PhraseRepository $phraseRepository
     * @param HistoryPhraseRepository $historyPhraseRepository
     * @param TokenStorage|null $user
     */
    public function __construct(
        private EntityManagerInterface $entityManager,
        private SerializerInterface $serializer,
        private PhraseRepository $phraseRepository,
        private HistoryPhraseRepository $historyPhraseRepository,
        protected ?TokenStorage $user
    )
    {
        parent::__construct($user);
    }

    /**
     * @param GetPhraseList $input
     * @return PhraseList
     */
    public function getList(GetPhraseList $input): PhraseList
    {
        return (new PhraseList())
            ->setItems($this->phraseRepository->getListWithFilters($input))
            ->setCount($this->phraseRepository->getCountWithFilter($input));
    }

    /**
     * @param PhraseGetOneDto $input
     * @return PhraseDto
     * @throws PhraseNotFoundException
     */
    public function getOne(PhraseGetOneDto $input): PhraseDto
    {
        $phrase = $this->phraseRepository->getById($input->getId());
        return $phrase->getPhraseDtoWithHistory($input->getLimit(), $input->getOffset());
    }

    /**
     * @param PhraseDto $input
     * @return PhraseDto
     * @throws PhraseIsExistException
     * @throws NotHaveDiffForUpdate
     */
    public function addNew(PhraseDto $input): PhraseDto
    {
        $phrase = $this->phraseRepository->getByText($input->getText());
        if (!empty($phrase)) {
            throw new PhraseIsExistException(PhraseIsExistException::MESSAGE . ' Phrase Id: ' . $phrase->getId());
        }
        $phrase = (new Phrase())->setPhraseDto($input)
            ->setTsCreated(new DateTime()) //todo в фоне
            ->setUpdatedBy($this->user->getToken()->getUser()->getUserIdentifier());
        $this->entityManager->persist($phrase);
        $this->entityManager->flush();
        $this->saveHistory(null, $phrase);
        return $phrase->getPhraseDto();
    }

    /**
     * @param PhraseDto $input
     * @return PhraseDto
     * @throws PhraseNotFoundException
     * @throws NotHaveDiffForUpdate
     */
    public function update(PhraseDto $input): PhraseDto
    {
        $phrase = $this->phraseRepository->getById($input->getId());
        $before = $phrase->getPhraseDto();
        $phrase->setPhraseDto($input)
            ->setUpdatedBy($this->user->getToken()->getUser()->getUserIdentifier());
        $this->saveHistory($before, $phrase);
        $this->entityManager->flush();
        return $phrase->getPhraseDto();
    }

    /**
     * @param GetPhraseHistory $input
     * @return PhraseDto
     * @throws PhraseNotFoundException
     * @throws PhraseHistoryNotFoundException|ImpossibleRevertPhaseAtHistory|NotHaveDiffForUpdate
     */
    public function revertAtHistory(GetPhraseHistory $input): PhraseDto
    {
        $history = $this->historyPhraseRepository->getById($input->getId());
        if (!empty($history->getBefore())) {
            $before = $this->serializer->deserialize(json_encode($history->getBefore()), PhraseDto::class, 'json');
            return $this->update($before);
        }
        throw new ImpossibleRevertPhaseAtHistory();
    }

    /**
     * @param PhraseDto|null $before
     * @param Phrase $phrase
     * @return HistoryPhrase
     * @throws NotHaveDiffForUpdate
     */
    private function saveHistory(?PhraseDto $before, Phrase $phrase): HistoryPhrase
    {
        $history = (new HistoryPhrase())
            ->setBefore($before)
            ->setAfter($phrase->getPhraseDto())
            ->setPhrase($phrase)
            ->setTsCreated(new DateTime()) //todo в фоне
            ->setUpdatedBy($this->user->getToken()->getUser()->getUserIdentifier());
        $history->checkHaveDiff();

        $this->entityManager->persist($history);
        $this->entityManager->flush();
        return $history;
    }
}