<?php


namespace App\Service;

use App\Solvers\Solution;

/**
 * Логика обработки массива "решений" сгенерированного разными модулями
 * Class SolutionService
 * @package App\Service
 */
class SolutionService
{
    /**
     * Выбираем один ответ с самым большим QualityOfChoice
     * Рандомизируем выбор если у нескольких ответов одинаковое качество
     * @param array $solutions
     * @return Solution|null
     */
    public function getTheBestOneByQualityOfChoice(array $solutions): ?Solution
    {
        $potential_result = $this->getResponseByQualityOfChoice($solutions,
            $this->getMaxQualityOfChoice($solutions));

        $search_key = rand(0, count($potential_result) - 1);

        /** @var Solution $solution */
        $solution = $potential_result[$search_key];
        return $solution;
    }

    /**
     * Получить максимальное значение QualityOfChoice в массиве всех доступхы ответов от всех модулей
     * @param array $solutions
     * @return int
     */
    private function getMaxQualityOfChoice(array $solutions): int
    {
        $max = 0;
        /** @var Solution $solution */
        foreach ($solutions as $key => $solution) {
            if ($solution->getEstimations()->getQualityOfChoice() > $max) {
                $max = $solution->getEstimations()->getQualityOfChoice();
            }
        }
        return $max;
    }

    /**
     * Получить все объекты ответа с указанным качеством(QualityOfChoice) из массива всех ответов от всех модулей
     * @param array $solutions
     * @param int $value
     * @return array
     */
    private function getResponseByQualityOfChoice(array $solutions, int $value): array
    {
        $result = [];
        /** @var Solution $solution */
        foreach ($solutions as $solution) {
            if ($solution->getEstimations()->getQualityOfChoice() == $value) {
                $result[] = $solution;
            }
        }
        return $result;
    }
}