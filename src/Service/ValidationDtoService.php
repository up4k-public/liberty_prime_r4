<?php


namespace App\Service;

use App\Dto\Api\ApiDto;
use App\Dto\Api\ValidationFieldsError;
use App\Exceptions\ApiValidationFieldsException;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class ValidationDtoService
 * @package App\Service
 */
class ValidationDtoService
{
    /**
     * ValidationDtoService constructor.
     * @param ValidatorInterface $validator
     */
    public function __construct(
        private ValidatorInterface $validator,
    )
    {
    }

    /**
     * @param $model
     * @return bool
     * @throws ApiValidationFieldsException
     */
    public function validate($model): bool
    {
        $errors = $this->recursiveValidateDto($model);

        if (!empty($errors)) {
            throw (new ApiValidationFieldsException())
                ->setFields($errors);
        }
        return true;
    }


    /**
     * @param $errors
     * @param string $field_name
     * @return array
     */
    private function customConstraintViolationListToArray($errors, $field_name = ''): array
    {
        $errors_dto = [];
        foreach ($errors as $error) {
            if ($error instanceof ConstraintViolation) {
                $error_dto = (new ValidationFieldsError())
                    ->setMessage($error->getMessage());
                if (!empty($field_name)) {
                    $error_dto->setProperty($field_name . '.' . $error->getPropertyPath());
                } else {
                    $error_dto->setProperty($error->getPropertyPath());
                }
                $errors_dto[] = $error_dto;
            }
        }
        return $errors_dto;
    }

    /**
     * У меня такое чувство, что тут я изобрёл велосипед.
     * Почему оно из коробки не работате рекурсивно и массивами объектов? TODO проверить
     * @param $model
     * @param string $parent_field_name
     * @return array
     */
    private function recursiveValidateDto($model, $parent_field_name = ''): array
    {
        $errors = $this->customConstraintViolationListToArray($this->validator->validate($model), $parent_field_name);

        if ($model instanceof ApiDto && is_array(get_object_vars($model))) {
            $vars = get_object_vars($model);
            foreach ($vars as $key => $var) {
                if ($var instanceof ApiDto) {
                    $errors = array_merge($errors, $this->recursiveValidateDto($var, $parent_field_name . $key));
                }
                if (is_array($var)) {
                    foreach ($var as $var_item) {
                        if ($var_item instanceof ApiDto) {
                            $errors = array_merge($errors, $this->recursiveValidateDto($var, $parent_field_name . $key));
                        }
                    }
                }
            }
        }
        return $errors;
    }

}