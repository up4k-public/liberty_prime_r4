<?php


namespace App\Service;

use App\Dto\Api\User\Response\GenerateInvite as GenerateInviteDtoResponse;
use App\Dto\Api\User\Response\RegisterNew as RegisterNewDtoResponse;
use App\Dto\Api\BaseList;
use App\Dto\Api\Register\RegisterNewUser;
use App\Dto\Api\User\Request\InviteList;
use App\Dto\Api\User\Response\InviteList as ResponseInviteList;
use App\Dto\Api\User\Request\UserList;
use App\Dto\Api\User\Response\UserList as ResponseUserList;
use App\Dto\Api\User\Response\Patron;
use App\Dto\Api\User\Response\ShortUserInfo;
use App\Entity\Invite;
use App\Entity\User;
use App\Dto\Api\User\Response\User as UserDto;
use App\Dto\Api\User\Response\Invite as InviteDto;
use App\Exceptions\User\InvalidInviteException;
use App\Exceptions\User\InviteHasBeenUsedException;
use App\Exceptions\User\UserAlreadyExistException;
use App\Repository\InviteRepository;
use App\Repository\UserRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Uid\Factory\UuidFactory;

/**
 * Class UserService
 * @package App\Service
 */
class UserService extends ApiService
{
    /**
     * UserService constructor.
     * @param UserPasswordHasherInterface $passwordHasher
     * @param UserRepository $userRepository
     * @param InviteRepository $inviteRepository
     * @param UuidFactory $uuidFactory
     * @param EntityManagerInterface $entityManager
     * @param TokenStorage|null $user
     */
    public function __construct(
        private UserPasswordHasherInterface $passwordHasher,
        private UserRepository $userRepository,
        private InviteRepository $inviteRepository,
        private UuidFactory $uuidFactory,
        private EntityManagerInterface $entityManager,
        protected ?TokenStorage $user
    )
    {
        parent::__construct($user);
    }


    /**
     * @param RegisterNewUser $input
     * @return RegisterNewDtoResponse
     * @throws InvalidInviteException
     * @throws InviteHasBeenUsedException
     * @throws UserAlreadyExistException
     */
    public function registerNew(RegisterNewUser $input): RegisterNewDtoResponse
    {
        $invite = $this->getInviteAndCheck($input->getInvite());
        $this->checkUserIsExist($input);

        $user = $this->createUser($input, $invite);
        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return $this->makeResponseRegisterNewUser($user, $invite);
    }

    /**
     * @param InviteList $input
     * @return ResponseInviteList
     */
    public function getInviteList(InviteList $input): ResponseInviteList
    {
        $items = [];
        /** @var Invite $invite */
        foreach ($this->getCurrentUser()->getInvites() as $invite) {
            $items[] = $this->custInviteEntityToInviteApiDto($invite);
        }
        $items = array_slice($items, $input->getOffset(), $input->getLimit());
        return (new ResponseInviteList())
            ->setItems($items)
            ->setCount(count($this->getCurrentUser()->getInvites()));
    }

    /**
     * @param UserList $input
     * @return ResponseUserList
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function list(UserList $input): ResponseUserList
    {
        $users = $this->userRepository->getList($input->getLimit(), $input->getOffset(),
            $input->getSortField(), $input->getSortBy());
        $items = [];

        /** @var User $user */
        foreach ($users as $user) {
            $items[] = $this->custUserEntityToUserApiDto($user);
        }

        return (new ResponseUserList())
            ->setItems($items)
            ->setCount($this->userRepository->getTotalCount());
    }

    /**
     * @return GenerateInviteDtoResponse
     */
    public function generateInvite(): GenerateInviteDtoResponse
    {
        $invite = (new Invite())
            ->setTsCreated(new DateTime())
            ->setOwner($this->getCurrentUser())
            ->setKey($this->uuidFactory->create());
        $this->entityManager->persist($invite);
        $this->entityManager->flush();
        return (new GenerateInviteDtoResponse())->setInvite($invite->getKey());
    }



    /**
     * @param RegisterNewUser $input
     * @param Invite $invite
     * @return User
     */
    private function createUser(RegisterNewUser $input, Invite $invite): User
    {
        $user = new User();
        $user->setPassword($this->passwordHasher->hashPassword(
            $user,
            $input->getPassword()
        ))
            ->setTsCreated(new DateTime())
            ->setTsUpdated(new DateTime())
            ->setOwnerInvite($invite)
            ->setName($input->getUsername());
        return $user;
    }

    /**
     * @param User $user
     * @param Invite $invite
     * @return RegisterNewDtoResponse
     */
    private function makeResponseRegisterNewUser(User $user, Invite $invite): RegisterNewDtoResponse
    {
        return ($this->makeShortUserInfoByResponse($user))
            ->mothToRegisterNew()->setPatronName($invite->getOwner()->getName());
    }

    /**
     * @param User $user
     * @return ShortUserInfo
     */
    private function makeShortUserInfoByResponse(User $user): ShortUserInfo
    {
        return (new ShortUserInfo())
            ->setId($user->getId())
            ->setUsername($user->getName());
    }

    /**
     * @param RegisterNewUser $input
     * @return bool
     * @throws UserAlreadyExistException
     */
    private function checkUserIsExist(RegisterNewUser $input): bool
    {
        $user = $this->userRepository->getByName($input->getUsername());
        if (!empty($user)) {
            throw new UserAlreadyExistException();
        }
        return false;
    }

    /**
     * @param string $invite
     * @return Invite
     * @throws InvalidInviteException
     * @throws InviteHasBeenUsedException
     */
    private function getInviteAndCheck(string $invite): Invite
    {
        $invite = $this->inviteRepository->findOneBy(['key' => $invite]);
        if (empty($invite)) {
            throw new InvalidInviteException();
        }
        if (!empty($invite->getRegisteredBy())) {
            throw new InviteHasBeenUsedException();
        }
        return $invite;
    }

    /**
     * @param User $user
     * @return UserDto
     */
    private function custUserEntityToUserApiDto(User $user): UserDto
    {
        return (new UserDto())
            ->setId($user->getId())
            ->setName($user->getName())
            ->setTsCreated($user->getTsCreated()->format('Y-m-d H:i:s'))
            ->setPatron((new Patron())
                ->setName($user?->getOwnerInvite()?->getOwner()->getName())
                ->setId($user?->getOwnerInvite()?->getOwner()->getId()))
            ->setIsBanned($user->isIsBanned());
    }

    /**
     * @param Invite $invite
     * @return InviteDto
     */
    private function custInviteEntityToInviteApiDto(Invite $invite): InviteDto
    {
        $result = (new InviteDto())
            ->setKey($invite->getKey())
            ->setTsCreated($invite->getTsCreated()->format('Y-m-d H:i:s'));

        if (!empty($invite?->getRegisteredBy())) {
            $result
                ->setProtege((new Patron())
                    ->setName($invite?->getRegisteredBy()?->getName())
                    ->setId($invite?->getRegisteredBy()?->getId()))
                ->setIsUse(true);
        } else {
            $result
                ->setProtege(null)
                ->setIsUse(false);
        }
        return $result;

    }
}