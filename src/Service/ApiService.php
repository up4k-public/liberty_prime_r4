<?php


namespace App\Service;

use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

/**
 * Class ApiService
 * @package App\Service
 */
class ApiService
{
    /**
     * ApiService constructor.
     * @param TokenStorage|null $user
     */
    public function __construct(
        protected ?TokenStorage $user
    )
    {
    }

    /**
     * @return User
     */
    public function getCurrentUser(): User
    {
        return $this->user->getToken()->getUser();
    }
}