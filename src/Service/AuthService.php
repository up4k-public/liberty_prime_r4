<?php


namespace App\Service;

use App\Dto\Api\BaseResponseFormat;
use Exception;
use JMS\Serializer\SerializerInterface;

/**
 * Сервис для обслуживания проксирующей логики из AuthController'a
 * AuthController потребен для смены формата(обёртывание {success:true, data:[] и единый формат ошибки} ответа на единый для системы в целом, и адекватного отображения в swagger apiDoc
 * в запросах на получение и работы с JWT token, refresh_token и logout. Ибо свагер не кушает "jwt-маршруты" из коробки.
 * Да, смена формата(обёртывание) нарушает спецификацию Auth2.0, но кто соблюдает все 76 листов рекомендаций в этом талмуде?
 *
 * TODO убрать, когда apiDoc свагера перестанет использоваться, как эрзац фронт рядом пользователей
 * Class AuthService
 * @package App\Service
 */
class AuthService
{

    /**
     * AuthService constructor.
     * @param SerializerInterface $serializer
     */
    public function __construct(
        private SerializerInterface $serializer)
    {
    }

    /**
     * @param $url
     * @param $is_post
     * @param $data
     * @param $headers
     * @param null $class
     * @return bool|string
     * @throws Exception
     */
    public function redirect($url, $is_post, $data, $headers, $class = null)
    {
        $result_headers = [];
        foreach ($headers as $key => $header) {
            $result_headers[] = $key . ': ' . implode(',', $header);
        }

        $curl = curl_init();

        $options = [
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_HTTPHEADER => $result_headers,
        ];
        if ($is_post) {
            $options[CURLOPT_CUSTOMREQUEST] = "POST";
        } else {
            $options[CURLOPT_CUSTOMREQUEST] = "GET";
        }

        curl_setopt_array($curl, $options);


        $response = curl_exec($curl);

        $curl_info = curl_getinfo($curl);
        if ($curl_info['http_code'] != 200) {
            curl_close($curl);
            /** @var  BaseResponseFormat $error */
            $error = $this->serializer->deserialize($response, BaseResponseFormat::class, 'json');
            throw new Exception($error->getError()?->getMessage(), $error->getError()?->getCode());
        }

        curl_close($curl);


        if (empty($class)) {
            return $response;
        }

        return $this->serializer->deserialize($response, $class, 'json');
    }
}