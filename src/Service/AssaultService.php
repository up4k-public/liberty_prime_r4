<?php


namespace App\Service;

use App\Dto\Api\Assault\Request\AssaultList;
use App\Dto\Api\Assault\Response\AssaultList as AssaultListResponse;
use App\Dto\Api\Assault\Request\CreateAssault;
use App\Dto\Api\Assault\Request\GetOneAssault;
use App\Dto\Api\Assault\Request\UpdateAssault;
use App\Dto\Api\User\Response\ShortCartrigeInfo;
use App\Entity\Assault;
use App\Entity\Cartrige;
use App\Exceptions\Assault\AssaultNotFound;
use App\Exceptions\User\AccessDeniedToCartrige;
use App\Exceptions\User\NotFoundCartrigeById;
use App\Factories\UnitsSettingsFactory;
use App\Dto\Api\Assault\Response\Assault as AssaultDto;
use App\Repository\AssaultRepository;
use App\Repository\CartrigeRepository;
use App\Settings\Settings;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use JMS\Serializer\SerializerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

/**
 * Class AssaultService
 * @package App\Service
 */
class AssaultService extends ApiService
{
    /**
     * AssaultService constructor.
     * @param SerializerInterface $serializer
     * @param CartrigeRepository $cartrigeRepository
     * @param AssaultRepository $assaultRepository
     * @param EntityManagerInterface $entityManager
     * @param TokenStorage|null $user
     */
    public function __construct(
        private SerializerInterface $serializer,
        private CartrigeRepository $cartrigeRepository,
        private AssaultRepository $assaultRepository,
        private EntityManagerInterface $entityManager,
        protected ?TokenStorage $user
    )
    {
        parent::__construct($user);
    }

    /**
     * @param CreateAssault $input
     * @return AssaultDto
     * @throws AccessDeniedToCartrige|NotFoundCartrigeById
     */
    public function createNew(CreateAssault $input): AssaultDto
    {
        $this->checkAccessToCartrige($input->getCartrigeId());
        $assault = $this->makeAssaultByInputDto($input);
        $this->assaultRepository->save($assault, true);
        return $this->makeAssaultResponseDto($assault);
    }

    /**
     * @param AssaultList $input
     * @return AssaultListResponse
     * @throws NoResultException|NonUniqueResultException
     */
    public function list(AssaultList $input): AssaultListResponse
    {
        $assaults = $this->assaultRepository->getByStatusAndCartridges(
            $this->getCurrentUser()->getCartriges(),
            $input->getStatuses(),
            $input->getSortField(),
            $input->getSortBy(),
            $input->getLimit(),
            $input->getOffset());

        $items = [];
        foreach ($assaults as $assault) {
            $items[] = $this->makeAssaultResponseDto($assault);
        }

        return (new AssaultListResponse())
            ->setItems($items)
            ->setCount($this->assaultRepository->getCountByStatusAndCartridges($this->getCurrentUser()->getCartriges(), $input->getStatuses()));
    }

    /**
     * @param UpdateAssault $input
     * @return AssaultDto
     * @throws NotFoundCartrigeById
     * @throws AssaultNotFound
     */
    public function update(UpdateAssault $input): AssaultDto
    {
        $assault = $this->assaultRepository->getById($input->getId());
        $assault->setSettings($input->getSettings())
            ->setStatus(Assault::STATUS_NEW)
            ->setCartrige($this->cartrigeRepository->getById($input->getCartrigeId()));
        $this->assaultRepository->save($assault, true);
        return $this->makeAssaultResponseDto($assault);
    }

    /**
     * @param GetOneAssault $input
     * @return AssaultDto
     * @throws AssaultNotFound
     */
    public function restart(GetOneAssault $input): AssaultDto
    {
        $assault = $this->assaultRepository->getById($input->getId());
        $assault->setStatus(Assault::STATUS_NEW);
        $this->assaultRepository->save($assault, true);
        return $this->makeAssaultResponseDto($assault);
    }

    /**
     * @param GetOneAssault $input
     * @return AssaultDto
     * @throws AssaultNotFound
     */
    public function stop(GetOneAssault $input): AssaultDto
    {
        $assault = $this->assaultRepository->getById($input->getId());
        $assault->setStatus(Assault::STATUS_CANCELLED);
        $this->assaultRepository->save($assault, true);
        return $this->makeAssaultResponseDto($assault);
    }

    /**
     * @param Assault $assault
     * @return Settings
     */
    public function getSettings(Assault $assault): Settings
    {
        /** @var Settings $settings */
        $settings = $this->serializer->deserialize(json_encode($assault->getSettings()), Settings::class, 'json');
        $settings->setModuleSettings($this->custModuleSettingsToObject($settings->getModuleSettings()));
        return $settings;
    }

    /**
     * Костыльно-велосипедно работаю с сериалайзером
     * Всё за ради того, чтобы без правки dto и донастройки сериалайзера, при новых модулях всё подхватывать
     * TODO покурить доку сериалайзера и понять, как это делать нативно
     * @param array $modules_settings
     * @return array
     */
    private function custModuleSettingsToObject(array $modules_settings): array
    {
        foreach ($modules_settings as $key => &$module_settings) {
            $module_settings = $this->serializer->deserialize(json_encode($module_settings), UnitsSettingsFactory::NAMESPACE . ucfirst($key), 'json');
        }
        unset($module_settings);
        return $modules_settings;
    }

    /**
     * @param int $cartrige_id
     * @return bool
     * @throws AccessDeniedToCartrige
     */
    private function checkAccessToCartrige(int $cartrige_id): bool
    {
        $cartrige_ids = [];
        /** @var Cartrige $cartrige */
        foreach ($this->getCurrentUser()->getCartriges() as $cartrige) {
            $cartrige_ids[] = $cartrige->getId();
        }
        if (!in_array($cartrige_id, $cartrige_ids)) {
            throw new AccessDeniedToCartrige();
        }
        return true;
    }

    /**
     * @param CreateAssault $input
     * @return Assault
     * @throws NotFoundCartrigeById
     */
    private function makeAssaultByInputDto(CreateAssault $input): Assault
    {
        return (new Assault())
            ->setSettings($input->getSettings())
            ->setTargetId($input->getTargetId())
            ->setCreatedBy($this->getCurrentUser())
            ->setUpdatedBy($this->getCurrentUser())
            ->setGlobalType($input->getIntegrationType())
            ->setLocalType($input->getLocalType())
            ->setStatus(Assault::STATUS_NEW)
            ->setTsCreated(new DateTime())->setTsUpdated(new DateTime()) //todo
            ->setCartrige($this->cartrigeRepository->getById($input->getCartrigeId()));
    }

    /**
     * @param Assault $assault
     * @return AssaultDto
     */
    private function makeAssaultResponseDto(Assault $assault): AssaultDto
    {
        return (new AssaultDto())
            ->setId($assault->getId())
            ->setStatus($assault->getStatus())
            ->setSettings($this->serializer->deserialize(json_encode($assault->getSettings()), Settings::class, 'json'))
            ->setCartrige((new ShortCartrigeInfo())
                ->setId($assault->getCartrige()->getId())
                ->setName($assault->getCartrige()->getName())
                ->setExternalId($assault->getCartrige()->getExternalId())
                ->setInfo($assault->getCartrige()->getInfo($this->serializer)))
            ->setTargetId($assault->getTargetId())
            ->setIntegrationType($assault->getGlobalType())
            ->setLocalType($assault->getLocalType());
    }


    /**
     * @param Assault $assault
     * @return bool
     */
    public function checkAssaultInProcess(Assault $assault): bool
    {
        $this->entityManager->refresh($assault);
        if ($assault->getStatus() != Assault::STATUS_IN_PROCESS) {
            return false;
        }
        return true;
    }
}