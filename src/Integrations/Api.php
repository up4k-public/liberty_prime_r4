<?php


namespace App\Integrations;

use App\Exceptions\Integration\InvalidAuthData;
use App\Service\ValidationDtoService;
use JMS\Serializer\SerializerInterface;

/**
 * Class Api
 * @package App\Integration
 */
abstract class Api
{
    /**
     * VkApi constructor.
     * @param SerializerInterface $serializer
     * @param ValidationDtoService $validationDtoService
     */
    public function __construct(
        protected SerializerInterface $serializer,
        protected ValidationDtoService $validationDtoService
    )
    {
    }

    /**
     * @param array|null $data
     * @throws InvalidAuthData
     * @return bool
     */
    abstract function setAuthorization(?array $data): bool;

    /**
     * @return string
     */
    abstract function getType(): string;
}