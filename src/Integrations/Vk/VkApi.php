<?php


namespace App\Integrations\Vk;

use App\Exceptions\ApiValidationFieldsException;
use App\Exceptions\Integration\InvalidAuthData;
use App\Integrations\Api;
use App\Integrations\Vk\dto\AuthData;
use App\Integrations\Vk\dto\Response\Base;
use App\Integrations\Vk\dto\Response\Messages\GetHistory\MessagesGetHistoryBase;
use App\Integrations\Vk\dto\Response\Messages\GetHistory\MessagesGetHistoryResponse;
use App\Integrations\Vk\dto\User\UserGetInfoBase;
use Exception;

/**
 * Class VkApi
 * @package App\Integration\Vk
 */
class VkApi extends Api
{
    const BASE_API_URL = 'https://api.vk.com/method/';
    const CHAT_ID_SHIFT = 2000000000;
    private ?string $api_version = '5.131';
    private ?string $token = null;

    /**
     * @param array|null $data
     * @return bool
     * @throws InvalidAuthData
     */
    function setAuthorization(?array $data): bool
    {
        try {
            /** @var AuthData $authData */
            $authData = $this->serializer->deserialize(json_encode($data), AuthData::class, 'json');
            $this->validationDtoService->validate($authData);

        } catch (Exception) {
              throw new InvalidAuthData();
        }
        $this->setToken($authData->getToken());
        return true;
    }

    /**
     * @return string
     */
    function getType(): string
    {
        return Handbook::TYPE;
    }

    /**
     * @return string|null
     */
    public function getToken(): ?string
    {
        return $this->token;
    }

    /**
     * @param string|null $token
     * @return VkApi
     */
    public function setToken(?string $token): VkApi
    {
        $this->token = $token;
        return $this;
    }

    /**
     * @param string $login
     * @param string $password
     * @param $app_id
     * @return string
     */
    public function refreshToken(string $login, string $password, $app_id): string
    {
        return $this->token;
    }

    /**
     * @param string $methodName
     * @param array|null $params
     * @return string
     * @throws Exception
     */
    public function sent(string $methodName, ?array $params = null): string
    {
        $url = self::BASE_API_URL . $methodName;
        $params['v'] = $this->api_version;
        $params['access_token'] = $this->token;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type: multipart/form-data',
            'charset=utf-8',]);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_TIMEOUT, 300);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        $res = curl_exec($ch);
        //var_dump($params);
        //var_dump($res);
        //die;
        $curl_info = curl_getinfo($ch);
        if ($curl_info['http_code'] != 200) {
            curl_close($ch);
            throw new Exception('Vk Api http status code ' . $curl_info['http_code']);
        }

        /** @var Base $result */
        $result = $this->serializer->deserialize($res, Base::class, 'json');
        if (!empty($result) && !empty($result?->getError())) {
            throw new Exception('Vk Api error code: ' . $result->getError()->getErrorCode() . ' Msg:' . $result->getError()->getErrorMsg());
        }
        curl_close($ch);
        return $res;
    }

    /**
     * @param array|null $ids
     * @return array|null
     * @throws Exception
     */
    public function usersGet(?array $ids = null): ?array
    {
        $methodMethodName = 'users.get';

        $params = [
            'fields' => implode(',', Handbook::USER_GET_ADD_FIELDS)
        ];
        if (!empty($ids)) {
            $params['user_ids'] = implode(',', $ids);
        }

        $data = $this->sent($methodMethodName, $params);

        /** @var UserGetInfoBase $result */
        $result = $this->serializer->deserialize($data, UserGetInfoBase::class, 'json');
        return $result->getResponse();
    }

    public function messagesAddChatUser(int $chat_id, int $user_id)
    {
        $methodMethodName = 'messages.addChatUser';
        return $this->sent($methodMethodName,
            [
                'chat_id' => $chat_id,
                'user_id' => $user_id,
                'visible_messages_count' => 200
            ]);
    }

    public function messagesRemoveChatUser(int $chat_id, int $user_id)
    {
        $methodMethodName = 'messages.removeChatUser';
        return $this->sent($methodMethodName,
            [
                'chat_id' => $chat_id,
                'user_id' => $user_id,
            ]);
    }


    public function messagesGetConversations(int $limit = 1, int $offset = 0)
    {
        $methodMethodName = 'messages.getConversations';
        return $this->sent($methodMethodName,
            [
                'offset' => $offset,
                'count' => $limit,
            ]);
    }

    public function messagesGetConversationsById(int $id)
    {
        $methodMethodName = 'messages.getConversations';
        return $this->sent($methodMethodName,
            [
                'peer_ids' => $id,
            ]);
    }

    /**
     * @param int $peer_id
     * @param string|null $start_message_id
     * @return MessagesGetHistoryResponse
     * @throws Exception
     */
    public function messagesGetHistory(int $peer_id, ?string $start_message_id = null): MessagesGetHistoryResponse
    {
        $methodMethodName = 'messages.getHistory';

        $params = [
            'peer_id' => $peer_id,
            'extended' => 1,
            'fields' => implode(',', Handbook::USER_GET_ADD_FIELDS)];

        if (!empty($start_message_id)) {
            //тому что для vk есть разница не передать start_message_id или перелат start_message_id=null
            $params['start_message_id'] = $start_message_id;
        }


        $data = $this->sent($methodMethodName, $params);
        /** @var MessagesGetHistoryBase $result */
        $result = $this->serializer->deserialize($data, MessagesGetHistoryBase::class, 'json');
        //die;
        return $result->getResponse();
    }

    public function messagesSent(int $peer_id, ?string $message, ?array $forward_messages)
    {
        $methodMethodName = 'messages.send';
        $params = [
            'peer_id' => $peer_id,
            'message' => $message,
            'random_id' => rand(0, 255),
            'forward_messages' => implode(',', $forward_messages)];
        $data = $this->sent($methodMethodName, $params);
        return ['response' => 1];
    }

    /**
     * @param array|null $message_ids
     * @param string $peer_id
     * @return int[]
     * @throws Exception
     */
    public function markAsRead(?array $message_ids, string $peer_id)
    {
        $methodMethodName = 'messages.markAsRead';
        $params = [
            //'message_ids' => $message_ids, Устарело в прошлой версии API
            'peer_id' => $peer_id
        ];
        $data = $this->sent($methodMethodName, $params);
        return ['response' => 1];
    }

    public function setActivity(string $peer_id, string $type = 'typing')
    {
        $methodMethodName = 'messages.setActivity';
        $params = [
            'type' => $type,
            'peer_id' => $peer_id
        ];
        $data = $this->sent($methodMethodName, $params);
        return ['response' => 1];
    }

}