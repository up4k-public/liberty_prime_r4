<?php


namespace App\Integrations\Vk\dto\Response\Messages;

use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Type;

/**
 * Class ChatSettings
 * @package App\Integrations\Vk\dto\Response\Messages
 */
class ChatSettings
{
    #[Serializer\Type("string")]
    private ?string $title;

    #[Serializer\Type("integer")]
    private ?int $members_count;

    #[Serializer\Type("integer")]
    private ?int $friends_count;

    #[Serializer\Type("integer")]
    private ?int $owner_id;

    #[Serializer\Type("string")]
    private ?string $state;

    #[Serializer\Type("array")]
    private ?array $photo;

    #[Serializer\Type("array")]
    private ?array $admin_ids = [];

    #[Serializer\Type("array")]
    private ?array $active_ids;

    #[Serializer\Type("bool")]
    private ?bool $is_group_channel;

    #[Serializer\Type("array")]
    private ?array $acl;

    #[Serializer\Type("bool")]
    private ?bool $is_disappearing;

    #[Serializer\Type("bool")]
    private ?bool $is_service;

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string|null $title
     * @return ChatSettings
     */
    public function setTitle(?string $title): ChatSettings
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getMembersCount(): ?int
    {
        return $this->members_count;
    }

    /**
     * @param int|null $members_count
     * @return ChatSettings
     */
    public function setMembersCount(?int $members_count): ChatSettings
    {
        $this->members_count = $members_count;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getFriendsCount(): ?int
    {
        return $this->friends_count;
    }

    /**
     * @param int|null $friends_count
     * @return ChatSettings
     */
    public function setFriendsCount(?int $friends_count): ChatSettings
    {
        $this->friends_count = $friends_count;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getOwnerId(): ?int
    {
        return $this->owner_id;
    }

    /**
     * @param int|null $owner_id
     * @return ChatSettings
     */
    public function setOwnerId(?int $owner_id): ChatSettings
    {
        $this->owner_id = $owner_id;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getState(): ?string
    {
        return $this->state;
    }

    /**
     * @param string|null $state
     * @return ChatSettings
     */
    public function setState(?string $state): ChatSettings
    {
        $this->state = $state;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getPhoto(): ?array
    {
        return $this->photo;
    }

    /**
     * @param array|null $photo
     * @return ChatSettings
     */
    public function setPhoto(?array $photo): ChatSettings
    {
        $this->photo = $photo;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getAdminIds(): ?array
    {
        return $this->admin_ids;
    }

    /**
     * @param array|null $admin_ids
     * @return ChatSettings
     */
    public function setAdminIds(?array $admin_ids): ChatSettings
    {
        $this->admin_ids = $admin_ids;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getActiveIds(): ?array
    {
        return $this->active_ids;
    }

    /**
     * @param array|null $active_ids
     * @return ChatSettings
     */
    public function setActiveIds(?array $active_ids): ChatSettings
    {
        $this->active_ids = $active_ids;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getIsGroupChannel(): ?bool
    {
        return $this->is_group_channel;
    }

    /**
     * @param bool|null $is_group_channel
     * @return ChatSettings
     */
    public function setIsGroupChannel(?bool $is_group_channel): ChatSettings
    {
        $this->is_group_channel = $is_group_channel;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getAcl(): ?array
    {
        return $this->acl;
    }

    /**
     * @param array|null $acl
     * @return ChatSettings
     */
    public function setAcl(?array $acl): ChatSettings
    {
        $this->acl = $acl;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getIsDisappearing(): ?bool
    {
        return $this->is_disappearing;
    }

    /**
     * @param bool|null $is_disappearing
     * @return ChatSettings
     */
    public function setIsDisappearing(?bool $is_disappearing): ChatSettings
    {
        $this->is_disappearing = $is_disappearing;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getIsService(): ?bool
    {
        return $this->is_service;
    }

    /**
     * @param bool|null $is_service
     * @return ChatSettings
     */
    public function setIsService(?bool $is_service): ChatSettings
    {
        $this->is_service = $is_service;
        return $this;
    }
}