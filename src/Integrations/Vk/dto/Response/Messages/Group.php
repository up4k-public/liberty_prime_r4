<?php


namespace App\Integrations\Vk\dto\Response\Messages;

use App\Integrations\Vk\dto\Response\Geo;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Type;

/**
 * Class Group
 * @package App\Integrations\Vk\dto\Response\Messages
 */
class Group
{
    #[Serializer\Type("integer")]
    public ?int $id;

    #[Serializer\Type("App\Integrations\Vk\dto\Response\Geo")]
    public ?Geo $country = null;

    #[Serializer\Type("string")]
    public ?string $name;

    #[Serializer\Type("string")]
    public ?string $screen_name;

    #[Serializer\Type("boolean")]
    public ?bool $is_closed;

    #[Serializer\Type("string")]
    public ?string $type;

    #[Serializer\Type("boolean")]
    public ?bool $is_admin;

    #[Serializer\Type("boolean")]
    public ?bool $is_member;

    #[Serializer\Type("boolean")]
    public ?bool $is_advertiser;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return Group
     */
    public function setId(?int $id): Group
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return Geo|null
     */
    public function getCountry(): ?Geo
    {
        return $this->country;
    }

    /**
     * @param Geo|null $country
     * @return Group
     */
    public function setCountry(?Geo $country): Group
    {
        $this->country = $country;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     * @return Group
     */
    public function setName(?string $name): Group
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getScreenName(): ?string
    {
        return $this->screen_name;
    }

    /**
     * @param string|null $screen_name
     * @return Group
     */
    public function setScreenName(?string $screen_name): Group
    {
        $this->screen_name = $screen_name;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getIsClosed(): ?bool
    {
        return $this->is_closed;
    }

    /**
     * @param bool|null $is_closed
     * @return Group
     */
    public function setIsClosed(?bool $is_closed): Group
    {
        $this->is_closed = $is_closed;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string|null $type
     * @return Group
     */
    public function setType(?string $type): Group
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getIsAdmin(): ?bool
    {
        return $this->is_admin;
    }

    /**
     * @param bool|null $is_admin
     * @return Group
     */
    public function setIsAdmin(?bool $is_admin): Group
    {
        $this->is_admin = $is_admin;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getIsMember(): ?bool
    {
        return $this->is_member;
    }

    /**
     * @param bool|null $is_member
     * @return Group
     */
    public function setIsMember(?bool $is_member): Group
    {
        $this->is_member = $is_member;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getIsAdvertiser(): ?bool
    {
        return $this->is_advertiser;
    }

    /**
     * @param bool|null $is_advertiser
     * @return Group
     */
    public function setIsAdvertiser(?bool $is_advertiser): Group
    {
        $this->is_advertiser = $is_advertiser;
        return $this;
    }
}