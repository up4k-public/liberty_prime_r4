<?php


namespace App\Integrations\Vk\dto\Response\Messages;

use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Type;

/**
 * Class Action
 * @package App\Integrations\Vk\dto\Response\Messages
 */
class Action
{
    const TYPES = [
        'chat_photo_update',
        'chat_title_update',
        'chat_kick_user',
        'chat_invite_user',
        'chat_unpin_message',
        'chat_pin_message'
    ];

    #[Serializer\Type("string")]
    private ?string $type = null;

    #[Serializer\Type("string")]
    private ?string $text = null;

    #[Serializer\Type("string")]
    private ?string $member_id = null;

    /**
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string|null $type
     * @return Action
     */
    public function setType(?string $type): Action
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getText(): ?string
    {
        return $this->text;
    }

    /**
     * @param string|null $text
     * @return Action
     */
    public function setText(?string $text): Action
    {
        $this->text = $text;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getMemberId(): ?string
    {
        return $this->member_id;
    }

    /**
     * @param string|null $member_id
     * @return Action
     */
    public function setMemberId(?string $member_id): Action
    {
        $this->member_id = $member_id;
        return $this;
    }

}