<?php


namespace App\Integrations\Vk\dto\Response\Messages;

use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Type;

/**
 * Class Conversation
 * @package App\Integrations\Vk\dto\Response\Messages
 */
class Conversation
{
    #[Serializer\Type("array")]
    private ?array $peer;

    #[Serializer\Type("integer")]
    private ?int $last_message_id;

    #[Serializer\Type("integer")]
    private ?int $in_read;

    #[Serializer\Type("integer")]
    private ?int $out_read;

    #[Serializer\Type("array")]
    private ?array $sort_id;

    #[Serializer\Type("integer")]
    private ?int $last_conversation_message_id;

    #[Serializer\Type("integer")]
    private ?int $in_read_cmid;

    #[Serializer\Type("integer")]
    private ?int $out_read_cmid;

    #[Serializer\Type("integer")]
    private ?int $unread_count;

    #[Serializer\Type("boolean")]
    private ?bool $is_marked_unread;

    #[Serializer\Type("boolean")]
    private ?bool $important;

    #[Serializer\Type("array")]
    private ?array $push_settings;

    #[Serializer\Type("array")]
    private ?array $can_write;

    #[Serializer\Type("boolean")]
    private ?bool $can_send_money;

    #[Serializer\Type("boolean")]
    private ?bool $can_receive_money;

    #[Serializer\Type("App\Integrations\Vk\dto\Response\Messages\ChatSettings")]
    private ?ChatSettings $chat_settings;

    #[Serializer\Type("boolean")]
    private ?bool $is_new;

    /**
     * @return array|null
     */
    public function getPeer(): ?array
    {
        return $this->peer;
    }

    /**
     * @param array|null $peer
     * @return Conversation
     */
    public function setPeer(?array $peer): Conversation
    {
        $this->peer = $peer;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getLastMessageId(): ?int
    {
        return $this->last_message_id;
    }

    /**
     * @param int|null $last_message_id
     * @return Conversation
     */
    public function setLastMessageId(?int $last_message_id): Conversation
    {
        $this->last_message_id = $last_message_id;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getInRead(): ?int
    {
        return $this->in_read;
    }

    /**
     * @param int|null $in_read
     * @return Conversation
     */
    public function setInRead(?int $in_read): Conversation
    {
        $this->in_read = $in_read;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getOutRead(): ?int
    {
        return $this->out_read;
    }

    /**
     * @param int|null $out_read
     * @return Conversation
     */
    public function setOutRead(?int $out_read): Conversation
    {
        $this->out_read = $out_read;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getSortId(): ?array
    {
        return $this->sort_id;
    }

    /**
     * @param array|null $sort_id
     * @return Conversation
     */
    public function setSortId(?array $sort_id): Conversation
    {
        $this->sort_id = $sort_id;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getLastConversationMessageId(): ?int
    {
        return $this->last_conversation_message_id;
    }

    /**
     * @param int|null $last_conversation_message_id
     * @return Conversation
     */
    public function setLastConversationMessageId(?int $last_conversation_message_id): Conversation
    {
        $this->last_conversation_message_id = $last_conversation_message_id;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getInReadCmid(): ?int
    {
        return $this->in_read_cmid;
    }

    /**
     * @param int|null $in_read_cmid
     * @return Conversation
     */
    public function setInReadCmid(?int $in_read_cmid): Conversation
    {
        $this->in_read_cmid = $in_read_cmid;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getOutReadCmid(): ?int
    {
        return $this->out_read_cmid;
    }

    /**
     * @param int|null $out_read_cmid
     * @return Conversation
     */
    public function setOutReadCmid(?int $out_read_cmid): Conversation
    {
        $this->out_read_cmid = $out_read_cmid;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getUnreadCount(): ?int
    {
        return $this->unread_count;
    }

    /**
     * @param int|null $unread_count
     * @return Conversation
     */
    public function setUnreadCount(?int $unread_count): Conversation
    {
        $this->unread_count = $unread_count;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getIsMarkedUnread(): ?bool
    {
        return $this->is_marked_unread;
    }

    /**
     * @param bool|null $is_marked_unread
     * @return Conversation
     */
    public function setIsMarkedUnread(?bool $is_marked_unread): Conversation
    {
        $this->is_marked_unread = $is_marked_unread;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getImportant(): ?bool
    {
        return $this->important;
    }

    /**
     * @param bool|null $important
     * @return Conversation
     */
    public function setImportant(?bool $important): Conversation
    {
        $this->important = $important;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getPushSettings(): ?array
    {
        return $this->push_settings;
    }

    /**
     * @param array|null $push_settings
     * @return Conversation
     */
    public function setPushSettings(?array $push_settings): Conversation
    {
        $this->push_settings = $push_settings;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getCanWrite(): ?array
    {
        return $this->can_write;
    }

    /**
     * @param array|null $can_write
     * @return Conversation
     */
    public function setCanWrite(?array $can_write): Conversation
    {
        $this->can_write = $can_write;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getCanSendMoney(): ?bool
    {
        return $this->can_send_money;
    }

    /**
     * @param bool|null $can_send_money
     * @return Conversation
     */
    public function setCanSendMoney(?bool $can_send_money): Conversation
    {
        $this->can_send_money = $can_send_money;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getCanReceiveMoney(): ?bool
    {
        return $this->can_receive_money;
    }

    /**
     * @param bool|null $can_receive_money
     * @return Conversation
     */
    public function setCanReceiveMoney(?bool $can_receive_money): Conversation
    {
        $this->can_receive_money = $can_receive_money;
        return $this;
    }

    /**
     * @return ChatSettings|null
     */
    public function getChatSettings(): ?ChatSettings
    {
        return $this->chat_settings;
    }

    /**
     * @param ChatSettings|null $chat_settings
     * @return Conversation
     */
    public function setChatSettings(?ChatSettings $chat_settings): Conversation
    {
        $this->chat_settings = $chat_settings;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getIsNew(): ?bool
    {
        return $this->is_new;
    }

    /**
     * @param bool|null $is_new
     * @return Conversation
     */
    public function setIsNew(?bool $is_new): Conversation
    {
        $this->is_new = $is_new;
        return $this;
    }

}