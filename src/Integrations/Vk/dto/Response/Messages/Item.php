<?php


namespace App\Integrations\Vk\dto\Response\Messages;

use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Type;

/**
 * Class Item
 * @package App\Integrations\Vk\dto\Response\Messages
 */
class Item
{
    #[Serializer\Type("integer")]
    private ?int $date;

    #[Serializer\Type("integer")]
    private ?int $from_id;

    #[Serializer\Type("integer")]
    private ?int $id;

    #[Serializer\Type("integer")]
    private ?int $out;

    #[Serializer\Type("array")]
    private ?array $attachments;

    #[Serializer\Type("App\Integrations\Vk\dto\Response\Messages\Action")]
    private ?Action $action = null;

    #[Serializer\Type("integer")]
    private ?int $conversation_message_id;

    #[Serializer\Type("array")]
    private ?array $fwd_messages;

    #[Serializer\Type("boolean")]
    private ?bool $important;

    #[Serializer\Type("boolean")]
    private ?bool $is_hidden;

    #[Serializer\Type("integer")]
    private ?int $peer_id;

    #[Serializer\Type("integer")]
    private ?int $random_id;

    #[Serializer\Type("string")]
    private ?string $text;

    #[Serializer\Type("boolean")]
    private ?bool $is_expired = false;

    #[Serializer\Type("string")]
    private ?string $update_time = null;

    #[Serializer\Type("integer")]
    private ?int $expire_ttl = null;

    /**
     * @return int|null
     */
    public function getDate(): ?int
    {
        return $this->date;
    }

    /**
     * @return int|null
     */
    public function getFromId(): ?int
    {
        return $this->from_id;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return int|null
     */
    public function getOut(): ?int
    {
        return $this->out;
    }

    /**
     * @return array|null
     */
    public function getAttachments(): ?array
    {
        return $this->attachments;
    }

    /**
     * @return int|null
     */
    public function getConversationMessageId(): ?int
    {
        return $this->conversation_message_id;
    }

    /**
     * @return array|null
     */
    public function getFwdMessages(): ?array
    {
        return $this->fwd_messages;
    }

    /**
     * @return bool|null
     */
    public function getImportant(): ?bool
    {
        return $this->important;
    }

    /**
     * @return bool|null
     */
    public function getIsHidden(): ?bool
    {
        return $this->is_hidden;
    }

    /**
     * @return int|null
     */
    public function getPeerId(): ?int
    {
        return $this->peer_id;
    }

    /**
     * @return int|null
     */
    public function getRandomId(): ?int
    {
        return $this->random_id;
    }

    /**
     * @return string|null
     */
    public function getText(): ?string
    {
        return $this->text;
    }

    /**
     * @return Action|null
     */
    public function getAction(): ?Action
    {
        return $this->action;
    }

    /**
     * @param Action|null $action
     * @return Item
     */
    public function setAction(?Action $action): Item
    {
        $this->action = $action;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getIsExpired(): ?bool
    {
        return $this->is_expired;
    }

    /**
     * @param bool|null $is_expired
     * @return Item
     */
    public function setIsExpired(?bool $is_expired): Item
    {
        $this->is_expired = $is_expired;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUpdateTime()
    {
        return $this->update_time;
    }

    /**
     * @param mixed $update_time
     * @return Item
     */
    public function setUpdateTime($update_time)
    {
        $this->update_time = $update_time;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getExpireTtl(): ?int
    {
        return $this->expire_ttl;
    }

    /**
     * @param int|null $expire_ttl
     * @return Item
     */
    public function setExpireTtl(?int $expire_ttl): Item
    {
        $this->expire_ttl = $expire_ttl;
        return $this;
    }

}