<?php


namespace App\Integrations\Vk\dto\Response\Messages;

use App\Integrations\Vk\dto\Response\Geo;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Type;

/**
 * Class Profile
 * @package App\Integrations\Vk\dto\Response\Messages
 */
class Profile
{
    #[Serializer\Type("integer")]
    private ?int $id;

    #[Serializer\Type("string")]
    private ?string $first_name_nom;

    #[Serializer\Type("string")]
    private ?string $first_name_gen;

    #[Serializer\Type("string")]
    private ?string $first_name_dat;

    #[Serializer\Type("string")]
    private ?string $first_name_acc;

    #[Serializer\Type("string")]
    private ?string $first_name_ins;

    #[Serializer\Type("string")]
    private ?string $first_name_abl;

    #[Serializer\Type("string")]
    private ?string $nickname = null;

    #[Serializer\Type("string")]
    private ?string $bdate = null;

    #[Serializer\Type("App\Integrations\Vk\dto\Response\Geo")]
    private ?Geo $city = null;

    #[Serializer\Type("App\Integrations\Vk\dto\Response\Geo")]
    private ?Geo $country = null;

    #[Serializer\Type("integer")]
    private ?int $is_friend;

    #[Serializer\Type("integer")]
    private ?int $blacklisted;

    #[Serializer\Type("integer")]
    private ?int $sex;

    #[Serializer\Type("string")]
    private ?string $first_name;

    #[Serializer\Type("string")]
    private ?string $last_name;

    #[Serializer\Type("boolean")]
    private ?bool $can_access_closed;

    #[Serializer\Type("boolean")]
    private ?bool $is_closed;

    #[Serializer\Type("string")]
    private ?string $domain;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return Profile
     */
    public function setId(?int $id): Profile
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getFirstNameNom(): ?string
    {
        return $this->first_name_nom;
    }

    /**
     * @param string|null $first_name_nom
     * @return Profile
     */
    public function setFirstNameNom(?string $first_name_nom): Profile
    {
        $this->first_name_nom = $first_name_nom;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getFirstNameGen(): ?string
    {
        return $this->first_name_gen;
    }

    /**
     * @param string|null $first_name_gen
     * @return Profile
     */
    public function setFirstNameGen(?string $first_name_gen): Profile
    {
        $this->first_name_gen = $first_name_gen;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getFirstNameDat(): ?string
    {
        return $this->first_name_dat;
    }

    /**
     * @param string|null $first_name_dat
     * @return Profile
     */
    public function setFirstNameDat(?string $first_name_dat): Profile
    {
        $this->first_name_dat = $first_name_dat;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getFirstNameAcc(): ?string
    {
        return $this->first_name_acc;
    }

    /**
     * @param string|null $first_name_acc
     * @return Profile
     */
    public function setFirstNameAcc(?string $first_name_acc): Profile
    {
        $this->first_name_acc = $first_name_acc;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getFirstNameIns(): ?string
    {
        return $this->first_name_ins;
    }

    /**
     * @param string|null $first_name_ins
     * @return Profile
     */
    public function setFirstNameIns(?string $first_name_ins): Profile
    {
        $this->first_name_ins = $first_name_ins;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getFirstNameAbl(): ?string
    {
        return $this->first_name_abl;
    }

    /**
     * @param string|null $first_name_abl
     * @return Profile
     */
    public function setFirstNameAbl(?string $first_name_abl): Profile
    {
        $this->first_name_abl = $first_name_abl;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getNickname(): ?string
    {
        return $this->nickname;
    }

    /**
     * @param string|null $nickname
     * @return Profile
     */
    public function setNickname(?string $nickname): Profile
    {
        $this->nickname = $nickname;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getBdate(): ?string
    {
        return $this->bdate;
    }

    /**
     * @param string|null $bdate
     * @return Profile
     */
    public function setBdate(?string $bdate): Profile
    {
        $this->bdate = $bdate;
        return $this;
    }

    /**
     * @return Geo|null
     */
    public function getCity(): ?Geo
    {
        return $this->city;
    }

    /**
     * @param Geo|null $city
     * @return Profile
     */
    public function setCity(?Geo $city): Profile
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @return Geo|null
     */
    public function getCountry(): ?Geo
    {
        return $this->country;
    }

    /**
     * @param Geo|null $country
     * @return Profile
     */
    public function setCountry(?Geo $country): Profile
    {
        $this->country = $country;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getIsFriend(): ?int
    {
        return $this->is_friend;
    }

    /**
     * @param int|null $is_friend
     * @return Profile
     */
    public function setIsFriend(?int $is_friend): Profile
    {
        $this->is_friend = $is_friend;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getBlacklisted(): ?int
    {
        return $this->blacklisted;
    }

    /**
     * @param int|null $blacklisted
     * @return Profile
     */
    public function setBlacklisted(?int $blacklisted): Profile
    {
        $this->blacklisted = $blacklisted;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getSex(): ?int
    {
        return $this->sex;
    }

    /**
     * @param int|null $sex
     * @return Profile
     */
    public function setSex(?int $sex): Profile
    {
        $this->sex = $sex;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getFirstName(): ?string
    {
        return $this->first_name;
    }

    /**
     * @param string|null $first_name
     * @return Profile
     */
    public function setFirstName(?string $first_name): Profile
    {
        $this->first_name = $first_name;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLastName(): ?string
    {
        return $this->last_name;
    }

    /**
     * @param string|null $last_name
     * @return Profile
     */
    public function setLastName(?string $last_name): Profile
    {
        $this->last_name = $last_name;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getCanAccessClosed(): ?bool
    {
        return $this->can_access_closed;
    }

    /**
     * @param bool|null $can_access_closed
     * @return Profile
     */
    public function setCanAccessClosed(?bool $can_access_closed): Profile
    {
        $this->can_access_closed = $can_access_closed;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getIsClosed(): ?bool
    {
        return $this->is_closed;
    }

    /**
     * @param bool|null $is_closed
     * @return Profile
     */
    public function setIsClosed(?bool $is_closed): Profile
    {
        $this->is_closed = $is_closed;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDomain(): ?string
    {
        return $this->domain;
    }

    /**
     * @param string|null $domain
     * @return Profile
     */
    public function setDomain(?string $domain): Profile
    {
        $this->domain = $domain;
        return $this;
    }

}