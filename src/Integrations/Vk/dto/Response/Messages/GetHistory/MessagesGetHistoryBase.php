<?php


namespace App\Integrations\Vk\dto\Response\Messages\GetHistory;

use App\Integrations\Vk\dto\Response\Base;

use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Type;

/**
 * Class MessagesGetHistoryBase
 * @package App\Integrations\Vk\dto\Response
 */
class MessagesGetHistoryBase extends Base
{
    #[Serializer\Type("App\Integrations\Vk\dto\Response\Messages\GetHistory\MessagesGetHistoryResponse")]
    public ?MessagesGetHistoryResponse $response = null;

    /**
     * @return MessagesGetHistoryResponse|null
     */
    public function getResponse():?MessagesGetHistoryResponse
    {
        return $this->response;
    }

    /**
     * @param null $response
     * @return MessagesGetHistoryBase
     */
    public function setResponse($response):MessagesGetHistoryBase
    {
        $this->response = $response;
        return $this;
    }

}