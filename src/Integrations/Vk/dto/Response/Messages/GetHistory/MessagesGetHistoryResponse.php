<?php


namespace App\Integrations\Vk\dto\Response\Messages\GetHistory;

use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Type;

/**
 * Class MessagesGetHistoryResponse
 * @package App\Integrations\Vk\dto\Response
 */
class MessagesGetHistoryResponse
{
    #[Serializer\Type("integer")]
    private ?int $count = null;

    #[Serializer\Type("array<App\Integrations\Vk\dto\Response\Messages\Item>")]
    private ?array $items = null;

    #[Serializer\Type("array<App\Integrations\Vk\dto\Response\Messages\Profile>")]
    private ?array $profiles = null;

    #[Serializer\Type("array<App\Integrations\Vk\dto\Response\Messages\Group>")]
    private ?array $groups = null;

    #[Serializer\Type("array<App\Integrations\Vk\dto\Response\Messages\Conversation>")]
    private ?array $conversations = null;

    /**
     * @return int|null
     */
    public function getCount(): ?int
    {
        return $this->count;
    }

    /**
     * @param int|null $count
     * @return MessagesGetHistoryResponse
     */
    public function setCount(?int $count): MessagesGetHistoryResponse
    {
        $this->count = $count;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getItems(): ?array
    {
        return $this->items;
    }

    /**
     * @param array|null $items
     * @return MessagesGetHistoryResponse
     */
    public function setItems(?array $items): MessagesGetHistoryResponse
    {
        $this->items = $items;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getProfiles(): ?array
    {
        return $this->profiles;
    }

    /**
     * @param array|null $profiles
     * @return MessagesGetHistoryResponse
     */
    public function setProfiles(?array $profiles): MessagesGetHistoryResponse
    {
        $this->profiles = $profiles;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getConversations(): ?array
    {
        return $this->conversations;
    }

    /**
     * @param array|null $conversations
     * @return MessagesGetHistoryResponse
     */
    public function setConversations(?array $conversations): MessagesGetHistoryResponse
    {
        $this->conversations = $conversations;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getGroups(): ?array
    {
        return $this->groups;
    }

    /**
     * @param array|null $groups
     * @return MessagesGetHistoryResponse
     */
    public function setGroups(?array $groups): MessagesGetHistoryResponse
    {
        $this->groups = $groups;
        return $this;
    }

}