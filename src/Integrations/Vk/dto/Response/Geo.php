<?php


namespace App\Integrations\Vk\dto\Response;

use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Type;

/**
 * Class Geo
 * @package App\Integrations\Vk\dto\Response
 */
class Geo
{
    #[Serializer\Type("integer")]
    private ?int $id = null;

    #[Serializer\Type("string")]
    private ?string $title = null;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return Geo
     */
    public function setId(?int $id): Geo
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string|null $title
     * @return Geo
     */
    public function setTitle(?string $title): Geo
    {
        $this->title = $title;
        return $this;
    }

}