<?php


namespace App\Integrations\Vk\dto\Response;

use App\Integrations\Vk\dto\Response\Error\Error;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Type;

/**
 * Class BaseResponseFormat
 * @package App\Integrations\Vk\dto\Response
 */
class Base
{
    #[Serializer\Type("App\Integrations\Vk\dto\Response\Error\Error")]
    private ?Error $error = null;

    /**
     * @return null
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @return null
     */
    public function getError():?Error
    {
        return $this->error;
    }
}