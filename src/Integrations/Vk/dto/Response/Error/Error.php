<?php


namespace App\Integrations\Vk\dto\Response\Error;

use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Type;

/**
 * Class Error
 * @package App\Integrations\Vk\dto\Response\Error
 */
class Error
{
    #[Serializer\Type("string")]
    private ?string $error_code;

    #[Serializer\Type("string")]
    private ?string $error_msg;

    #[Serializer\Type("array")]
    private ?array $request_params;

    /**
     * @return string|null
     */
    public function getErrorCode(): ?string
    {
        return $this->error_code;
    }

    /**
     * @param string|null $error_code
     * @return Error
     */
    public function setErrorCode(?string $error_code): Error
    {
        $this->error_code = $error_code;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getErrorMsg(): ?string
    {
        return $this->error_msg;
    }

    /**
     * @param string|null $error_msg
     * @return Error
     */
    public function setErrorMsg(?string $error_msg): Error
    {
        $this->error_msg = $error_msg;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getRequestParams(): ?array
    {
        return $this->request_params;
    }

    /**
     * @param array|null $request_params
     * @return Error
     */
    public function setRequestParams(?array $request_params): Error
    {
        $this->request_params = $request_params;
        return $this;
    }

}