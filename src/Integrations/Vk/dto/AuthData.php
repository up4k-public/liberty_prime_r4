<?php


namespace App\Integrations\Vk\dto;

use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Type;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Class AuthData
 * @package App\Integrations\Vk\dto
 */
class AuthData
{
    #[Serializer\Type("string")]
    #[Assert\NotBlank]
    public string $token;

    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }

    /**
     * @param string $token
     * @return AuthData
     */
    public function setToken(string $token): AuthData
    {
        $this->token = $token;
        return $this;
    }
}