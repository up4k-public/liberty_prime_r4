<?php


namespace App\Integrations\Vk\dto\User;


use App\Integrations\Vk\dto\Response\Base;

use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Type;

/**
 * Class UserGetInfoBase
 * @package App\Integrations\Vk\dto\User
 */
class UserGetInfoBase extends Base
{
    #[Serializer\Type("array<App\Integrations\Vk\dto\Response\Messages\Profile>")]
    public ?array $response = null;

    /**
     * @return array|null
     */
    public function getResponse(): ?array
    {
        return $this->response;
    }

    /**
     * @param array|null $response
     * @return UserGetInfoBase
     */
    public function setResponse(?array $response): UserGetInfoBase
    {
        $this->response = $response;
        return $this;
    }

}