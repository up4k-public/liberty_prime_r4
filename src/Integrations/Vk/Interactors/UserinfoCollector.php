<?php


namespace App\Integrations\Vk\Interactors;

use App\Core\Author;
use App\Core\Interactors\AbstractUserinfoCollector;
use App\Integrations\Vk\Adapters\AdapterUserinfoCollector;
use App\Integrations\Vk\VkApi;
use Exception;

/**
 * Class UserinfoCollector
 * @package App\Integrations\Vk\Interactors
 */
class UserinfoCollector extends AbstractUserinfoCollector
{
    public function getApi():VkApi
    {
        return parent::getApi();
    }

    public function getAdapter(): AdapterUserinfoCollector
    {
        return parent::getAdapter();
    }

    /**
     * @param string|null $user_id
     * @return Author
     * @throws Exception
     */
    public function getInfo(?string $user_id = null): Author
    {
        return $this->getAdapter()->convert(
            $this->getApi()->usersGet()
        );
    }
}