<?php


namespace App\Integrations\Vk\Interactors\Chat;


use App\Core\Interactors\AbstractMessageRecipient;
use App\Integrations\Vk\VkApi;
use Exception;

/**
 * Class MessageRecipient
 * @package App\Integration\Vk\Interactors\Chat
 */
class MessageRecipient extends AbstractMessageRecipient
{
    /**
     * @return array
     * @throws Exception
     */
    public function getMessage(): array
    {
        $data = $this->getApi()->messagesGetHistory(VkApi::CHAT_ID_SHIFT + $this->getTargetId());
        $message = $this->getAdapter()->convert($data->getItems());
        $message = $this->getAdapter()->addAuthorInfo($message, $data);
        return $message;
    }

}