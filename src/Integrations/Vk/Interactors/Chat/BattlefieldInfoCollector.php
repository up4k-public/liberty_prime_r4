<?php


namespace App\Integrations\Vk\Interactors\Chat;

use App\Core\BattleField;
use App\Core\Interactors\AbstractBattlefieldInfoCollector;
use App\Integrations\Vk\Adapters\Chat\AdapterBattlefieldInfoCollector;
use App\Integrations\Vk\VkApi;
use Exception;

/**
 * Class AbstractBattlefieldInfoCollector
 * @package App\Integrations\Vk\Interactors\Chat
 */
class BattlefieldInfoCollector extends AbstractBattlefieldInfoCollector
{
    public function getApi(): VkApi
    {
        return parent::getApi();
    }

    public function getAdapter(): ?AdapterBattlefieldInfoCollector
    {
        return parent::getAdapter();
    }

    /**
     * @param string $target_id
     * @return BattleField
     * @throws Exception
     */
    public function getInfo(string $target_id): BattleField
    {
        $vk_info = $this->getApi()->messagesGetHistory(VkApi::CHAT_ID_SHIFT + (int)$target_id);
        $battle_field = $this->getAdapter()->convert($vk_info);
        $battle_field->setIntegrationType($this->getApi()->getType());
        return $battle_field;
    }

}