<?php


namespace App\Integrations\Vk\Interactors\Chat;


use App\Core\Author;
use App\Core\Interactors\AbstractUserinfoCollector;
use App\Integrations\Vk\Interactors\UserinfoCollector as SelfUserInfoCollector;
use Exception;

/**
 * Class UserinfoCollector
 * @package App\Integrations\Vk\Interactors\Chat
 */
class UserinfoCollector extends SelfUserInfoCollector
{
    /**
     * @param string|null $user_id
     * @return Author
     * @throws Exception
     */
    public function getInfo(?string $user_id = null): Author
    {
        return parent::getInfo($user_id);
    }

}