<?php


namespace App\Integrations\Vk\Interactors\Chat;


use App\Core\Interactors\AbstractReadMarker;
use App\Integrations\Vk\VkApi;
use Exception;

/**
 * Class ReadMarker
 * @package App\Integrations\Vk\Interactors\Chat
 */
class ReadMarker extends AbstractReadMarker
{
    public function getApi(): VkApi
    {
        return parent::getApi();
    }

    /**
     * @param array|null $ids
     * @return bool
     * @throws Exception
     */
    function markAsRead(?array $ids = []): bool
    {
        $this->getApi()->markAsRead($ids, VkApi::CHAT_ID_SHIFT + (int)$this->getTargetId());
        return true;
    }

}