<?php


namespace App\Integrations\Vk\Interactors\Chat;


use App\Core\Interactors\AbstractMessageSender;
use App\Core\ResponseMessage;
use App\Integrations\Vk\Adapters\Chat\AdapterMessageSender;
use App\Integrations\Vk\VkApi;
use App\Repository\LogRepository;

/**
 * Class MessageSender
 * @package App\Integration\Vk\Interactors\Chat
 */
class MessageSender extends AbstractMessageSender
{
    private AdapterMessageSender $messageAdapter;
    private VkApi $api;

    /**
     * MessageRecipient constructor.
     * @param LogRepository $eventLogger
     * @param VkApi $api
     * @param AdapterMessageSender $messageAdapter
     */
    public function __construct(
        LogRepository $eventLogger,
        VkApi $api,
        AdapterMessageSender $messageAdapter)
    {
        parent::__construct($eventLogger,$api);
        $this->api = $api;
        $this->messageAdapter = $messageAdapter;
    }

    public function postMessage(ResponseMessage $message): ?int
    {
        $this->api->messagesSent(VkApi::CHAT_ID_SHIFT + $this->getTargetId(), $message->getText(), $message->getForwardMessageExternalIds());
        return null;
    }

}