<?php


namespace App\Integrations\Vk\Interactors\Chat;


use App\Core\Interactors\AbstractTypesetter;
use App\Integrations\Vk\Handbook;
use App\Integrations\Vk\VkApi;

/**
 * Class Typesetter
 * @package App\Integrations\Vk\Interactors\Chat
 */
class Typesetter extends AbstractTypesetter
{
    public function getApi(): VkApi
    {
        return parent::getApi();
    }

    /**
     * VkDoc: После успешного выполнения возвращает 1.
     * Текст «N набирает сообщение...» отображается в течение 5-10 секунд после вызова метода, либо до момента отправки сообщения.
     * @param int $duration
     * @return bool
     */
    public function setActivity(int $duration = 0): bool
    {
        $remains = $duration;
        //Если нам надо, что «USERNAME набирает сообщение...» горело долго, то дёрнуть VkApi придётся несколько раз
        do {
            $this->getApi()->setActivity(VkApi::CHAT_ID_SHIFT + (int)$this->getTargetId());
            if ($remains < Handbook::LAG_TYPESEETER) {
                sleep($remains);
            } else {
                sleep(Handbook::LAG_TYPESEETER);
            }
            $remains += ~Handbook::LAG_TYPESEETER; //~порязрядная операция инверсии
        } while ($remains >= 0);
        return true;
    }

}