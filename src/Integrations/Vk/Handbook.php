<?php


namespace App\Integrations\Vk;

/**
 * Class Handbook
 * @package App\Integration\Vk
 */
class Handbook
{
    const TYPE = 'Vk';
    const LAG_TYPESEETER = 5;


    const USER_GET_ADD_FIELDS = [
        'sex',
        'city',
        'maiden_name',
        'nickname',
        'domain',
        'bdate',
        'blacklisted',
        'country',
        'is_friend',
        'first_name_nom',
        'first_name_gen',
        'first_name_dat',
        'first_name_acc',
        'first_name_ins',
        'first_name_abl',
    ];

    const BOT_NAME = 'bot';
}