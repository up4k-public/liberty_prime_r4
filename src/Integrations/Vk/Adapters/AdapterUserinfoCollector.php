<?php


namespace App\Integrations\Vk\Adapters;


use App\Core\Author;
use App\Core\BaseAdapter;
use App\Core\NameCases;
use App\Exceptions\Integration\InvalidDateFormat;
use App\Integrations\Vk\dto\Response\Messages\Profile;
use App\Integrations\Vk\Handbook;
use DateTime;
use Exception;

/**
 * Class AdapterUserinfoCollector
 * @package App\Integrations\Vk\Adapters
 */
class AdapterUserinfoCollector extends BaseAdapter
{
    /**
     * @param $data
     * @return Author
     * @throws InvalidDateFormat
     */
    function convert($data): Author
    {
        /** @var Profile $vk_profile */
        $vk_profile = $data[0];
        return (self::makeAuthorByProfile($vk_profile))->setIsMy(true);
    }

    /**
     * Иногда вк апи отдаёт нам профиль пользователя не тольпо по user.get,но и в других методах,
     * чтобы получать актуальную инфу по юзеру без доп. запроса и не ломать базовую структуру Интерракторов
     * и не плодить копипасту в статик метод
     * TODO придумать как убрать этот костыль
     * @param Profile $profile
     * @return Author
     * @throws InvalidDateFormat
     */
    public static function makeAuthorByProfile(Profile $profile): Author
    {
        $all_names = [
            $profile->getFirstName(),
            $profile->getLastName(),
            $profile->getNickname(),
            str_replace('_', ' ', ucfirst($profile->getDomain())),
            $profile->getFirstName() . ' ' . $profile->getLastName(),
            //todo псевдонимы из базы для этого имени
            //todo прозвища из базы для этого аккаунта
        ];

        return (new Author())
            ->setCountry($profile->getCountry()?->getTitle())
            ->setCity($profile->getCity()?->getTitle())
            ->setBdate(self::formatBDate($profile->getBdate()))
            ->setFirstName($profile->getFirstName())
            ->setLastName($profile->getLastName())
            ->setDomain($profile->getDomain())
            ->setExternalId($profile->getId())
            ->setInternalId(null) //todo айди в нашей базе
            ->setEnvType(Handbook::TYPE)
            ->setInterests([
                //todo интересы из профиля
                //todo список групп подписки
            ])
            ->setSex(self::calculateSex($profile->getSex()))
            ->setAllNames($all_names)
            ->setFirstNameCases((new NameCases())
                ->setNominativ($profile->getFirstNameNom())
                ->setGenitive($profile->getFirstNameGen())
                ->setDative($profile->getFirstNameDat())
                ->setAccusative($profile->getFirstNameAcc())
                ->setAblative($profile->getFirstNameAbl())
                ->setPrepositional($profile->getFirstNameIns())
                ->setVocative($profile->getFirstNameNom()) //todo хелпер для вычисления звательного падежа Миш, Олесь, Насть, Гриш
            );
    }

    /**
     * Мужик ли аккаунт
     * @param int|null $sex
     * @return bool|null
     */
    private static function calculateSex(?int $sex): ?bool
    {
        if ($sex === 2) {
            return Author::SEX_MAN;
        }
        if ($sex === 1) {
            return Author::SEX_WOMEN;
        }
        return null;
    }


    /**
     * @param string|null $bdate
     * @return string|null
     * @throws InvalidDateFormat
     */
    private static function formatBDate(?string $bdate): ?string
    {
        if (empty($bdate)) {
            return null;
        }
        try {
            return (new DateTime($bdate))->format('Y-m-d');
        } catch (Exception) {
           throw new InvalidDateFormat(InvalidDateFormat::MESSAGE . ' ' . $bdate);
        }

    }
}