<?php


namespace App\Integrations\Vk\Adapters\Chat;

use App\Integrations\Vk\Adapters\AdapterUserinfoCollector as SelfAdapterUserinfoCollector;

/**
 * Class AdapterUserinfoCollector
 * @package App\Integrations\Vk\Adapters
 */
class AdapterUserinfoCollector extends SelfAdapterUserinfoCollector
{

}