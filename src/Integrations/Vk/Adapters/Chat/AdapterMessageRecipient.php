<?php


namespace App\Integrations\Vk\Adapters\Chat;

use App\Core\Author;
use App\Core\BaseAdapter;
use App\Core\DialogAction\BaseDialogAction;
use App\Core\DialogAction\ChatInviteUser;
use App\Core\DialogAction\ChatKikUser;
use App\Core\DialogAction\ChatLeaveUser;
use App\Core\DialogAction\ChatPhotoUpdate;
use App\Core\DialogAction\ChatPinMessage;
use App\Core\DialogAction\ChatReturnUser;
use App\Core\DialogAction\ChatTitleUpdate;
use App\Core\DialogAction\ChatUnpinMessage;
use App\Core\Message;
use App\Core\NameCases;
use App\Integrations\Vk\dto\Response\Messages\Action;
use App\Integrations\Vk\dto\Response\Messages\GetHistory\MessagesGetHistoryResponse;
use App\Integrations\Vk\dto\Response\Messages\Group;
use App\Integrations\Vk\dto\Response\Messages\Item;
use App\Integrations\Vk\dto\Response\Messages\Profile;
use App\Integrations\Vk\Handbook;
use App\Service\PharasesService;

/**
 * Class AdapterMessageRecipient
 * @package App\Integration\Vk\Adapters
 */
class AdapterMessageRecipient extends BaseAdapter
{
    /**
     * @param array|null $data
     * @return array
     */
    public function convert($data): array
    {
        $result = [];
        if (empty($data)) {
            return $result;
        }

        /** @var Item $message */
        foreach ($data as $message) {
            $new_msg = (new Message())
                ->setAuthorId($message->getFromId())
                ->setDatetime($message->getDate()) //todo преобразовавать в единый формат времени
                ->setExternalId($message->getId())
                ->setInternalId(null)
                ->setText($message->getText())
                ->setTargetInternalId(null)
                ->setEnvType(Handbook::TYPE)
                ->setActions([$this->makeAction($message)])
                ->setAdditionalInfo([
                    $message->getConversationMessageId(),
                    $message->getFwdMessages(),
                    $message->getRandomId()
                ])
                ->setAttachments($message->getAttachments());

            if ($message->getIsExpired() || !empty($message->getExpireTtl())) {
                $new_msg->addText(PharasesService::COMPLEX_IS_EXPIRE)
                    ->setCanForward(false);
            }

            //если сервисное сообщение, смена названия-авы чата, добавление-удаление пользователя
            if (!empty($message->getAction()?->getType())) {
                $new_msg->setIsService(true)
                    ->setCanForward(false)
                    ->setServiceInfo([
                        'type' => $message->getAction()->getType(),
                        'member_id' => $message->getAction()->getMemberId()
                    ]);
                if (!empty($message->getAction()->getText())) {
                    $new_msg->setText($new_msg->getText() . $message->getAction()->getText());
                }
            }

            $result[] = $new_msg;

        }
        return $result;
    }

    /**
     * Обогощаем сообщение информацией об авторе
     * @param array|null $messages
     * @param MessagesGetHistoryResponse $data
     * @param string
     * @return array
     */
    public function addAuthorInfo(?array $messages, MessagesGetHistoryResponse $data): array
    {
        /** @var Message $message */
        foreach ($messages as $message) {
            if ($message->getAuthorId() > 0) {
                /** @var Profile $profile */
                foreach ($data->getprofiles() as $profile) {
                    if ($message->getAuthorId() == $profile->getId()) {
                        $author = $this->makeAuthorByProfile($profile);

                        $message->setAuthor($author);
                        break;
                    }
                }

            } else {
                /** @var Group $profile */
                foreach ($data->getGroups() as $profile) {
                    if ($message->getAuthorId() * (-1) == $profile->getId()) {
                        $message->setAuthor($this->makeAuthorByGroup($profile));
                        break;
                    }

                }
            }

            if (is_array($message->getActions())) {
                /** @var BaseDialogAction $action */
                foreach ($message->getActions() as $action) {
                    if (!empty($action) && ($action instanceof ChatInviteUser)) {
                        foreach ($data->getprofiles() as $profile) {
                            if ($action->getProcessingObjectId() == $profile->getId()) {
                                $author = $this->makeAuthorByProfile($profile);
                                $message->getActions()[0]->setProcessingUser($author);
                                break;
                            }
                        }
                    }
                }
            }

        }
        return $messages;
    }

    /**
     * @param Profile $profile
     * @return Author
     */
    private function makeAuthorByProfile(Profile $profile): Author
    {
        //Стащим у соседа, чтобы не копипастить
        return AdapterUserinfoCollector::makeAuthorByProfile($profile);
    }

    /**
     * Если автор сообщения бот-группа
     * @param Group $profile
     * @return Author
     */
    private function makeAuthorByGroup(Group $profile): Author
    {
        return (new Author())
            ->setExternalId($profile->getId())
            ->setFirstName($profile->getName())
            ->setIsBot(true)
            ->setAllNames([
                $profile->getName(),
                $profile->getScreenName()
            ])
            ->setCountry($profile->getCountry()?->getTitle());
    }

    /**
     * TODO зарефакторит вместе со всем адаптером вк в лб
     * @param Item|null $message
     * @return BaseDialogAction|null
     */
    private function makeAction(?Item $message): ?BaseDialogAction
    {
        if (empty($message?->getAction()?->getType())) {
            return null;
        }

        try {
           //chat_create
            $result = match ($message->getAction()->getType()) {
                'chat_kick_user' => (new ChatKikUser())
                    ->setProcessingObjectId($message->getAction()->getMemberId()),
                'chat_invite_user_by_link'=> (new ChatInviteUser())
                    ->setProcessingObjectId($message->getAction()->getMemberId()),
                'chat_invite_user' => (new ChatInviteUser())
                    ->setProcessingObjectId($message->getAction()->getMemberId()),
                'chat_photo_update' => (new ChatPhotoUpdate())
                    ->setProcessingObjectId($message->getAction()->getMemberId()),
                'chat_title_update' => (new ChatTitleUpdate())
                    ->setText($message->getAction()->getText())
                    ->setProcessingObjectId($message->getAction()->getMemberId()),
                'chat_pin_message' => (new ChatPinMessage())
                    ->setText($message->getAction()->getText())
                    ->setProcessingObjectId($message->getAction()->getMemberId()),
                'chat_unpin_message' => (new ChatUnpinMessage())
                    ->setProcessingObjectId($message->getAction()->getMemberId()),
            };
        } catch (\Throwable $exception) {
            var_dump($message->getAction()->getType() . ' error' );
            return null;
        }

        if (($result instanceof ChatInviteUser) && $result->getProcessingObjectId() == $message->getFromId()) {
              $result = (new ChatReturnUser())
                  ->setProcessingObjectId($message->getAction()->getMemberId());
        }
        if (($result instanceof ChatKikUser) && $result->getProcessingObjectId() == $message->getFromId()) {
            $result = (new ChatLeaveUser())
                ->setProcessingObjectId($message->getAction()->getMemberId());
        }

        return $result;
    }

}