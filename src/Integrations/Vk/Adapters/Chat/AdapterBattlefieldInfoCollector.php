<?php


namespace App\Integrations\Vk\Adapters\Chat;

use App\Core\BaseAdapter;
use App\Core\BattleField;
use App\Integrations\Vk\dto\Response\Messages\Conversation;
use App\Integrations\Vk\dto\Response\Messages\GetHistory\MessagesGetHistoryResponse;
use Exception;

/**
 * Class AdapterBattlefieldInfoCollector
 * @package App\Integrations\Vk\Adapters\Chat
 */
class AdapterBattlefieldInfoCollector extends BaseAdapter
{
    /**
     * @param $data
     * @return BattleField
     * @throws Exception
     */
    function convert($data): BattleField
    {
        if (!($data instanceof MessagesGetHistoryResponse)) {
            throw new Exception('Invalid format by ' . self::class);
        }
        /** @var  Conversation $chat_info */
        $chat_info = $data->getConversations()[0];
        return (new BattleField())
            ->setName($chat_info->getChatSettings()->getTitle())
            ->setAdmins($chat_info->getChatSettings()->getAdminIds());
    }

}