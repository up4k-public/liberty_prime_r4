<?php


namespace App\Estimations;

use Exception;

/**
 * Class Estimations
 * Метрики фразы глобальные, метрики фразы локально для конкретного диалога
 * @package App\Estimations
 */
class Estimations
{
    const MIN_QUALITY_OF_CHOICE = 0;
    const MAX_QUALITY_OF_CHOICE = 100;

    private int $quality_of_choice;
    private array $phrase_metrics = [];

    /**
     * @return int
     */
    public function getQualityOfChoice(): int
    {
        return $this->quality_of_choice;
    }

    /**
     * @param int $quality_of_choice
     * @return Estimations
     * @throws Exception
     */
    public function setQualityOfChoice(int $quality_of_choice): Estimations
    {
        if ($quality_of_choice < self::MIN_QUALITY_OF_CHOICE || $quality_of_choice > self::MAX_QUALITY_OF_CHOICE) {
            throw new Exception('Invalid quality_of_choice');
        }
        $this->quality_of_choice = $quality_of_choice;
        return $this;
    }

    /**
     * @return array
     */
    public function getPhraseMetrics(): array
    {
        return $this->phrase_metrics;
    }

    /**
     * @param array $phrase_metrics
     * @return Estimations
     */
    public function setPhraseMetrics(array $phrase_metrics): Estimations
    {
        $this->phrase_metrics = $phrase_metrics;
        return $this;
    }

}