<?php


namespace App\Command;

use App\Entity\Assault;
use App\Exceptions\Assault\AssaultNotFound;
use App\Exceptions\Core\ScenarioNotFound;
use App\Factories\ScenariosFactory;
use App\Repository\AssaultRepository;
use App\Service\AssaultService;
use App\Settings\RequirementsForInterlocutorSettings;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Throwable;

#[AsCommand(name: 'app:attack')]
class AttackController extends Command
{
    /**
     * AttackController constructor.
     * @param AssaultRepository $assaultRepository
     * @param AssaultService $assaultService
     * @param ScenariosFactory $scenariosFactory
     * @param string|null $name
     */
    public function __construct(
        private AssaultRepository $assaultRepository,
        private AssaultService $assaultService,
        private ScenariosFactory $scenariosFactory,
        string $name = null,
    )
    {
        parent::__construct($name);
    }

    protected function configure()
    {
        $this
            ->addArgument('assault_id', InputArgument::OPTIONAL, 'Assault Id')
            ->setDescription('Add a short description for your command')
            ->addOption('mode', 'm', InputOption::VALUE_OPTIONAL, 'Option description', 'cron')
            ->addOption('time-limit', 't', InputOption::VALUE_OPTIONAL, 'Time before down');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null
     * @throws AssaultNotFound|ScenarioNotFound|Throwable
     */
    protected function execute(InputInterface $input, OutputInterface $output): ?int
    {
        $assault = $this->assaultRepository->getById($input->getArgument('assault_id'));
        try {
            if ($this->assaultService->checkAssaultInProcess($assault)) {
                throw new \Exception('Assault in process');
            }
            $assault->setStatus(Assault::STATUS_IN_PROCESS);
            $this->assaultRepository->save($assault, true);

            var_dump("Start:({$assault->getStatus()}) " . $assault->getGlobalType() . ' ' . $assault->getLocalType() . ' ' . $assault->getTargetId() .
                ' by: ' . $assault->getCartrige()->getName());

            $settings = $this->assaultService->getSettings($assault);
            $scenario = $this->scenariosFactory->create($settings->getBehaviorSettings()->getScenarios());
            $scenario->setAssault($assault)->run();
        } catch (Throwable $exception) {
            $assault->setStatus(Assault::STATUS_ERROR);
            $this->assaultRepository->save($assault, true);
            throw $exception;
        }
        return 1;
    }
}