<?php


namespace App\Exceptions;


use Exception;
use Throwable;

/**
 * Class BaseProjectException
 * @package App\Exceptions
 */
class BaseProjectException extends Exception
{
    const CODE = 800000;
    const MESSAGE = 'Liberty Prime unknown error';

    public function __construct(string $message = self::MESSAGE, int $code = self::CODE, ?Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}