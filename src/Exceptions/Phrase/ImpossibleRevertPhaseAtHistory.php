<?php


namespace App\Exceptions\Phrase;


use Throwable;

/**
 * Class ImpossibleRevertPhaseAtHistory
 * @package App\Exceptions\Phrase
 */
class ImpossibleRevertPhaseAtHistory extends BasePhraseException
{
    const CODE = 800203;
    const MESSAGE = 'Impossible revert phase at history.';

    public function __construct(string $message = self::MESSAGE, int $code = self::CODE, ?Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}