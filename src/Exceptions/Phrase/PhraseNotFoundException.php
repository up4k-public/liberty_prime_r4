<?php


namespace App\Exceptions\Phrase;


use Throwable;

/**
 * Class PhraseNotFoundException
 * @package App\Exceptions\Phrase
 */
class PhraseNotFoundException extends BasePhraseException
{
    const CODE = 800201;
    const MESSAGE = 'Phrase not found by Id.';

    public function __construct(string $message = self::MESSAGE, int $code = self::CODE, ?Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}