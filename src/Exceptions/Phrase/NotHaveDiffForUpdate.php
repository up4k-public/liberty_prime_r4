<?php


namespace App\Exceptions\Phrase;


use Throwable;

/**
 * Class NotHaveDiffForUpdate
 * @package App\Exceptions\Phrase
 */
class NotHaveDiffForUpdate extends BasePhraseException
{
    const CODE = 800204;
    const MESSAGE = 'Not have diff for update.';

    public function __construct(string $message = self::MESSAGE, int $code = self::CODE, ?Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}