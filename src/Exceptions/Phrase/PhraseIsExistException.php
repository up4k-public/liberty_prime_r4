<?php


namespace App\Exceptions\Phrase;


use Throwable;

/**
 * Class PhraseIsExistException
 * @package App\Exceptions\Phrase
 */
class PhraseIsExistException extends BasePhraseException
{
    const CODE = 800205;
    const MESSAGE = 'Phrase is exist.';

    public function __construct(string $message = self::MESSAGE, int $code = self::CODE, ?Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}