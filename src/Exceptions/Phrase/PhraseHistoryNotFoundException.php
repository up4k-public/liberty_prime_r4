<?php


namespace App\Exceptions\Phrase;


use Throwable;

/**
 * Class PhraseHistoryNotFoundException
 * @package App\Exceptions\Phrase
 */
class PhraseHistoryNotFoundException extends BasePhraseException
{
    const CODE = 800202;
    const MESSAGE = 'Phrase history not found by Id.';

    public function __construct(string $message = self::MESSAGE, int $code = self::CODE, ?Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}