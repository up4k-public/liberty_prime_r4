<?php


namespace App\Exceptions\Phrase;


use App\Exceptions\BaseProjectException;
use Throwable;

/**
 * Class BasePhraseException
 * @package App\Exceptions\Phrase
 */
class BasePhraseException extends BaseProjectException
{
    const CODE = 800200;
    const MESSAGE = 'Unknown problem in core logic';

    public function __construct(string $message = self::MESSAGE, int $code = self::CODE, ?Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}