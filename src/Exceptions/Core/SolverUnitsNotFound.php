<?php


namespace App\Exceptions\Core;

use Throwable;

/**
 * Class SolverUnitsNotFound
 * @package App\Exceptions\Core
 */
class SolverUnitsNotFound extends BaseCoreException
{
    const CODE = 800901;
    const MESSAGE = 'Not Found Solver Unit';

    public function __construct(string $message = self::MESSAGE, int $code = self::CODE, ?Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}