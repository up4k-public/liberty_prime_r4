<?php


namespace App\Exceptions\Core;


use Throwable;

/**
 * Class ScenarioNotFound
 * @package App\Exceptions\Core
 */
class ScenarioNotFound extends BaseCoreException
{
    const CODE = 800902;
    const MESSAGE = 'Not Found Scenario';

    public function __construct(string $message = self::MESSAGE, int $code = self::CODE, ?Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}