<?php


namespace App\Exceptions\Core;

use App\Exceptions\BaseProjectException;
use Throwable;

/**
 * Class BaseCoreException
 * @package App\Exceptions\Core
 */
class BaseCoreException extends BaseProjectException
{
    const CODE = 800900;
    const MESSAGE = 'Unknown problem with phrase';

    public function __construct(string $message = self::MESSAGE, int $code = self::CODE, ?Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}