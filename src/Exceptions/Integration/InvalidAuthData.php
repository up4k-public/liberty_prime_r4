<?php


namespace App\Exceptions\Integration;

use Throwable;

/**
 * Class InvalidAuthData
 * @package App\Exceptions\Integration
 */
class InvalidAuthData extends BaseIntegrationException
{
    const CODE = 800801;
    const MESSAGE = 'Invalid authData for integration';

    public function __construct(string $message = self::MESSAGE, int $code = self::CODE, ?Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}