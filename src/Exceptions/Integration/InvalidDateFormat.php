<?php


namespace App\Exceptions\Integration;


use Throwable;

/**
 * Class InvalidDateFormat
 * @package App\Exceptions\Integration
 */
class InvalidDateFormat extends BaseIntegrationException
{
    const CODE = 800802;
    const MESSAGE = 'Invalid date format';

    public function __construct(string $message = self::MESSAGE, int $code = self::CODE, ?Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}