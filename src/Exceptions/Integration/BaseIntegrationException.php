<?php


namespace App\Exceptions\Integration;


use App\Exceptions\BaseProjectException;
use Throwable;

/**
 * Class BaseIntegrationException
 * @package App\Exceptions\Integration
 */
class BaseIntegrationException extends BaseProjectException
{
    const CODE = 800800;
    const MESSAGE = 'Unknown problem with integration';

    public function __construct(string $message = self::MESSAGE, int $code = self::CODE, ?Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}