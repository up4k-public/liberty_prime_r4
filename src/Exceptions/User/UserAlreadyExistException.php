<?php


namespace App\Exceptions\User;

use Throwable;

/**
 * Class UserAlreadyExistException
 * @package App\Exceptions\User
 */
class UserAlreadyExistException extends BaseUserException
{
    const CODE = 800101;
    const MESSAGE = 'User already exist';

    public function __construct(string $message = self::MESSAGE, int $code = self::CODE, ?Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}