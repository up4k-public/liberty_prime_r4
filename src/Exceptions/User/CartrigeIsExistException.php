<?php


namespace App\Exceptions\User;


use Throwable;

/**
 * Class CartrigeIsExistException
 * @package App\Exceptions\User
 */
class CartrigeIsExistException extends BaseUserException
{
    const CODE = 800104;
    const MESSAGE = 'Cartrige is exist.';

    public function __construct(string $message = self::MESSAGE, int $code = self::CODE, ?Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}