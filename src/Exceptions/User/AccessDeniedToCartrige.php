<?php


namespace App\Exceptions\User;


use Throwable;

/**
 * Class AccessDeniedToCartrige
 * @package App\Exceptions\User
 */
class AccessDeniedToCartrige extends BaseUserException
{
    const CODE = 800107;
    const MESSAGE = 'Access denied to cartrige.';

    public function __construct(string $message = self::MESSAGE, int $code = self::CODE, ?Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}