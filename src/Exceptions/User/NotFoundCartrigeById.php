<?php


namespace App\Exceptions\User;


use Throwable;

/**
 * Class NotFoundCartrigeById
 * @package App\Exceptions\User
 */
class NotFoundCartrigeById extends BaseUserException
{
    const CODE = 800105;
    const MESSAGE = 'Not found cartrige by id.';

    public function __construct(string $message = self::MESSAGE, int $code = self::CODE, ?Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}