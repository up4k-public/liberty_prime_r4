<?php


namespace App\Exceptions\User;

use Throwable;

/**
 * Class InviteHasBeenUsedException
 * @package App\Exceptions\User
 */
class InviteHasBeenUsedException extends BaseUserException
{
    const CODE = 800103;
    const MESSAGE = 'Invite has been used';

    public function __construct(string $message = self::MESSAGE, int $code = self::CODE, ?Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}