<?php


namespace App\Exceptions\User;


use Throwable;

/**
 * Class InvalidInvite
 * @package App\Exceptions\User
 */
class InvalidInviteException extends BaseUserException
{
    const CODE = 800102;
    const MESSAGE = 'Invalid invite';

    public function __construct(string $message = self::MESSAGE, int $code = self::CODE, ?Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}