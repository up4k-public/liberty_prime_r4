<?php


namespace App\Exceptions\User;


use Throwable;

/**
 * Class AttemptToUpdateAnotherAccount
 * @package App\Exceptions\User
 */
class AttemptToUpdateAnotherAccount extends BaseUserException
{
    const CODE = 800106;
    const MESSAGE = 'Attempt to update another account.';

    public function __construct(string $message = self::MESSAGE, int $code = self::CODE, ?Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}