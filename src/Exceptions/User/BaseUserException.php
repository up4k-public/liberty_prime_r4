<?php


namespace App\Exceptions\User;


use App\Exceptions\BaseProjectException;
use Throwable;

/**
 * Class BaseUserException
 * @package App\Exceptions\User
 */
class BaseUserException extends BaseProjectException
{
    const CODE = 800100;
    const MESSAGE = 'Unknown problem with user';

    public function __construct(string $message = self::MESSAGE, int $code = self::CODE, ?Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}