<?php


namespace App\Exceptions\Assault;


use App\Exceptions\BaseProjectException;
use Throwable;

/**
 * Class BasePhraseException
 * @package App\Exceptions\Phrase
 */
class BaseAssaultException extends BaseProjectException
{
    const CODE = 800300;
    const MESSAGE = 'Unknown problem with Assault';

    public function __construct(string $message = self::MESSAGE, int $code = self::CODE, ?Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}