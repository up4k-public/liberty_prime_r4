<?php


namespace App\Exceptions\Assault;


use Throwable;

/**
 * Class AssaultNotFound
 * @package App\Exceptions\Assault
 */
class AssaultNotFound extends  BaseAssaultException
{
    const CODE = 800301;
    const MESSAGE = 'Not Found Assault By Id';

    public function __construct(string $message = self::MESSAGE, int $code = self::CODE, ?Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}