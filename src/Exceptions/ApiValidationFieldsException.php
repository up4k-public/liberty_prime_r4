<?php


namespace App\Exceptions;

use Throwable;

/**
 * Class ApiValidationFieldsException
 * @package App\Exceptions
 */
class ApiValidationFieldsException extends BaseProjectException
{
    const CODE = 801000;
    const MESSAGE = 'Api Request Fields Error';

    public function __construct(string $message = self::MESSAGE, int $code = self::CODE, ?Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

    public $fields = [];

    /**
     * @return array
     */
    public function getFields(): array
    {
        return $this->fields;
    }

    /**
     * @param array $fields
     * @return ApiValidationFieldsException
     */
    public function setFields(array $fields): ApiValidationFieldsException
    {
        $this->fields = $fields;
        return $this;
    }

}