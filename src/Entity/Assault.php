<?php

namespace App\Entity;

use App\Repository\AssaultRepository;
use App\Settings\Settings;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: AssaultRepository::class)]
class Assault
{
    const FILTER_NAME = [
        'owner_id',
        'local_type',
        'global_type',
        'cartrige_id',
        'target_id',
    ];

    const STATUS_NEW = 'new';
    const STATUS_IN_PROCESS = 'in_process';
    const STATUS_ERROR = 'error';
    const STATUS_CANCELLED = 'cancelled';

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $global_type = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $local_type = null;

    #[ORM\Column(length: 255)]
    private ?string $target_id = null;

    #[ORM\ManyToOne(inversedBy: 'assaults')]
    private ?Cartrige $cartrige = null;

    #[ORM\Column(nullable: true)]
    private array $settings = [];

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $ts_created = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $ts_updated = null;

    #[ORM\ManyToOne]
    private ?User $updated_by = null;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: false)]
    private ?User $created_by = null;

    #[ORM\Column(length: 255)]
    private ?string $status = null;

    #[ORM\OneToMany(mappedBy: 'assault', targetEntity: Log::class, orphanRemoval: true)]
    private Collection $logs;

    public function __construct()
    {
        $this->logs = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGlobalType(): ?string
    {
        return $this->global_type;
    }

    public function setGlobalType(string $global_type): self
    {
        $this->global_type = $global_type;

        return $this;
    }

    public function getLocalType(): ?string
    {
        return $this->local_type;
    }

    public function setLocalType(?string $local_type): self
    {
        $this->local_type = $local_type;

        return $this;
    }

    public function getTargetId(): ?string
    {
        return $this->target_id;
    }

    public function setTargetId(string $target_id): self
    {
        $this->target_id = $target_id;

        return $this;
    }


    public function getCartrige(): ?Cartrige
    {
        return $this->cartrige;
    }

    public function setCartrige(?Cartrige $cartrige): self
    {
        $this->cartrige = $cartrige;

        return $this;
    }

    public function getSettings(): array
    {
        return $this->settings;
    }

    public function getTsCreated(): ?\DateTimeInterface
    {
        return $this->ts_created;
    }

    public function setTsCreated(\DateTimeInterface $ts_created): self
    {
        $this->ts_created = $ts_created;

        return $this;
    }

    public function setSettings(Settings $settings): self
    {
       $this->settings = json_decode(json_encode($settings), true);
       return $this;
    }

    public function getTsUpdated(): ?\DateTimeInterface
    {
        return $this->ts_updated;
    }

    public function setTsUpdated(?\DateTimeInterface $ts_updated): self
    {
        $this->ts_updated = $ts_updated;

        return $this;
    }

    public function getUpdatedBy(): ?User
    {
        return $this->updated_by;
    }

    public function setUpdatedBy(?User $updated_by): self
    {
        $this->updated_by = $updated_by;

        return $this;
    }

    public function getCreatedBy(): ?User
    {
        return $this->created_by;
    }

    public function setCreatedBy(?User $created_by): self
    {
        $this->created_by = $created_by;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return Collection<int, Log>
     */
    public function getLogs(): Collection
    {
        return $this->logs;
    }

    public function addLog(Log $log): self
    {
        if (!$this->logs->contains($log)) {
            $this->logs->add($log);
            $log->setAssault($this);
        }

        return $this;
    }

    public function removeLog(Log $log): self
    {
        if ($this->logs->removeElement($log)) {
            // set the owning side to null (unless already changed)
            if ($log->getAssault() === $this) {
                $log->setAssault(null);
            }
        }

        return $this;
    }

}
