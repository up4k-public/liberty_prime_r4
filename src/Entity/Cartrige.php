<?php

namespace App\Entity;

use App\Core\Author;
use App\Repository\CartrigeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\SerializerInterface;

#[ORM\Entity(repositoryClass: CartrigeRepository::class)]
class Cartrige
{
    const AVAILABLE_SORT_FIELD = [
        'id',
        'name',
        'ts_created',
    ];

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\OneToMany(mappedBy: 'cartrige', targetEntity: Assault::class)]
    private Collection $assaults;

    #[ORM\Column(length: 255)]
    private ?string $integration_type = null;

    #[ORM\Column(nullable: true)]
    private array $info = [];

    #[ORM\Column]
    private array $auth_data = [];

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $ts_created = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $ts_updated = null;

    #[ORM\Column(nullable: true)]
    private ?bool $is_up4k_warior = null;

    #[ORM\ManyToMany(targetEntity: User::class, mappedBy: 'cartriges')]
    private Collection $users;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $name = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $external_id = null;

    #[ORM\ManyToOne(inversedBy: 'owner_cartriges')]
    private ?User $owner = null;

    public function __construct()
    {
        $this->assaults = new ArrayCollection();
        $this->users = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection<int, Assault>
     */
    public function getAssaults(): Collection
    {
        return $this->assaults;
    }

    public function addAssault(Assault $assault): self
    {
        if (!$this->assaults->contains($assault)) {
            $this->assaults->add($assault);
            $assault->setCartrige($this);
        }

        return $this;
    }

    public function removeAssault(Assault $assault): self
    {
        if ($this->assaults->removeElement($assault)) {
            // set the owning side to null (unless already changed)
            if ($assault->getCartrige() === $this) {
                $assault->setCartrige(null);
            }
        }

        return $this;
    }

    public function getIntegrationType(): ?string
    {
        return $this->integration_type;
    }

    public function setIntegrationType(string $integration_type): self
    {
        $this->integration_type = $integration_type;

        return $this;
    }

    public function getInfo(SerializerInterface $serializer = null): Author
    {
        return $serializer->deserialize(json_encode($this->info), Author::class, 'json');
    }

    public function setInfo(array $info): self
    {
        $this->info = $info;

        return $this;
    }

    public function getAuthData(): array
    {
        return $this->auth_data;
    }

    public function setAuthData(array $auth_data): self
    {
        $this->auth_data = $auth_data;

        return $this;
    }

    public function getTsCreated(): ?\DateTimeInterface
    {
        return $this->ts_created;
    }

    public function setTsCreated(\DateTimeInterface $ts_created): self
    {
        $this->ts_created = $ts_created;

        return $this;
    }

    public function getTsUpdated(): ?\DateTimeInterface
    {
        return $this->ts_updated;
    }

    public function setTsUpdated(\DateTimeInterface $ts_updated): self
    {
        $this->ts_updated = $ts_updated;

        return $this;
    }

    public function isIsUp4kWarior(): ?bool
    {
        return $this->is_up4k_warior;
    }

    public function setIsUp4kWarior(?bool $is_up4k_warior): self
    {
        $this->is_up4k_warior = $is_up4k_warior;

        return $this;
    }

    /**
     * @return Collection<int, User>
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users->add($user);
            $user->addCartrige($this);
        }

        return $this;
    }

    public function addUsers(?array $users):self
    {
        $old_users = $this->getUsers();
        foreach ($old_users as $old_user) {
            $this->removeUser($old_user);
        }

        if (empty($users)) {
            return $this;
        }
        foreach ($users as $user) {
            $this->addUser($user);
        }
        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->removeElement($user)) {
            $user->removeCartrige($this);
        }

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getExternalId(): ?string
    {
        return $this->external_id;
    }

    public function setExternalId(?string $external_id): self
    {
        $this->external_id = $external_id;

        return $this;
    }

    public function getOwner(): ?User
    {
        return $this->owner;
    }

    public function setOwner(?User $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

}
