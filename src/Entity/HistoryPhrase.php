<?php

namespace App\Entity;

use App\Dto\Api\Phrase\Phrase as PhraseDto;
use App\Exceptions\Phrase\NotHaveDiffForUpdate;
use App\Repository\HistoryPhraseRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: HistoryPhraseRepository::class)]
class HistoryPhrase
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(nullable: true)]
    private ?array $before = [];

    #[ORM\Column(nullable: true)]
    private ?array $after = [];

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $ts_created = null;

    #[ORM\Column(length: 255)]
    private ?string $updated_by = null;

    #[ORM\ManyToOne(inversedBy: 'historyPhrases')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Phrase $phrase = null;

    /**
     * @return bool
     * @throws NotHaveDiffForUpdate
     */
    public function checkHaveDiff():bool
    {
        if (json_encode($this->getBefore()) == json_encode($this->getAfter())) {
            throw new NotHaveDiffForUpdate();
        }
        return true;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBefore(): array
    {
        return $this->before;
    }

    public function setBefore(?PhraseDto $before): self
    {
        if (empty($before)) {
            $this->before = [];
        } else {
            $this->before = json_decode(json_encode($before), true);
        }

        return $this;
    }

    public function getAfter(): array
    {
        return $this->after;
    }

    public function setAfter(?PhraseDto $after): self
    {
        if (empty($after)) {
            $this->after = [];
        } else {
            $this->after = json_decode(json_encode($after), true);
        }

        return $this;
    }

    public function getTsCreated(): ?\DateTimeInterface
    {
        return $this->ts_created;
    }

    public function setTsCreated(\DateTimeInterface $ts_created): self
    {
        $this->ts_created = $ts_created;

        return $this;
    }

    public function getUpdatedBy(): ?string
    {
        return $this->updated_by;
    }

    public function setUpdatedBy(string $updated_by): self
    {
        $this->updated_by = $updated_by;

        return $this;
    }

    public function getPhrase(): ?Phrase
    {
        return $this->phrase;
    }

    public function setPhrase(?Phrase $phrase): self
    {
        $this->phrase = $phrase;

        return $this;
    }
}
