<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

#[ORM\Entity(repositoryClass: UserRepository::class)]
#[ORM\Table(name: '`user`')]
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    const AVAILABLE_SORT_FIELD = [
        'id',
        'name',
        'is_banned',
        'ts_created'
    ];

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255, unique: true)]
    private ?string $name = null;

    #[ORM\Column(length: 255)]
    private ?string $password = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $ts_created = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $ts_updated = null;

    #[ORM\Column(nullable: true)]
    private ?bool $is_banned = null;

    #[ORM\ManyToMany(targetEntity: Cartrige::class, inversedBy: 'users')]
    private Collection $cartriges;

    #[ORM\Column(type: 'json')]
    private $roles = [];

    #[ORM\OneToMany(mappedBy: 'owner', targetEntity: Invite::class, orphanRemoval: true)]
    private Collection $invites;

    #[ORM\OneToOne(mappedBy: 'registered_by', cascade: ['persist', 'remove'])]
    private ?Invite $owner_invite = null;

    #[ORM\OneToMany(mappedBy: 'owner', targetEntity: Cartrige::class)]
    private Collection $owner_cartriges;

    public function __construct()
    {
        $this->cartriges = new ArrayCollection();
        $this->invites = new ArrayCollection();
        $this->owner_cartriges = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getTsCreated(): ?\DateTimeInterface
    {
        return $this->ts_created;
    }

    public function setTsCreated(\DateTimeInterface $ts_created): self
    {
        $this->ts_created = $ts_created;

        return $this;
    }

    public function getTsUpdated(): ?\DateTimeInterface
    {
        return $this->ts_updated;
    }

    public function setTsUpdated(\DateTimeInterface $ts_updated): self
    {
        $this->ts_updated = $ts_updated;

        return $this;
    }

    public function isIsBanned(): ?bool
    {
        return $this->is_banned;
    }

    public function setIsBanned(?bool $is_banned): self
    {
        $this->is_banned = $is_banned;

        return $this;
    }

    /**
     * @return Collection<int, Cartrige>
     */
    public function getCartriges(): Collection
    {
        return $this->cartriges;
    }

    public function addCartrige(Cartrige $cartrige): self
    {
        if (!$this->cartriges->contains($cartrige)) {
            $this->cartriges->add($cartrige);
        }

        return $this;
    }

    public function removeCartrige(Cartrige $cartrige): self
    {
        $this->cartriges->removeElement($cartrige);

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->name;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }


    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    /**
     * @return Collection<int, Invite>
     */
    public function getInvites(): Collection
    {
        return $this->invites;
    }

    public function addInvite(Invite $invite): self
    {
        if (!$this->invites->contains($invite)) {
            $this->invites->add($invite);
            $invite->setOwner($this);
        }

        return $this;
    }

    public function removeInvite(Invite $invite): self
    {
        if ($this->invites->removeElement($invite)) {
            // set the owning side to null (unless already changed)
            if ($invite->getOwner() === $this) {
                $invite->setOwner(null);
            }
        }

        return $this;
    }

    public function getOwnerInvite(): ?Invite
    {
        return $this->owner_invite;
    }

    public function setOwnerInvite(?Invite $owner_invite): self
    {
        // unset the owning side of the relation if necessary
        if ($owner_invite === null && $this->owner_invite !== null) {
            $this->owner_invite->setRegisteredBy(null);
        }

        // set the owning side of the relation if necessary
        if ($owner_invite !== null && $owner_invite->getRegisteredBy() !== $this) {
            $owner_invite->setRegisteredBy($this);
        }

        $this->owner_invite = $owner_invite;

        return $this;
    }

    /**
     * @return Collection<int, Cartrige>
     */
    public function getOwnerCartriges(): Collection
    {
        return $this->owner_cartriges;
    }

    public function addOwnerCartrige(Cartrige $ownerCartrige): self
    {
        if (!$this->owner_cartriges->contains($ownerCartrige)) {
            $this->owner_cartriges->add($ownerCartrige);
            $ownerCartrige->setOwner($this);
        }

        return $this;
    }

    public function removeOwnerCartrige(Cartrige $ownerCartrige): self
    {
        if ($this->owner_cartriges->removeElement($ownerCartrige)) {
            // set the owning side to null (unless already changed)
            if ($ownerCartrige->getOwner() === $this) {
                $ownerCartrige->setOwner(null);
            }
        }

        return $this;
    }

}
