<?php

namespace App\Entity;

use App\Dto\Api\Phrase\Request\GetOne as PhraseGetOneDto;
use App\Dto\Api\Phrase\Response\GetOne;
use App\Repository\PhraseRepository as PhraseRepositoryAlias;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use App\Dto\Api\Phrase\Phrase as PhraseDto;

#[ORM\Entity(repositoryClass: PhraseRepositoryAlias::class)]
class Phrase
{
    const FILTER_NAME = [
        'sex',
        'params',
        'triggers',
        'updated_by',
        'modules',
        'author',
        'tags'
    ];

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: Types::TEXT)]
    private ?string $text = null;

    #[ORM\Column]
    private ?int $sex = null;

    #[ORM\Column(nullable: true)]
    private ?array $params = [];

    #[ORM\Column(nullable: true)]
    private ?array $assessment = [];

    #[ORM\Column(nullable: true)]
    private ?array $triggers = [];

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $ts_created = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $ts_updated = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $updated_by = null;

    #[ORM\Column(nullable: true)]
    private ?array $tags = [];

    #[ORM\Column(nullable: true)]
    private ?array $modules = [];

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $author = null;

    #[ORM\OneToMany(mappedBy: 'phrase', targetEntity: HistoryPhrase::class, orphanRemoval: true)]
    private Collection $historyPhrases;

    public function __construct()
    {
        $this->historyPhrases = new ArrayCollection();
    }

    /**
     * Возвращаем DTO с которым работате API.
     * Похоже на entity, но есть и отличия
     * @return PhraseDto
     */
    public function getPhraseDto(): PhraseDto
    {
        return (new PhraseDto())
            ->setId($this->getId())
            ->setUpdatedBy($this->getUpdatedBy())
            ->setModules($this->getModules())
            ->setTags($this->getTags())
            ->setTriggers($this->getTriggers())
            ->setAssessment($this->getAssessment())
            ->setParams($this->getParams())
            ->setText($this->getText())
            ->setAuthor($this->getAuthor());
    }

    public function getPhraseDtoWithHistory(int $history_limit = 20, int $history_offset = 0): GetOne
    {
        return (new GetOne())
            ->setId($this->getId())
            ->setUpdatedBy($this->getUpdatedBy())
            ->setModules($this->getModules())
            ->setTags($this->getTags())
            ->setTriggers($this->getTriggers())
            ->setAssessment($this->getAssessment())
            ->setParams($this->getParams())
            ->setText($this->getText())
            ->setHistory($this->getHistoryWithPaginate($history_limit, $history_offset))
            ->setHistoryCount(count($this->getHistoryPhrases()->toArray()))
            ->setAuthor($this->getAuthor());
    }

    /**
     * @param PhraseDto $input
     * @return $this
     */
    public function setPhraseDto(PhraseDto $input): self
    {
        $this->setText($input->getText())
            ->setSex(1)
            ->setParams($input->getParams())
            ->setAssessment($input->getAssessment())
            ->setTriggers($input->getTriggers())
            ->setModules($input->getModules())
            ->setTsUpdated(new DateTime()) //todo в фоне
            ->setTags($input->getTags());
        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getSex(): ?int
    {
        return $this->sex;
    }

    public function setSex(int $sex): self
    {
        $this->sex = $sex;

        return $this;
    }

    public function getParams(): array
    {
        return $this->params;
    }

    public function setParams(?array $params): self
    {
        $this->params = $params;

        return $this;
    }

    public function getAssessment(): array
    {
        return $this->assessment;
    }

    public function setAssessment(?array $assessment): self
    {
        $this->assessment = $assessment;

        return $this;
    }

    public function getTsCreated(): ?\DateTimeInterface
    {
        return $this->ts_created;
    }

    public function setTsCreated(\DateTimeInterface $ts_created): self
    {
        $this->ts_created = $ts_created;

        return $this;
    }

    public function getTsUpdated(): ?\DateTimeInterface
    {
        return $this->ts_updated;
    }

    public function setTsUpdated(\DateTimeInterface $ts_updated): self
    {
        $this->ts_updated = $ts_updated;

        return $this;
    }

    public function getUpdatedBy(): ?string
    {
        return $this->updated_by;
    }

    public function setUpdatedBy(?string $updated_by): self
    {
        $this->updated_by = $updated_by;

        return $this;
    }

    public function getTriggers(): ?array
    {
        return $this->triggers;
    }

    public function setTriggers(?array $triggers): Phrase
    {
        $this->triggers = $triggers;
        return $this;
    }

    public function getTags(): ?array
    {
        return $this->tags;
    }

    public function setTags(?array $tags): self
    {
        $this->tags = $tags;

        return $this;
    }

    public function getModules(): ?array
    {
        return $this->modules;
    }

    public function setModules(?array $modules): self
    {
        $this->modules = $modules;

        return $this;
    }

    public function getAuthor(): ?string
    {
        return $this->author;
    }

    public function setAuthor(?string $author): self
    {
        $this->author = $author;

        return $this;
    }

    /**
     * @return Collection<int, HistoryPhrase>
     */
    public function getHistoryPhrases(): Collection
    {
        return $this->historyPhrases;
    }

    /**
     * @param int $limit
     * @param int $offset
     * @return array
     */
    public function getHistoryWithPaginate(int $limit, int $offset): array
    {
        //array_slice не подойдёт
        $history = $this->getHistoryPhrases()->toArray();
        $result = [];
        for ($i = $offset; $i < $limit; $i++) {
            if (isset($history[$i])) {
                $result[] = $history[$i];
            }
        }
        return $result;
    }

    public function addHistoryPhrase(HistoryPhrase $historyPhrase): self
    {
        if (!$this->historyPhrases->contains($historyPhrase)) {
            $this->historyPhrases->add($historyPhrase);
            $historyPhrase->setPhrase($this);
        }

        return $this;
    }

    public function removeHistoryPhrase(HistoryPhrase $historyPhrase): self
    {
        if ($this->historyPhrases->removeElement($historyPhrase)) {
            // set the owning side to null (unless already changed)
            if ($historyPhrase->getPhrase() === $this) {
                $historyPhrase->setPhrase(null);
            }
        }

        return $this;
    }

}
