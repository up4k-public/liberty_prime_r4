<?php


namespace App\Factories;

use App\Settings\AbstractSolversModuleSettings;
use Exception;

/**
 * Class UnitsSettingsFactory
 * @package App\Factories
 */
class UnitsSettingsFactory
{
    const NAMESPACE = "App\Solvers\UnitsSettings\\";
    const NAMESPACE_UNIT = "App\Solvers\Units\\";
    const POSTFIX = 'Settings';

    const PATH = 'src/Solvers/Units';

    /**
     * @param string $name
     * @return AbstractSolversModuleSettings
     * @throws Exception
     */
    public function create(string $name): AbstractSolversModuleSettings
    {
        $name = self::NAMESPACE . $name . self::POSTFIX;

        $class = new $name();

        if (!($class instanceof AbstractSolversModuleSettings)) {
            throw new Exception($name . " must be extend " . AbstractSolversModuleSettings::class);
        }

        return $class;
    }

    /**
     * Создаём для каждого модуля класс настроек. Если на новом модуле нет класса настроек, то падаём
     * @return array
     * @throws Exception
     */
    public function makeAllUnitsSettings(): array
    {
        $names = array_diff(scandir(self::PATH), ['..', '.']);
        foreach ($names as &$name) {
            $name = str_replace('.php', '', $name);
        }
        unset($name);

        if (empty($names)) {
            throw new Exception('Not Found Solver UnitSettings');
        }

        $result = [];
        foreach ($names as $name) {
            $class = $this->create($name);
            $result[self::generateKey($class::class)] = $class;
        }
        return $result;
    }

    /**
     * Генерируем более короткий ключ для массива настроек модуля, чтобы json был симпатичный
     * На вход берём или имя класса модуля или класса-настроек, ну так получилось TODO исправить на два метода (?)
     * @param string $class_name
     * @return string
     */
    public static function generateKey(string $class_name):string
    {
        return lcfirst(
            str_replace(self::NAMESPACE_UNIT,'', str_replace(self::NAMESPACE, '', $class_name)));
    }

}