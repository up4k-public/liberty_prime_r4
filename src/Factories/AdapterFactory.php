<?php


namespace App\Factories;

use App\Core\BaseAdapter;
use App\Exceptions\Core\BaseCoreException;
use Error;
use Exception;

/**
 * Class AdapterFactory
 * @package App\Integrations
 */
class AdapterFactory
{
    const NAMESPACE = "App\Integrations\\";
    const FOLDER_NAME = 'Adapters';
    const PREFIX_NAME = 'Adapter';

    /**
     * @param string $name
     * @param string $type
     * @param string|null $local_type
     * @return BaseAdapter|null
     * @throws BaseCoreException
     */
    public function create(string $name, string $type, ?string $local_type = null): ?BaseAdapter
    {
        $class_name = self::NAMESPACE . $type . "\\" . self::FOLDER_NAME . "\\";

        if (!empty($local_type)) {
            $class_name .= $local_type . "\\";
        }
        $class_name .= self::PREFIX_NAME . $name;

        try {
            $adapter = new $class_name;
        } catch (Error $exception) {
            return null;
        }

        if (!($adapter instanceof BaseAdapter)) {
            throw new BaseCoreException(self::PREFIX_NAME . $name . " must be extend " . BaseAdapter::class);
        }
        return $adapter;
    }
}