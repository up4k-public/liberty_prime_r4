<?php


namespace App\Factories;

use App\Exceptions\Core\BaseCoreException;
use App\Exceptions\Integration\InvalidAuthData;
use App\Integrations\Api;
use App\Service\ValidationDtoService;
use JMS\Serializer\SerializerInterface;

/**
 * Class IntegrationsFactory
 * @package App\Factories
 */
class IntegrationsFactory
{
    const PATH = '../src/Integrations/';
    const INTERACTORS_FOLDER = '/Interactors';
    const NAMESPACE = "App\Integrations\\";

    /**
     * IntegrationsFactory constructor.
     * @param SerializerInterface $serializer
     * @param ValidationDtoService $validationDtoService
     */
    public function __construct(
        private SerializerInterface $serializer,
        private ValidationDtoService $validationDtoService,
    )
    {
    }

    /**
     * @param string $name имя интеграции for example VkApi
     * @param array|null $data заходки (логин:пароль, токен)
     * @return Api
     * @throws BaseCoreException|InvalidAuthData
     */
    public function create(string $name, ?array $data = null): Api
    {
        $name = self::NAMESPACE . $name . "\\" . $name . 'Api';
        $api = new $name($this->serializer, $this->validationDtoService);

        if (!($api instanceof Api)) {
            throw new BaseCoreException($name . " must be extend " . Api::class);
        }
        if (!$api->setAuthorization($data)) {
            throw new InvalidAuthData();
        }

        return $api;
    }


    /**
     * @return array
     */
    public static function getAllNames(): array
    {
        $names = array_diff(scandir(self::PATH), ['..', '.']);

        $result = [];
        foreach ($names as $name) {
            if (!str_contains($name, '.php')) {
                $result[] = $name;
            }
        }
        return $result;
    }

    /**
     * @param string $integrationName
     * @return array|null
     */
    public static function getIntegrationSubNames(string $integrationName): ?array
    {
        $names = array_diff(scandir(self::PATH . $integrationName . self::INTERACTORS_FOLDER), ['..', '.']);

        $result = [];
        foreach ($names as $name) {
            if (!str_contains($name, '.php')) {
                $result[] = $name;
            }
        }
        if (empty($result)) {
            return null;
        }
        return $result;
    }
}