<?php


namespace App\Factories;

use App\Core\BaseAdapter;
use App\Core\Interactors\AbstractInteractors;
use App\Exceptions\Core\BaseCoreException;
use App\Integrations\Api;
use App\Repository\LogRepository;
use Exception;

/**
 * Class InteractorsFactory
 * @package App\Integrations
 */
class InteractorsFactory
{
    const NAMESPACE_PRE = "App\Integrations\\";
    const NAMESPACE_POST = "Interactors\\";

    /**
     * InteractorsFactory constructor.
     * @param LogRepository $eventLogger
     */
    public function __construct(
        private LogRepository $eventLogger)
    {
    }

    /**
     * @param Api $api
     * @param string $name
     * @param string $integration_type
     * @param string|null $local_type
     * @param BaseAdapter|null $adapter
     * @return AbstractInteractors
     * @throws BaseCoreException
     */
    public function create(Api $api, string $name, string $integration_type, ?string $local_type = null, BaseAdapter $adapter = null): AbstractInteractors
    {
        $class_name = self::NAMESPACE_PRE . $integration_type . "\\" . self::NAMESPACE_POST;

        if (!empty($local_type)) {
            $class_name .= $local_type . "\\";
        }
        $class_name .= $name;

        try {
            try {
                $class = new $class_name($this->eventLogger, $api, $adapter);
            } catch (\Error $exception) {
                if (!empty($local_type)) { //Если не получилось создать при переданном $local_type, то пробуем еще раз без него; Будем чуть лояльнее к автору свежей интеграции
                    $class_name = self::NAMESPACE_PRE . $integration_type . "\\" . self::NAMESPACE_POST . $name;
                    $class = new $class_name($api, $adapter);
                } else {
                    throw new BaseCoreException('Can not create: ' . $class_name . ' ' . $exception->getMessage());
                }
            }
        } catch (\Error $exception) {
            throw new BaseCoreException('Can not create: ' . $class_name . ' ' . $exception->getMessage());
        }

        if (!($class instanceof AbstractInteractors)) {
            throw new BaseCoreException($class_name . ' must be extend ' . AbstractInteractors::class);
        }
        return $class;

    }
}