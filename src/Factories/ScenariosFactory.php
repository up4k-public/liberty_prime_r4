<?php


namespace App\Factories;

use App\Exceptions\Core\ScenarioNotFound;
use App\Scenarios\AbstractScenario;
use App\Service\AssaultService;
use App\Service\BattleFieldService;
use App\Service\MessageService;
use App\Solvers\Overmind;

/**
 * Class ScenariosFactory
 * @package App\Factories
 */
class ScenariosFactory
{
    const PATH = 'src/Scenarios';
    const NAMESPACE = "App\Scenarios\\";

    /**
     * Scenario constructor.
     * @param IntegrationsFactory $integrationsFactory
     * @param InteractorsFactory $interactorsFactory
     * @param AdapterFactory $adapterFactory
     * @param AssaultService $assaultService
     * @param BattleFieldService $battleFieldService
     * @param MessageService $messageService
     * @param Overmind $overmind
     */
    public function __construct(
        private IntegrationsFactory $integrationsFactory,
        private InteractorsFactory $interactorsFactory,
        private AdapterFactory $adapterFactory,
        protected AssaultService $assaultService,
        protected BattleFieldService $battleFieldService,
        protected MessageService $messageService,
        protected Overmind $overmind)
    {
    }

    /**
     * @param string $name
     * @return AbstractScenario
     * @throws ScenarioNotFound
     */
    public function create(string $name): AbstractScenario
    {
        $name = self::NAMESPACE . $name;

        $class = new $name($this->integrationsFactory,
            $this->interactorsFactory,
            $this->adapterFactory,
            $this->assaultService,
            $this->battleFieldService,
            $this->messageService,
            $this->overmind);

        if (!($class instanceof AbstractScenario)) {
            throw new ScenarioNotFound(ScenarioNotFound::MESSAGE . ' ' . $name);
        }

        return $class;
    }

    /**
     * TODO похож на такой у UnitsFactory, вынести копипасту
     * @param bool $flag_run_by_api
     * @return array
     * @throws ScenarioNotFound
     */
    public function getAllNames($flag_run_by_api = false): array
    {
        $names = array_diff(scandir($this->getPath($flag_run_by_api)), ['..', '.']);

        foreach ($names as &$name) {
            $name = str_replace('.php', '', $name);
        }
        unset($name);

        if (empty($names)) {
            throw new ScenarioNotFound();
        }
        return array_values($names);
    }

    /**
     * @param false $flag_run_by_api
     * @return string
     */
    private function getPath($flag_run_by_api = false): string
    {
        if ($flag_run_by_api) {
            return '../' . self::PATH;
        }
        return self::PATH;
    }

}