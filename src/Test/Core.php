<?php


namespace App\Test;


class Core
{
   private $api;

    /**
     * Core constructor.
     * @param $api
     */
    public function __construct($api)
    {
        $this->api = $api;
    }

    /**
     * @return mixed
     */
    public function getApi()
    {
        return $this->api;
    }

    /**
     * @param mixed $api
     * @return Core
     */
    public function setApi($api)
    {
        $this->api = $api;
        return $this;
    }



}