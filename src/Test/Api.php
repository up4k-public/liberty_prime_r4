<?php


namespace App\Test;


class Api
{
   private $token;

    /**
     * @return mixed
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param mixed $token
     * @return Api
     */
    public function setToken($token)
    {
        $this->token = $token;
        return $this;
    }

}