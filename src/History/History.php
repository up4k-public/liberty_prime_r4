<?php


namespace App\History;

use App\Core\BattleField;
use App\Core\ResponseMessage;
use App\Settings\HistorySettings;
use App\Solvers\Solution;

/**
 * Class History
 * Содержит историю текущего диалога, историю взимодействия текущего аватара-бота с собеседником
 * @package App\History
 */
class History
{
    const MAX_HISTORY_EXTERNAL_IDS = 500;

    private BattleField $battle_field;
    private ?array $chain_of_events;
    private ?int $level_perturbation;
    private HistorySettings $settings;
    private ?string $first_external_id = null;

    private ?array $ids = null;
    private array $external_ids = [];
    private array $response_messages = [];
    private array $solutions = [];

    /**
     * @return array|null
     */
    public function getIds(): ?array
    {
        return $this->ids;
    }

    /**
     * @param array|null $ids
     * @return History
     */
    public function setIds(?array $ids): History
    {
        $this->ids = $ids;
        return $this;
    }

    /**
     * @return HistorySettings
     */
    public function getSettings(): HistorySettings
    {
        return $this->settings;
    }

    /**
     * @param HistorySettings $settings
     * @return History
     */
    public function setSettings(HistorySettings $settings): History
    {
        $this->settings = $settings;
        return $this;
    }


    /**
     * @param int|null $id
     * @return $this
     */
    public function addIds(?int $id): History
    {
        if (is_null($id)) {
            return $this;
        }
        if (is_array($this->ids)) {
            $this->ids[] = $id;
        }
        if (is_null($this->ids)) {
            $this->ids = [$id];
        }

        return $this;
    }

    /**
     * @return array
     */
    public function getExternalIds(): array
    {
        return $this->external_ids;
    }

    /**
     * @param array $ids
     * @return $this
     */
    public function addExternalIds(array $ids): self
    {
        $this->external_ids = array_unique(array_merge($this->external_ids, $ids));
        $this->checkLimitExternalIds();
        return $this;
    }

    /**
     * @return string|null
     */
    public function getFirstExternalId(): ?string
    {
        return $this->first_external_id;
    }

    /**
     * @param string|null $first_external_id
     * @return History
     */
    public function setFirstExternalId(?string $first_external_id): History
    {
        $this->first_external_id = $first_external_id;
        return $this;
    }


    /**
     * В массив айдишников сообщений не позволяем скопиться больше, чем указанно в настройках
     * TODO объединить с частью новой логики в добавлении response_messages
     * @return $this
     */
    private function checkLimitExternalIds(): self
    {
        $count = count($this->external_ids);
        if ($count > $this->getSettings()->getMaxCountOfMessageInDialogForAnalysis()) {
            for ($i = 0; $i <= ($count - $this->getSettings()->getMaxCountOfMessageInDialogForAnalysis() - 1); $i++) {
                unset($this->external_ids[$i]);
            }
            $this->external_ids = array_values($this->external_ids);
        }
        return $this;
    }

    /**
     * @return array
     */
    public function getResponseMessages(): array
    {
        return $this->response_messages;
    }

    /**
     * @param array $response_messages
     * @return History
     */
    public function setResponseMessages(array $response_messages): History
    {
        $this->response_messages = $response_messages;
        return $this;
    }

    /**
     * @param ResponseMessage $message
     * @return $this
     */
    public function addResponse(ResponseMessage $message):self
    {
        $this->response_messages[] = $message;

        //todo объединить с копипастой из checkLimitExternalIds
        $count = count($this->response_messages);
        if ($count > $this->getSettings()->getMaxCountOfMessageInDialogForAnalysis()) {
            for ($i = 0; $i <= ($count - $this->getSettings()->getMaxCountOfMessageInDialogForAnalysis() - 1); $i++) {
                unset($this->response_messages[$i]);
            }
            $this->response_messages = array_values($this->response_messages);
        }
        return $this;
    }

    /**
     * @param Solution $solution
     * @return $this
     */
    public function addSolution(Solution $solution):self
    {
        $this->solutions[] = $solution;

        //todo объединить с копипастой из checkLimitExternalIds
        $count = count($this->solutions);
        if ($count > $this->getSettings()->getMaxCountOfMessageInDialogForAnalysis()) {
            for ($i = 0; $i <= ($count - $this->getSettings()->getMaxCountOfMessageInDialogForAnalysis() - 1); $i++) {
                unset($this->solutions[$i]);
            }
            $this->solutions = array_values($this->solutions);
        }
        return $this;
    }

    /**
     * @return array
     */
    public function getSolutions(): array
    {
        return $this->solutions;
    }

    /**
     * @param array $solutions
     * @return History
     */
    public function setSolutions(array $solutions): History
    {
        $this->solutions = $solutions;
        return $this;
    }




    /**
     * Получть n последних своих сообщений
     * @param int|null $count
     * @return array
     */
    public function getYourLast(?int $count = null):array
    {
        if (empty($count)) {
            return $this->solutions;
        }
        return array_slice($this->solutions, 0, $count);
    }

    /**
     * Получть n последних своих сообщений адрессованных заданному собеседнику
     * @param string $external_id
     * @param int|null $count
     * @return array
     */
    public function getYourLastByUserExternalId(string $external_id, ?int $count = null):array
    {
        $result = [];
        /** @var Solution $solution */
        foreach ($this->solutions as $solution) {
            if ($solution->getMessage()->getInterlocutor()->getExternalId() == $external_id) {
                $result[]  = $solution;
            }
            if (count($result) >= $result) {
                break;
            }
        }
        return $result;
    }

}