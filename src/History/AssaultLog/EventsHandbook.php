<?php


namespace App\History\AssaultLog;

/**
 * Class EventsHandbook
 * @package App\History\AssaultLog
 */
class EventsHandbook
{
    const TYPES = [
        101 => 'SCAN_BATTLE_FIELD',
        102 => 'CHOOSING_ANSWER',
        103 => 'SCAN_USER',
        104 => 'POST_MESSAGE',
        105 => 'GET_MESSAGE',
        106 => 'TYPESET',
        107 => 'MARK_AS_READ',
    ];
}