<?php


namespace App\History\AssaultLog\EventsContext;

use App\Core\Message;
use App\Solvers\Solution;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Type;

/**
 * Class ChoosingAnswerContext
 * @package App\History\AssaultLog\EventsContext
 */
class ChoosingAnswerContext extends BaseEventContext
{
    #[Serializer\Type("string")]
    public ?string $response_text = null;

    #[Serializer\Type("string")]
    public ?string $interlocutor_name = null;

    #[Serializer\Type("string")]
    public ?string $interlocutor_text = null;

    #[Serializer\Type("string")]
    public ?string $solver_module_name = null;

    #[Serializer\Type("array")]
    public ?array $attachments = [];

    /**
     * ChoosingAnswerContext constructor.
     * @param Solution|null $result
     * @param Message|null $input
     */
    public function __construct(?Solution $result, ?Message $input)
    {
        $this->solver_module_name = $result->getModuleShortName();
        $this->interlocutor_name = $input->getAuthor()->getFullName();
        $this->interlocutor_text = $input->getText();
        $this->response_text = $result->getMessage()->getText();
    }

}