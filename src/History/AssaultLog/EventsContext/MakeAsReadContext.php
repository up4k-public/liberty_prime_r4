<?php


namespace App\History\AssaultLog\EventsContext;

use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Type;

/**
 * Class MakeAsReadContext
 * @package App\History\AssaultLog\EventsContext
 */
class MakeAsReadContext extends BaseEventContext
{
    #[Serializer\Type("integer")]
    public int $count = 0;

    /**
     * MakeAsReadContext constructor.
     * @param null $result
     * @param array|null $input
     */
    public function __construct($result = null, ?array $input = null)
    {
        if (!is_null($input)) {
            $this->count = count($input);
        }
    }
}