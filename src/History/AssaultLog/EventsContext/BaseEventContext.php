<?php

namespace App\History\AssaultLog\EventsContext;

use App\Dto\Api\ApiDto;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Type;

/**
 * Class BaseEventContext
 * @package App\History\AssaultLog\EventsContext
 */
abstract class BaseEventContext extends ApiDto
{
}