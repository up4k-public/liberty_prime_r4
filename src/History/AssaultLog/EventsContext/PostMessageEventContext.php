<?php


namespace App\History\AssaultLog\EventsContext;

use App\Core\ResponseMessage;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Type;

/**
 * Class PostMessageEventContext
 * @package App\History\AssaultLog\EventsContext
 */
class PostMessageEventContext extends BaseEventContext
{
    #[Serializer\Type("string")]
    public ?string $text = null;

    #[Serializer\Type("string")]
    public ?string $interlocutor_name = null;

    #[Serializer\Type("array")]
    public ?array $attachments = [];

    #[Serializer\Type("integer")]
    public ?int $message_id;

    /**
     * PostMessageEventContext constructor.
     * @param int|null $result
     * @param ResponseMessage $input
     */
    public function __construct(?int $result, ResponseMessage $input)
    {
        $this->text = $input->getText();
        $this->interlocutor_name = $input->getInterlocutor()->getFullName();
        $this->attachments = $input->getAttachment();
        $this->message_id = $result;
    }

    /**
     * @return string|null
     */
    public function getText(): ?string
    {

        return $this->text;
    }

    /**
     * @param string|null $text
     * @return PostMessageEventContext
     */
    public function setText(?string $text): PostMessageEventContext
    {
        $this->text = $text;
        return $this;
    }

    /**
     * @return array
     */
    public function getAttachments(): array
    {
        return $this->attachments;
    }

    /**
     * @param array $attachments
     * @return PostMessageEventContext
     */
    public function setAttachments(array $attachments): PostMessageEventContext
    {
        $this->attachments = $attachments;
        return $this;
    }

}