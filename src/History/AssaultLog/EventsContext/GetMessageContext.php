<?php


namespace App\History\AssaultLog\EventsContext;

use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Type;

/**
 * Class GetMessageContext
 * @package App\History\AssaultLog\EventsContext
 */
class GetMessageContext extends BaseEventContext
{
    #[Serializer\Type("string")]
    public ?string $target_id;

    #[Serializer\Type("integer")]
    public int $count = 0;

    /**
     * GetMessageContext constructor.
     * @param array|null $result
     * @param string|null $input
     */
    public function __construct(?array $result, ?string $input)
    {
        $this->target_id = $input;
        if (!is_null($result)) {
            $this->count = count($result);
        }
    }
}