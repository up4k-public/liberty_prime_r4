<?php


namespace App\History\AssaultLog\EventsContext;

use App\Core\Author;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Type;

/**
 * Class ScanUserContext
 * @package App\History\AssaultLog\EventsContext
 */
class ScanUserContext extends BaseEventContext
{
    #[Serializer\Type("string")]
    public ?string $external_id;

    #[Serializer\Type("string")]
    public string $full_name;

    #[Serializer\Type("boolean")]
    public ?bool $sex;


    /**
     * ScanUserContext constructor.
     * @param Author $result
     * @param string|null $input
     */
    public function __construct(Author $result, ?string $input)
    {
        $this->external_id = $input;
        $this->full_name = $result->getFullName();
        $this->sex = $result->getSex();
    }
}