<?php


namespace App\History\AssaultLog\EventsContext;

use App\Core\BattleField;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Type;

/**
 * Class ScanBattlefieldContext
 * @package App\History\AssaultLog\EventsContext
 */
class ScanBattlefieldContext extends BaseEventContext
{
    #[Serializer\Type("string")]
    public ?string $name;

    #[Serializer\Type("string")]
    public ?string $target;

    /**
     * ScanBattlefieldContext constructor.
     * @param $result
     * @param $input
     */
    public function __construct(BattleField $result, string $input)
    {
        $this->name = $result->getName();
        $this->target = $input;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     * @return ScanBattlefieldContext
     */
    public function setName(?string $name): ScanBattlefieldContext
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTarget(): ?string
    {
        return $this->target;
    }

    /**
     * @param string|null $target
     * @return ScanBattlefieldContext
     */
    public function setTarget(?string $target): ScanBattlefieldContext
    {
        $this->target = $target;
        return $this;
    }

}