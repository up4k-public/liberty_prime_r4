<?php


namespace App\History\AssaultLog\EventsContext;

use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Type;

/**
 * Class TypesetContext
 * @package App\History\AssaultLog\EventsContext
 */
class TypesetContext extends BaseEventContext
{
    #[Serializer\Type("integer")]
    public int $duration = 0;

    #[Serializer\Type("string")]
    public ?string $target_id = null;

    /**
     * TypesetContext constructor.
     * @param null $result
     * @param int|null $input
     * @param string|null $target_id
     */
    public function __construct($result = null, ?int $input = null, ?string $target_id = null)
    {
        $this->target_id = $target_id;
        if (!is_null($input)) {
            $this->duration = $input;
        }
    }
}