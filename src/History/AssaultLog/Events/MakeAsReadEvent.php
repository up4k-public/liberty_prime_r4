<?php


namespace App\History\AssaultLog\Events;

use App\History\AssaultLog\EventsContext\BaseEventContext;
use App\Entity\Assault;
use App\History\AssaultLog\EventsContext\GetMessageContext;
use App\History\AssaultLog\EventsContext\MakeAsReadContext;
use App\History\AssaultLog\EventsHandbook;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Type;

/**
 * Class MakeAsReadEvent
 * @package App\History\AssaultLog\Events
 */
class MakeAsReadEvent extends BaseAssaultLogEvent
{
    const CODE = 107;

    #[Serializer\Type("App\History\AssaultLog\EventsContext\MakeAsReadContext")]
    protected ?BaseEventContext $context = null;

    /**
     * MakeAsReadEvent constructor.
     * @param Assault $assault
     * @param MakeAsReadContext|null $context
     */
    public function __construct(Assault $assault, ?MakeAsReadContext $context = null)
    {
        parent::__construct($assault, self::CODE, EventsHandbook::TYPES[self::CODE], $context);
    }

    /**
     * @return MakeAsReadContext|null
     */
    function getContext(): ?MakeAsReadContext
    {
        return $this->context;
    }
}