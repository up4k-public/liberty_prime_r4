<?php


namespace App\History\AssaultLog\Events;

use App\History\AssaultLog\EventsContext\ScanBattlefieldContext;
use App\History\AssaultLog\EventsContext\BaseEventContext;
use App\Entity\Assault;
use App\History\AssaultLog\EventsHandbook;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Type;

/**
 * Class ScanBattlefield
 * @package App\History\AssaultLog\Events
 */
class ScanBattlefieldEvent extends BaseAssaultLogEvent
{
    const CODE = 101;

    #[Serializer\Type("App\History\AssaultLog\EventsContext\ScanBattlefieldContext")]
    protected ?BaseEventContext $context = null;

    /**
     * ScanBattlefield constructor.
     * @param Assault $assault
     * @param ScanBattlefieldContext|null $context
     */
    public function __construct(Assault $assault, ?ScanBattlefieldContext $context = null)
    {
        parent::__construct($assault, self::CODE, EventsHandbook::TYPES[self::CODE], $context);
    }

    function getContext(): ?ScanBattlefieldContext
    {
        return $this->context;
    }
}