<?php


namespace App\History\AssaultLog\Events;

use App\Entity\Assault;
use App\History\AssaultLog\EventsContext\BaseEventContext;
use App\History\AssaultLog\EventsContext\TypesetContext;
use App\History\AssaultLog\EventsHandbook;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Type;

/**
 * Class TypesetEvent
 * @package App\History\AssaultLog\Events
 */
class TypesetEvent extends BaseAssaultLogEvent
{
    const CODE = 106;

    #[Serializer\Type("App\History\AssaultLog\EventsContext\TypesetContext")]
    protected ?BaseEventContext $context = null;

    /**
     * TypesetEvent constructor.
     * @param Assault $assault
     * @param TypesetContext|null $context
     */
    public function __construct(Assault $assault, ?TypesetContext $context = null)
    {
        parent::__construct($assault, self::CODE, EventsHandbook::TYPES[self::CODE], $context);
    }

    function getContext(): ?TypesetContext
    {
        return $this->context;
    }
}