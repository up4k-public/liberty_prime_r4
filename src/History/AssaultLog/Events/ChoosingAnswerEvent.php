<?php


namespace App\History\AssaultLog\Events;

use App\Entity\Assault;
use App\History\AssaultLog\EventsContext\BaseEventContext;
use App\History\AssaultLog\EventsContext\ChoosingAnswerContext;
use App\History\AssaultLog\EventsHandbook;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Type;

/**
 * Class ChoosingAnswerEvent
 * @package App\History\AssaultLog\Events
 */
class ChoosingAnswerEvent extends BaseAssaultLogEvent
{
    const CODE = 102;

    #[Serializer\Type("App\History\AssaultLog\EventsContext\ChoosingAnswerContext")]
    protected ?BaseEventContext $context = null;

    /**
     * ChoosingAnswerEvent constructor.
     * @param Assault $assault
     * @param ChoosingAnswerContext|null $context
     */
    public function __construct(Assault $assault, ?ChoosingAnswerContext $context = null)
    {
        parent::__construct($assault, self::CODE, EventsHandbook::TYPES[self::CODE], $context);
    }

    function getContext(): ?ChoosingAnswerContext
    {
        return $this->context;
    }
}