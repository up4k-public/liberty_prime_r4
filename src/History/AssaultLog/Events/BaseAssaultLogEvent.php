<?php


namespace App\History\AssaultLog\Events;


use App\Entity\Assault;
use App\History\AssaultLog\EventsContext\BaseEventContext;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Type;

/**
 * Class AssaultLogEvent
 * @package App\History\AssaultLog
 */
abstract class BaseAssaultLogEvent
{
    #[Serializer\Type("string")]
    private string $code;

    #[Serializer\Type("string")]
    private string $message;

    #[Serializer\Type("integer")]
    private int $assault_id;

    #[Serializer\Type("string")]
    private string $ts_created;

    protected ?BaseEventContext $context = null;

    /**
     * AssaultLogEvent constructor.
     * @param Assault $assault
     * @param string $code
     * @param string $message
     * @param null $context
     */
    public function __construct(Assault $assault, string $code, string $message, $context = null)
    {
        $this->assault_id = $assault->getId();
        $this->ts_created = (new \DateTime())->format('Y-m-d H:i:s');
        $this->code = $code;
        $this->message = $message;
        $this->context = $context;
    }

    /**
     * @return BaseEventContext|null
     */
    public function getContext():?BaseEventContext
    {
        return $this->context;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @param string $code
     * @return BaseAssaultLogEvent
     */
    public function setCode(string $code): BaseAssaultLogEvent
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param string $message
     * @return BaseAssaultLogEvent
     */
    public function setMessage(string $message): BaseAssaultLogEvent
    {
        $this->message = $message;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getAssaultId(): ?int
    {
        return $this->assault_id;
    }

    /**
     * @param int $assault_id
     * @return BaseAssaultLogEvent
     */
    public function setAssaultId(int $assault_id): BaseAssaultLogEvent
    {
        $this->assault_id = $assault_id;
        return $this;
    }


    /**
     * @return string
     */
    public function getTsCreated(): string
    {
        return $this->ts_created;
    }

    /**
     * @param string $ts_created
     * @return BaseAssaultLogEvent
     */
    public function setTsCreated(string $ts_created): BaseAssaultLogEvent
    {
        $this->ts_created = $ts_created;
        return $this;
    }
}