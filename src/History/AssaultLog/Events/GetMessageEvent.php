<?php


namespace App\History\AssaultLog\Events;

use App\History\AssaultLog\EventsContext\BaseEventContext;
use App\Entity\Assault;
use App\History\AssaultLog\EventsContext\GetMessageContext;
use App\History\AssaultLog\EventsHandbook;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Type;

/**
 * Class GetMessageEvent
 * @package App\History\AssaultLog\Events
 */
class GetMessageEvent extends BaseAssaultLogEvent
{
    const CODE = 105;

    #[Serializer\Type("App\History\AssaultLog\EventsContext\GetMessageContext")]
    protected ?BaseEventContext $context = null;

    /**
     * GetMessageEvent constructor.
     * @param Assault $assault
     * @param GetMessageContext|null $context
     */
    public function __construct(Assault $assault, ?GetMessageContext $context = null)
    {
        parent::__construct($assault, self::CODE, EventsHandbook::TYPES[self::CODE], $context);
    }

    /**
     * @return GetMessageContext|null
     */
    function getContext(): ?GetMessageContext
    {
        return $this->context;
    }
}