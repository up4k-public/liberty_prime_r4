<?php


namespace App\History\AssaultLog\Events;

use App\History\AssaultLog\EventsContext\BaseEventContext;
use App\Entity\Assault;
use App\History\AssaultLog\EventsContext\PostMessageEventContext;
use App\History\AssaultLog\EventsHandbook;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Type;

/**
 * Class PostMessageEvent
 * @package App\History\AssaultLog\Events
 */
class PostMessageEvent extends BaseAssaultLogEvent
{
    const CODE = 104;

    #[Serializer\Type("App\History\AssaultLog\EventsContext\PostMessageEventContext")]
    protected ?BaseEventContext $context = null;

    /**
     * PostMessageEvent constructor.
     * @param Assault $assault
     * @param PostMessageEventContext|null $context
     */
    public function __construct(Assault $assault, ?PostMessageEventContext $context = null)
    {
        parent::__construct($assault, self::CODE, EventsHandbook::TYPES[self::CODE], $context);
    }

    /**
     * @return PostMessageEventContext|null
     */
    function getContext(): ?PostMessageEventContext
    {
        return $this->context;
    }

}