<?php


namespace App\History\AssaultLog\Events;

use App\Entity\Assault;
use App\History\AssaultLog\EventsContext\BaseEventContext;
use App\History\AssaultLog\EventsContext\ScanUserContext;
use App\History\AssaultLog\EventsHandbook;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Type;

/**
 * Class ScanUserEvent
 * @package App\History\AssaultLog\Events
 */
class ScanUserEvent extends BaseAssaultLogEvent
{
    const CODE = 103;

    #[Serializer\Type("App\History\AssaultLog\EventsContext\ScanUserContext")]
    protected ?BaseEventContext $context = null;

    /**
     * ScanUserEvent constructor.
     * @param Assault $assault
     * @param ScanUserContext|null $context
     */
    public function __construct(Assault $assault, ?ScanUserContext $context = null)
    {
        parent::__construct($assault, self::CODE, EventsHandbook::TYPES[self::CODE], $context);
    }

    function getContext(): ?ScanUserContext
    {
        return $this->context;
    }
}