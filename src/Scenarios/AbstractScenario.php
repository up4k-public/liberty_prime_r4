<?php


namespace App\Scenarios;


use App\Core\Author;
use App\Core\BattleField;
use App\Core\Interactors\AbstractBattlefieldInfoCollector;
use App\Core\Interactors\AbstractLiker;
use App\Core\Interactors\AbstractMessageRecipient;
use App\Core\Interactors\AbstractMessageSender;
use App\Core\Interactors\AbstractReadMarker;
use App\Core\Interactors\AbstractTypesetter;
use App\Core\Interactors\AbstractUserinfoCollector;
use App\Entity\Assault;
use App\Exceptions\Core\BaseCoreException;
use App\Factories\AdapterFactory;
use App\Helpers\TextConverterHelper;
use App\History\History;
use App\Integrations\Api;
use App\Factories\IntegrationsFactory;
use App\Factories\InteractorsFactory;
use App\Service\AssaultService;
use App\Service\BattleFieldService;
use App\Service\MessageService;
use App\Settings\Settings;
use App\Solvers\Overmind;
use Exception;

/**
 * Class AbstractScenario
 * @package App\Scenarios
 */
abstract class AbstractScenario
{
    protected ?Assault $assault;
    protected ?string $target_id;
    protected ?Settings $settings;

    protected int $cycle_delay = 0;
    protected BattleField $battleField;
    protected Author $selAvatar;
    protected History $history;
    protected array $messages_to_be_processed;

    protected ?Api $api;
    protected ?AbstractBattlefieldInfoCollector $battlefieldInfoCollector;
    protected ?AbstractLiker $liker;
    protected ?AbstractMessageRecipient $messageRecipient;
    protected ?AbstractMessageSender $messageSender;
    protected ?AbstractReadMarker $readMarker;
    protected ?AbstractTypesetter $typesetter;
    protected ?AbstractUserinfoCollector $userinfoCollector;

    const INTERACTORS = [
        'BattlefieldInfoCollector',
        'Liker',
        'MessageRecipient',
        'MessageSender',
        'ReadMarker',
        'Typesetter',
        'UserinfoCollector'
    ];


    /**
     * Scenario constructor.
     * @param IntegrationsFactory $integrationsFactory
     * @param InteractorsFactory $interactorsFactory
     * @param AdapterFactory $adapterFactory
     * @param AssaultService $assaultService
     * @param BattleFieldService $battleFieldService
     * @param MessageService $messageService
     * @param Overmind $overmind
     */
    public function __construct(
        private IntegrationsFactory $integrationsFactory,
        private InteractorsFactory $interactorsFactory,
        private AdapterFactory $adapterFactory,
        protected AssaultService $assaultService,
        protected BattleFieldService $battleFieldService,
        protected MessageService $messageService,
        protected Overmind $overmind)
    {
    }

    /**
     * @return string|null
     */
    public function getTargetId(): ?string
    {
        return $this->target_id;
    }

    /**
     * @return Settings
     */
    public function getSettings(): Settings
    {
        return $this->settings;
    }

    /**
     * Довольно костыльно
     * @param Assault $assault
     * @throws BaseCoreException
     */
    private function createInteractors(Assault $assault)
    {
        foreach (self::INTERACTORS as $name) {
            $property = TextConverterHelper::firstLetterToLowerCase($name);
            $this->$property = $this->interactorsFactory->create($this->api, $name,
                $assault->getGlobalType(), $assault->getLocalType(),
                $this->adapterFactory->create($name, $assault->getGlobalType(), $assault->getLocalType()));

            if (method_exists($this->$property, 'setTargetId')) {
                $this->$property->setTargetId($assault->getTargetId());
            }
            if (method_exists($this->$property, 'setAssault')) {
                $this->$property->setAssault($assault);
            }
        }
    }

    /**
     * @param Assault $assault
     * @return $this
     * @throws Exception
     */
    public function setAssault(Assault $assault): self
    {
        $this->assault = $assault;

        $this->target_id = $assault->getTargetId();
        $this->settings = $this->assaultService->getSettings($assault);

        $this->history = (new History())->setSettings($this->settings->getHistorySettings());
        $this->overmind
            ->setHistory($this->history)
            ->setSettings($this->getSettings());
        $this->api = $this->integrationsFactory->create($assault->getGlobalType(), $assault->getCartrige()->getAuthData());
        $this->createInteractors($assault);
        return $this;
    }

    abstract public function run();

    /**
     * @param array $messages
     */
    public function addMessageToProcessAndSaveHistory(array $messages)
    {
        if (empty($messages)) {
            return;
        }

        $this->messages_to_be_processed = $messages;

        $this->history->addExternalIds($this->messageService->getExternalIds($this->messages_to_be_processed));
        if (empty($this->history->getFirstExternalId())) {
            $this->history->setFirstExternalId($messages[0]->getExternalId());
        }
    }

    /**
     * Рассчитываем паузу между циклами, чем дольше молчат, тем больше будет пауза перед реакцией бота
     * @param bool $empty_new_message
     */
    protected function calculateCycleDelay(bool $empty_new_message)
    {
        if ($empty_new_message && $this->getSettings()->getDelaySettings()->isCumulativeEffectOfDelayInWaitingTheNextCycle()) {
            $this->cycle_delay += $this->getSettings()->getDelaySettings()->getDelayInWaitingTheNextCycle();

            if ($this->cycle_delay > $this->getSettings()->getDelaySettings()->getMaxDelayInWaitingTheNextCycle()) {
                $this->cycle_delay = $this->getSettings()->getDelaySettings()->getMaxDelayInWaitingTheNextCycle();
            }
        } else {
            $this->cycle_delay = $this->getSettings()->getDelaySettings()->getDelayInWaitingTheNextCycle();
        }
    }

    /**
     * Общий этап для разных сценариев, сбор первичной информации о месте общения и о заряженном аккаунте
     */
    protected function preStartPreparation()
    {
        $this->selAvatar = $this->userinfoCollector->runAndSaveLog();
        $this->battleField = $this->battlefieldInfoCollector->runAndSaveLog($this->getTargetId());
        $this->battleFieldService->setIamAdmin($this->selAvatar,
            $this->battleField);

        //А так же костыльное прокидование параметров и настроек по цепочке. TODO придумать что-то с этим, пока тестим так
        $this->overmind
            ->setAssault($this->assault)
            ->setSelfAvatar($this->selAvatar)
            ->setBattleField($this->battleField);
    }

}