<?php


namespace App\Scenarios;

use App\Core\Message;

/**
 * Стандартный, исторически сложившийся сценарий, где бот ведёт очень активный навязчивый диалог
 * Class DefaultScenario
 * @package App\Scenarios
 */
class DefaultScenario extends AbstractScenario
{

    public function run()
    {
        $this->preStartPreparation();

        while ($this->assaultService->checkAssaultInProcess($this->assault)) {
            $this->addMessageToProcessAndSaveHistory(
                $this->messageService->getNewBatchWithPreProcessing(
                $this->messageRecipient->runAndSaveLog(),
                $this->settings,
                $this->selAvatar));
            $this->calculateCycleDelay(empty($this->messages_to_be_processed));

            /** @var Message $message */
            while (!empty($this->messages_to_be_processed) && $this->assaultService->checkAssaultInProcess($this->assault)) {

                $this->readMarker->runAndSaveLog();
                $message = array_shift($this->messages_to_be_processed);
                $response_message = $this->overmind->getResponse($message);
                $this->history->addIds($response_message?->getPhrase()?->getId());

                $this->typesetter->runAndSaveLog(
                    $this->messageService->calculateTypingTime($response_message, $this->getSettings()->getDelaySettings()));

                if (!$response_message?->getIgnore()) {
                    $this->messageSender->runAndSaveLog($response_message);
                    sleep($this->settings->getDelaySettings()->getLagBetweenPost());
                }

                if ($this->getSettings()->getBehaviorSettings()->isRequestMessagesBetweenPosts()) {
                    $this->addMessageToProcessAndSaveHistory(
                        $this->messageService->requeryNewBatchWithPreProcessing(
                            $this->messageRecipient->runAndSaveLog(),
                            $this->settings,
                            $this->selAvatar,
                            $this->history));

                }
            }
            sleep($this->cycle_delay);
        }
    }

}