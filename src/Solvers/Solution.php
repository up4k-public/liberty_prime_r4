<?php


namespace App\Solvers;

use App\Core\ResponseMessage;
use App\Estimations\Estimations;
use App\Factories\UnitsSettingsFactory;

/**
 * Class Solution
 * @package App\Solvers
 */
class Solution
{
   private ?ResponseMessage $message = null;
   private Estimations $estimations;
   private ?string $module_name;

    /**
     * @return ResponseMessage|null
     */
    public function getMessage(): ?ResponseMessage
    {
        return $this->message;
    }

    /**
     * @param ResponseMessage|null $message
     * @return Solution
     */
    public function setMessage(?ResponseMessage $message): Solution
    {
        $this->message = $message;
        return $this;
    }

    /**
     * @return Estimations
     */
    public function getEstimations(): Estimations
    {
        return $this->estimations;
    }

    /**
     * @param Estimations $estimations
     * @return Solution
     */
    public function setEstimations(Estimations $estimations): Solution
    {
        $this->estimations = $estimations;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getModuleName(): ?string
    {
        return $this->module_name;
    }

    /**
     * @return string|null
     */
    public function getModuleShortName():?string
    {
        return str_replace(UnitsSettingsFactory::NAMESPACE_UNIT, '', $this->getModuleName());
    }

    /**
     * @param string|null $module_name
     * @return Solution
     */
    public function setModuleName(?string $module_name): Solution
    {
        $this->module_name = $module_name;
        return $this;
    }

}