<?php


namespace App\Solvers\UnitsSettings;

use App\Settings\AbstractSolversModuleSettings;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Type;

/**
 * Class OccurrenceSubstringSettings
 * @package App\Solvers\UnitsSettings
 */
class OccurrenceSubstringSettings extends AbstractSolversModuleSettings
{
    /**
     * @Serializer\Type("integer")
     */
    public int $base_quality_of_choice = 30;

    /**
     * @Serializer\Type("integer")
     */
    public int $number_of_instances = 5;

    /**
     * @Serializer\Type("integer")
     */
    public int $min_length_good_trigger = 3;

    /**
     * @Serializer\Type("integer")
     */
    public int $downgrade_with_bad_trigger = 3;

    /**
     * @return int
     */
    public function getMinLengthGoodTrigger(): int
    {
        return $this->min_length_good_trigger;
    }

    /**
     * @param int $min_length_good_trigger
     * @return OccurrenceSubstringSettings
     */
    public function setMinLengthGoodTrigger(int $min_length_good_trigger): OccurrenceSubstringSettings
    {
        $this->min_length_good_trigger = $min_length_good_trigger;
        return $this;
    }

    /**
     * @return int
     */
    public function getDowngradeWithBadTrigger(): int
    {
        return $this->downgrade_with_bad_trigger;
    }

    /**
     * @param int $downgrade_with_bad_trigger
     * @return OccurrenceSubstringSettings
     */
    public function setDowngradeWithBadTrigger(int $downgrade_with_bad_trigger): OccurrenceSubstringSettings
    {
        $this->downgrade_with_bad_trigger = $downgrade_with_bad_trigger;
        return $this;
    }


}