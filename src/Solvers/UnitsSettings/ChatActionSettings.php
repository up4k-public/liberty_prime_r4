<?php


namespace App\Solvers\UnitsSettings;

use App\Settings\AbstractSolversModuleSettings;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Type;

/**
 * Class ChatActionSettings
 * @package App\Solvers\UnitsSettings
 */
class ChatActionSettings  extends AbstractSolversModuleSettings
{
    /**
     * @Serializer\Type("integer")
     */
    public int $base_quality_of_choice = 45;

    /**
     * @Serializer\Type("integer")
     */
    public int $reuse_rate_by_quality_of_choice_in_dialog = 10;
}