<?php


namespace App\Solvers;

use App\Core\Author;
use App\Exceptions\Core\BaseCoreException;
use App\Exceptions\Core\SolverUnitsNotFound;
use App\History\History;
use App\Repository\PhraseRepository;
use App\Settings\Settings;
use Doctrine\ORM\EntityManagerInterface;
use Exception;

/**
 * Class UnitsFactory
 * @package App\Solvers
 */
class UnitsFactory
{
    const PATH = 'src/Solvers/Units';
    const NAMESPACE = "App\Solvers\Units\\";

    private ?History $history = null;
    private ?Author $self_avatar = null;

    /**
     * AbstractSolver constructor.
     * @param EntityManagerInterface $em
     * @param PhraseRepository $phraseRepository
     */
    public function __construct(
        private EntityManagerInterface $em,
        private PhraseRepository $phraseRepository
    )
    {
    }

    /**
     * @param History|null $history
     * @return UnitsFactory
     */
    public function setHistory(?History $history): UnitsFactory
    {
        $this->history = $history;
        return $this;
    }

    /**
     * @param Author|null $self_avatar
     * @return UnitsFactory
     */
    public function setSelfAvatar(?Author $self_avatar): UnitsFactory
    {
        $this->self_avatar = $self_avatar;
        return $this;
    }

    /**
     * @param string $name
     * @return AbstractSolver|null
     * @throws BaseCoreException
     */
    public function makeUnit(string $name): ?AbstractSolver
    {
        $name = self::NAMESPACE . $name;
        $class = new $name($this->em, $this->phraseRepository);
        if (!($class instanceof AbstractSolver)) {
            throw new BaseCoreException($name . " must be extend " . AbstractSolver::class);
        }
        $class->setHistory($this->history)->setSelfAvatar($this->self_avatar);
        return $class;
    }

    /**
     * @param Settings $settings
     * @return array
     * @throws Exception
     */
    public function makeAllUnits(Settings $settings): array
    {
        $names = $this->getAllNames();
        $result = [];

        foreach ($names as $name) {
            $class = $this->makeUnit($name);
            $class->setModuleSettings($settings->getModuleSettingsByName($class::class));
            if ($class->getModuleSettings()->isUseModule()) {
                for ($i = 0; $i < $class->getModuleSettings()->getNumberOfInstances(); $i++) {
                    $result[] = clone $class;
                }
                unset($class);
            }
        }
        return $result;
    }

    /**
     * @param bool $flag_run_by_api
     * @return array
     * @throws SolverUnitsNotFound
     */
    public function getAllNames($flag_run_by_api = false): array
    {
        $names = array_diff(scandir($this->getPath($flag_run_by_api)), ['..', '.']);

        foreach ($names as &$name) {
            $name = str_replace('.php', '', $name);
        }
        unset($name);

        if (empty($names)) {
            throw new SolverUnitsNotFound();
        }
        return array_values($names);
    }

    /**
     * @param false $flag_run_by_api
     * @return string
     */
    private function getPath($flag_run_by_api = false): string
    {
        if ($flag_run_by_api) {
            return '../' . self::PATH;
        }
        return self::PATH;
    }


}