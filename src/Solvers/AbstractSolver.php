<?php


namespace App\Solvers;


use App\Core\Author;
use App\Core\Message;
use App\Core\ResponseMessage;
use App\Estimations\Estimations;
use App\History\History;
use App\Repository\PhraseRepository;
use App\Settings\AbstractSolversModuleSettings;
use App\Settings\Settings;
use Exception;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class AbstractSolver
 * @package App\Solvers
 */
abstract class AbstractSolver
{
    protected ?History $history;
    protected ?Settings $settings;
    protected ?AbstractSolversModuleSettings $module_settings;
    protected ?Author $self_avatar;

    /**
     * AbstractSolver constructor.
     * @param EntityManagerInterface $em
     * @param PhraseRepository $phraseRepository
     */
    public function __construct(
        protected EntityManagerInterface $em,
        protected PhraseRepository $phraseRepository
    )
    {
        $ids = [63551, 63552, 63553, 63554, 63555];
    }

    /**
     * @return string
     */
    protected function getModuleName():string
    {
        return str_replace('App\Solvers\Units\\', '', get_class($this));
    }

    /**
     * @param Message $message
     * @return Solution|null
     * @throws Exception
     */
    public function getResponseAndEstimation(Message $message): ?Solution
    {
        $responseMessage = (new ResponseMessage())
            ->setInterlocutor($message->getAuthor())
            ->setRequestMessageExternalIds($message->getExternalIds());

        if ($message->getCanForward() && !($message->getIsService())) {
            $responseMessage->setForwardMessageExternalIds($message->getExternalIds());
        }

        $responseMessage = $this->fullnessResponseMessage($message, $responseMessage);

        return (new Solution())
            ->setMessage($responseMessage)
            ->setModuleName(get_class($this))
            ->setEstimations($this->calculateEstimation($message, $responseMessage));
    }

    /**
     * @param Message $message
     * @param ResponseMessage $responseMessage
     * @return ResponseMessage|null
     */
    abstract protected function fullnessResponseMessage(Message $message, ResponseMessage $responseMessage):?ResponseMessage;

    /**
     * Чтобы не забыть новуму класс-решале создать класс с его настройками
     * @return string
     */
    abstract public function getSettingsClassName(): string;

    /**
     * @param Message $message
     * @param ResponseMessage|null $responseMessage
     * @return Estimations
     * @throws Exception
     */
    protected function calculateEstimation(Message $message, ?ResponseMessage $responseMessage = null): Estimations
    {
        if (empty($responseMessage)) {
            return (new Estimations())
                ->setQualityOfChoice(0);
        }

        return (new Estimations())
            ->setQualityOfChoice($this->calculateBaseEstimationValue($message));
    }


    /**
     * @return History
     */
    public function getHistory(): History
    {
        return $this->history;
    }

    /**
     * @param History|null $history
     * @return AbstractSolver
     */
    public function setHistory(?History $history): AbstractSolver
    {
        $this->history = $history;
        return $this;
    }

    /**
     * @return AbstractSolversModuleSettings|null
     */
    public function getModuleSettings(): ?AbstractSolversModuleSettings
    {
        return $this->module_settings;
    }

    /**
     * @param AbstractSolversModuleSettings|null $module_settings
     * @return AbstractSolver
     */
    public function setModuleSettings(?AbstractSolversModuleSettings $module_settings): AbstractSolver
    {
        $this->module_settings = $module_settings;
        return $this;
    }

    /**
     * @return Settings|null
     */
    public function getSettings(): ?Settings
    {
        return $this->settings;
    }

    /**
     * @return EntityManagerInterface
     */
    public function getEm(): EntityManagerInterface
    {
        return $this->em;
    }

    /**
     * @return Author
     */
    public function getSelfAvatar(): Author
    {
        return $this->self_avatar;
    }

    /**
     * @param Author|null $self_avatar
     * @return AbstractSolver
     */
    public function setSelfAvatar(?Author $self_avatar): AbstractSolver
    {
        $this->self_avatar = $self_avatar;
        return $this;
    }


    /**
     * Рассчёт базового значения QualityOfChoice качества выбора с опорой на общие для всех "решал" параметры,
     * а именно, частота срабатывания конкретного решалы в недавней истории(как диалога в целом, так и этого собеседника в часности)
     * @param Message $message
     * @return int
     */
    protected function calculateBaseEstimationValue(Message $message): int
    {
        $history_all_dialog = $this->getHistory()->getYourLast($this->getModuleSettings()->getHistoryDepthOfAssessment());
        $history_user = $this->getHistory()->getYourLastByUserExternalId($message->getAuthor()->getExternalId(), $this->getModuleSettings()->getHistoryDepthOfAssessmentWithInterlocutor());


        $value = $this->getModuleSettings()->getBaseQualityOfChoice();
        /** @var Solution $solution */
        foreach ($history_all_dialog as $key => $solution) {
            if (get_class($this) == $solution->getModuleName()) {
                $value -= ($this->getModuleSettings()->getReuseRateByQualityOfChoiceInDialog());
            } else {
                $value += ($this->getModuleSettings()->getReuseRateByQualityOfChoiceInDialog());
            }
        }

        return $this->balanceEstimationLimitValue($value);
    }

    /**
     * Не больше базового, не меньше минимального допустимого
     * @param int $value
     * @return int
     */
    protected function balanceEstimationLimitValue(int $value): int
    {
        if ($value > $this->getModuleSettings()->getBaseQualityOfChoice()) {
            $value = $this->getModuleSettings()->getBaseQualityOfChoice();
        }

        if ($value < $this->getModuleSettings()->getMinQualityOfChoiceAfterCheckHistory()) {
            $value = $this->getModuleSettings()->getMinQualityOfChoiceAfterCheckHistory();
        }

        return $value;
    }


}