<?php


namespace App\Solvers\Units;


use App\Core\Message;
use App\Core\ResponseMessage;
use App\Solvers\AbstractSolver;
use App\Solvers\UnitsSettings\DetectedBotSettings;

/**
 * Реакций на сообщение от аккаунта, в явном виде помеченного системаой как бота (автоответчики, боты-админы и тд...)
 * Class DetectedBot
 * @package App\Solvers\Units
 */
class DetectedBot extends AbstractSolver
{
    public function getSettingsClassName(): string
    {
        return DetectedBotSettings::class;
    }

    /**
     * @param Message $message
     * @param ResponseMessage $responseMessage
     * @return ResponseMessage|null
     */
    protected function fullnessResponseMessage(Message $message, ResponseMessage $responseMessage): ?ResponseMessage
    {
        if ($message->getAuthor()->getIsBot()) {
            $phrases = $this->phraseRepository->getByModulesAndTagsBesidesId($this->getModuleName(),[], $this->getHistory()->getIds());
            if (!empty($phrases)) {
                $phrase = $phrases[rand(0, count($phrases) - 1)];

                $responseMessage
                    ->setText($phrase->getText())
                    ->setPhrase($phrase);
            }
        }
        return null;
    }

}