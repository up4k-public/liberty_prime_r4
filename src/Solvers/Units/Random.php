<?php


namespace App\Solvers\Units;

use App\Core\Message;
use App\Core\ResponseMessage;
use App\Entity\Phrase;
use App\Solvers\AbstractSolver;
use App\Solvers\UnitsSettings\RandomSettings;

/**
 * Самый последний рубеж. Когда совсем не знаем что сказать, берём рандомную фразу без списка фраз-без-тригера
 * Class Random
 * @package App\Solvers
 */
class Random extends AbstractSolver
{
    public function getSettingsClassName(): string
    {
        return RandomSettings::class;
    }

    /**
     * @param Message $message
     * @param ResponseMessage $responseMessage
     * @return ResponseMessage|null
     */
    protected function fullnessResponseMessage(Message $message, ResponseMessage $responseMessage): ?ResponseMessage
    {
        $phrases = $this->phraseRepository->getAllWithoutTriggersBesidesId($this->getHistory()->getIds());
        if (!empty($phrases)) {
            /** @var Phrase $phrase */
            $phrase = $phrases[rand(0, count($phrases) - 1)];

            $responseMessage
                ->setText($phrase->getText())
                ->setPhrase($phrase);
        }
        return $responseMessage;
    }

}