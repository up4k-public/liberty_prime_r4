<?php


namespace App\Solvers\Units;


use App\Core\DialogAction\BaseDialogAction;
use App\Core\Message;
use App\Core\ResponseMessage;
use App\Solvers\AbstractSolver;
use App\Solvers\UnitsSettings\ChatActionSettings;

/**
 * Реакцияя на события в чате -- добавление/удаление других учасников, смена названия/аватара, закреплене сообщения, удаление-возврат ботэ
 * Class ChatAction
 * @package App\Solvers\Units
 */
class ChatAction extends AbstractSolver
{
    public function getSettingsClassName(): string
    {
        return ChatActionSettings::class;
    }

    /**
     * @param Message $message
     * @param ResponseMessage $responseMessage
     * @return ResponseMessage|null
     */
    protected function fullnessResponseMessage(Message $message, ResponseMessage $responseMessage): ?ResponseMessage
    {
        if ($message->getIsService() &&
            !empty($message?->getActions()[0]) &&
            !($message->getAuthor()->getIsFriendCurrentAvatar() === true)) {
            $phrases = $this->phraseRepository->getByModulesAndTagsBesidesId($this->getModuleName(), [], $this->getHistory()->getIds());
            shuffle($phrases);

            foreach ($phrases as $phase) {
                if ($this->checkTriggers($this->getTrigger($message->getActions()[0]), $phase->getTriggers())) {
                    $responseMessage
                        ->setText($phase->getText())
                        ->setPhrase($phase);
                    break;
                }
            }
            return $responseMessage;
        }
        return null;
    }

    /**
     * @param BaseDialogAction $class
     * @return string
     */
    private function getTrigger(BaseDialogAction $class): string
    {
        //todo для ряда событий доуточнять триге, на предмет добавили друга, меня и тд
        return $trigger = $class->getTrigger();
    }

    /**
     * @param string|null $message
     * @param array|null $triggers
     * @return bool
     */
    protected function checkTriggers(?string $message, ?array $triggers): bool
    {
        /** @var string $trigger */
        foreach ($triggers as $trigger) {
            if (!(!str_contains(mb_strtolower($message), mb_strtolower($trigger)))) {
                return true;
            }
        }
        return false;
    }
}