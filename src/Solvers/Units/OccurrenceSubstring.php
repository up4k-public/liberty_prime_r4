<?php


namespace App\Solvers\Units;

use App\Core\Message;
use App\Core\ResponseMessage;
use App\Entity\Phrase;
use App\Estimations\Estimations;
use App\Repository\PhraseRepository;
use App\Settings\AbstractSolversModuleSettings;
use App\Solvers\AbstractSolver;
use App\Solvers\Solution;
use App\Solvers\UnitsSettings\OccurrenceSubstringSettings;
use Exception;

/**
 * Классика самых первых версии LibertyPrime, поиск фразы по детекции-вхождению
 * Class OccurrenceSubstring
 * @package App\Solvers
 */
class OccurrenceSubstring extends AbstractSolver
{
    private ?string $trigger = null;

    public function getSettingsClassName(): string
    {
        return OccurrenceSubstringSettings::class;
    }

    public function getModuleSettings(): ?OccurrenceSubstringSettings
    {
        return parent::getModuleSettings();
    }

    /**
     * @param Message $message
     * @param ResponseMessage $responseMessage
     * @return ResponseMessage|null
     */
    protected function fullnessResponseMessage(Message $message, ResponseMessage $responseMessage): ?ResponseMessage
    {
        $phrases = $this->phraseRepository->getAllWithTriggersBesidesId($this->getHistory()->getIds());
        shuffle($phrases);
        /** @var Phrase $phase */
        foreach ($phrases as $phase) {
            if ($this->checkTriggers($message->getText(), $phase->getTriggers())) {
                $responseMessage
                    ->setText($phase->getText())
                    ->setPhrase($phase);
                break;
            }
        }
        return $responseMessage;
    }

    /**
     * @param Message $message
     * @param ResponseMessage|null $responseMessage
     * @return Estimations
     * @throws Exception
     */
    protected function calculateEstimation(Message $message, ?ResponseMessage $responseMessage = null): Estimations
    {
        $estimation = parent::calculateEstimation($message, $responseMessage);

        $estimation = $this->downgradeWithBadTrigger($estimation);

        if (empty($responseMessage?->getText())) {
            $estimation->setQualityOfChoice(0);
        }
        var_dump(self::class . ': ' . $estimation->getQualityOfChoice());
        return $estimation;
    }

    /**
     * Если тригер слишком короткий, то такой ответ плохой и теряет в качестве
     * @param Estimations $estimation
     * @return Estimations
     * @throws Exception
     */
    private function downgradeWithBadTrigger(Estimations $estimation): Estimations
    {
        //todo убрать try когда будет валидация настроек на api
        try {
            if (!(is_null($this->trigger)) && (strlen($this->trigger) < $this->getModuleSettings()->getMinLengthGoodTrigger())) {
                $estimation->setQualityOfChoice(
                    $this->balanceEstimationLimitValue(
                        (int)round($estimation->getQualityOfChoice() / $this->getModuleSettings()->getDowngradeWithBadTrigger())));
            }
        } catch (\Error $error) {}

        return $estimation;
    }

    /**
     * @param string|null $message
     * @param array|null $triggers
     * @return bool
     */
    protected function checkTriggers(?string $message, ?array $triggers): bool
    {
        /** @var string $trigger */
        foreach ($triggers as $trigger) {
            if (!(!str_contains(mb_strtolower($message), mb_strtolower($trigger)))) {
                $this->trigger = $trigger;
                return true;
            }
        }
        return false;
    }

}