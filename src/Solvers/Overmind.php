<?php


namespace App\Solvers;

use App\Core\Author;
use App\Core\BattleField;
use App\Core\Message;
use App\Core\ResponseMessage;
use App\Entity\Assault;
use App\History\AssaultLog\Events\ChoosingAnswerEvent;
use App\History\AssaultLog\EventsContext\ChoosingAnswerContext;
use App\History\History;
use App\Repository\LogRepository;
use App\Service\PharasesService;
use App\Service\SolutionService;
use App\Settings\Settings;
use Exception;

/**
 * Class Overmind
 * Надмозг-решала, опрашивает модули генерирующие ответ,
 * выбирает наиболее подходящий в соотв. с настройкми,
 * модифицирует конечную фразу в соот. с настройкми
 * @package App\Solvers
 */
class Overmind
{
    private Author $self_avatar;
    private Settings $settings;
    private BattleField $battleField;
    private ?History $history;
    private Assault $assault;

    private array $solvers = [];

    /**
     * Overmind constructor.
     * @param UnitsFactory $unitsFactory
     * @param SolutionService $solutionService
     * @param LogRepository $eventLogger
     * @param PharasesService $pharasesService
     */
    public function __construct(
        private UnitsFactory $unitsFactory,
        private SolutionService $solutionService,
        private LogRepository $eventLogger,
        private PharasesService $pharasesService)
    {
    }

    /**
     * @return BattleField
     */
    public function getBattleField(): BattleField
    {
        return $this->battleField;
    }

    /**
     * @param BattleField $battleField
     * @return Overmind
     */
    public function setBattleField(BattleField $battleField): Overmind
    {
        $this->battleField = $battleField;
        return $this;
    }

    /**
     * @return Assault
     */
    public function getAssault(): Assault
    {
        return $this->assault;
    }

    /**
     * @param Assault $assault
     * @return Overmind
     */
    public function setAssault(Assault $assault): Overmind
    {
        $this->assault = $assault;
        return $this;
    }

    /**
     * @param Settings $settings
     * @return Overmind
     * @throws Exception
     */
    public function setSettings(Settings $settings): Overmind
    {
        $this->settings = $settings;
        $this->solvers = $this->unitsFactory->makeAllUnits($this->settings);
        return $this;
    }

    /**
     * @return History|null
     */
    public function getHistory(): ?History
    {
        return $this->history;
    }

    /**
     * @param History|null $history
     * @return Overmind
     */
    public function setHistory(?History $history): Overmind
    {
        $this->history = $history;
        return $this;
    }

    /**
     * @return Settings
     */
    public function getSettings(): Settings
    {
        return $this->settings;
    }

    /**
     * @return Author
     */
    public function getSelfAvatar(): Author
    {
        return $this->self_avatar;
    }

    /**
     * @param Author $self_avatar
     * @return Overmind
     */
    public function setSelfAvatar(Author $self_avatar): Overmind
    {
        $this->self_avatar = $self_avatar;
        return $this;
    }


    /**
     * @param Message $message
     * @return ResponseMessage|null
     * @throws Exception
     */
    public function getResponse(Message $message): ?ResponseMessage
    {
        $solutions = [];
        /** @var AbstractSolver $solver */
        foreach ($this->solvers as $solver) {
            $solve = $solver
                ->setHistory($this->getHistory())
                ->getResponseAndEstimation($message);
            if (!empty($solve)) {
                $solutions[] = $solve;
            }
        }

        $solution = $this->solutionService->getTheBestOneByQualityOfChoice($solutions);

        $this->getHistory()->addSolution($solution);
        $this->saveLogChoosingAnswer($solution, $message);

        $solution->setMessage($this->pharasesService
            ->setSelfAvatar($this->getSelfAvatar())
            ->setSettings($this->getSettings())
            ->convertSyntax($solution->getMessage()));
        return $solution->getMessage();
    }

    /**
     * @param Solution|null $solution
     * @param Message|null $message
     * @throws Exception
     */
    private function saveLogChoosingAnswer(?Solution $solution, ?Message $message)
    {
        $this->eventLogger->saveByEvent(
            (new ChoosingAnswerEvent($this->getAssault(),
                new ChoosingAnswerContext($solution, $message))), $this->getAssault()
        );
    }

}