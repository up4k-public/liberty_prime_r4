<?php


namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use OpenApi\Attributes as OA;

/**
 * Class UserController
 * @package App\Controller
 */
#[Route(name: 'main_')]
class MainController
{
    #[Route('/', name: 'index', methods: ["GET"])]
    #[OA\Tag(name: 'Main')]
    public function index()
    {
        $authors = explode(',',$_ENV['AUTHORS']);
        foreach ($authors as &$author) {
            $author = trim($author);
        }
        unset($author);

        return new JsonResponse([
            'success' => true,
            'data' => [
                'name' => $_ENV['PROJECT_NAME'],
                'desc' => $_ENV['DESCRIPTION'],
                'authors' =>  $authors,
                'api_version' => $_ENV['API_VERSION']
            ]
        ]);
    }

    #[Route('/options', name: 'options', methods: ["GET", "POST", "OPTIONS"])]
    public function options()
    {

    }

    #[Route('/health', name: 'health', methods: ["GET"])]
    #[OA\Tag(name: 'health')]
    public function health()
    {
        return [];
    }
}