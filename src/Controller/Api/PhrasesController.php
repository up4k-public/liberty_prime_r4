<?php


namespace App\Controller\Api;

use App\Dto\Api\Phrase\Phrase as PhraseDto;
use App\Dto\Api\Phrase\Request\GetOne;
use App\Dto\Api\Phrase\Request\GetPhraseHistory;
use App\Dto\Api\Phrase\Request\GetPhraseList;
use App\Dto\Api\Phrase\Request\UpdatePhrase as PhraseDtoUpdate;
use App\Dto\Api\Phrase\Response\PhraseList;
use App\Exceptions\ApiValidationFieldsException;
use App\Exceptions\Phrase\ImpossibleRevertPhaseAtHistory;
use App\Exceptions\Phrase\NotHaveDiffForUpdate;
use App\Exceptions\Phrase\PhraseHistoryNotFoundException;
use App\Exceptions\Phrase\PhraseIsExistException;
use App\Exceptions\Phrase\PhraseNotFoundException;
use App\Service\PharasesLibrarianService;
use App\Service\ValidationDtoService;
use JMS\Serializer\SerializerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use OpenApi\Attributes as OA;

/**
 * Class PhrasesController
 * Всё для работы с базой данных по фразам, метрики-фразы, добавление\редактирование
 * @package App\Controller\Api
 */
#[Route('/api/phrase', name: 'phrase_')]
class PhrasesController extends AbstractController
{
    /**
     * List of all phrases, filtered by modules and tags
     * Список всех фраз, с фильтрацией по модулям и тегам
     *
     * @param Request $request
     * @param PharasesLibrarianService $pharasesService
     * @param SerializerInterface $serializer
     * @param ValidationDtoService $validateService
     * @return PhraseList
     * @throws ApiValidationFieldsException
     */
    #[Route('/list', name: 'list', methods: ["POST"])]
    #[OA\RequestBody(
        description: 'GetPhraseList',
        content: new Model(type: GetPhraseList::class)
    )]
    #[OA\Response(
        response: 200,
        description: 'PhraseList',
        content: new Model(type: PhraseList::class)
    )]
    #[OA\Tag(name: 'Phrase')]
    #[Security(name: 'Bearer')]
    public function list(Request $request, PharasesLibrarianService $pharasesService, SerializerInterface $serializer, ValidationDtoService $validateService):PhraseList
    {
        /** @var GetPhraseList $input */
        $input = $serializer->deserialize($request->getContent(), GetPhraseList::class, 'json');
        $validateService->validate($input);
        return $pharasesService->getList($input);
    }

    /**
     * Getting a single phrase together with the history change and pagination by history
     * Получение одной фразы вместе с историей изменение и пагинацией по истории
     *
     * @param Request $request
     * @param PharasesLibrarianService $pharasesService
     * @param SerializerInterface $serializer
     * @param ValidationDtoService $validateService
     * @return PhraseDto
     * @throws ApiValidationFieldsException|PhraseNotFoundException
     */
    #[Route('/get-one', name: 'get-one', methods: ["POST"])]
    #[OA\RequestBody(
        description: 'GetOne',
        content: new Model(type: GetOne::class)
    )]
    #[OA\Response(
        response: 200,
        description: 'Get one',
        content: new Model(type: PhraseDto::class)
    )]
    #[OA\Tag(name: 'Phrase')]
    #[Security(name: 'Bearer')]
    public function getOne(Request $request, PharasesLibrarianService $pharasesService, SerializerInterface $serializer, ValidationDtoService $validateService): PhraseDto
    {
        /** @var GetOne $input */
        $input = $serializer->deserialize($request->getContent(), GetOne::class, 'json');
        $validateService->validate($input);
        return $pharasesService->getOne($input);
    }

    /**
     * Add new phrase
     * Добавление новой фразы
     *
     * @param Request $request
     * @param PharasesLibrarianService $pharasesService
     * @param SerializerInterface $serializer
     * @param ValidationDtoService $validateService
     * @return PhraseDto
     * @throws NotHaveDiffForUpdate|PhraseIsExistException|ApiValidationFieldsException
     */
    #[Route('/new', name: 'new', methods: ["POST"])]
    #[OA\RequestBody(
        description: 'PhraseDto',
        content: new Model(type: PhraseDto::class)
    )]
    #[OA\Response(
        response: 200,
        description: 'PhraseDto',
        content: new Model(type: PhraseDto::class)
    )]
    #[OA\Tag(name: 'Phrase')]
    #[Security(name: 'Bearer')]
    public function new(Request $request, PharasesLibrarianService $pharasesService, SerializerInterface $serializer, ValidationDtoService $validateService): PhraseDto
    {
        /** @var PhraseDto $input */
        $input = $serializer->deserialize($request->getContent(), PhraseDto::class, 'json');
        $validateService->validate($input);
        return $pharasesService->addNew($input);
    }

    /**
     * Updating an existing phrase in the database
     * Обновление существующей фразы в базе
     *
     * @param Request $request
     * @param PharasesLibrarianService $pharasesService
     * @param SerializerInterface $serializer
     * @param ValidationDtoService $validateService
     * @return PhraseDto
     * @throws ApiValidationFieldsException|PhraseNotFoundException|NotHaveDiffForUpdate
     */
    #[Route('/update', name: 'update', methods: ["POST"])]
    #[OA\RequestBody(
        description: 'PhraseDto',
        content: new Model(type: PhraseDto::class)
    )]
    #[OA\Response(
        response: 200,
        description: 'PhraseDto',
        content: new Model(type: PhraseDto::class)
    )]
    #[OA\Tag(name: 'Phrase')]
    #[Security(name: 'Bearer')]
    public function update(Request $request, PharasesLibrarianService $pharasesService, SerializerInterface $serializer, ValidationDtoService $validateService): PhraseDto
    {
        /** @var PhraseDtoUpdate $input */
        $input = $serializer->deserialize($request->getContent(), PhraseDtoUpdate::class, 'json');
        $validateService->validate($input);
        return $pharasesService->update($input);
    }

    /**
     * Rollback of a phrase in one of its past states
     * Откат фразы на одно из прошлых её состояний
     *
     * @param Request $request
     * @param PharasesLibrarianService $pharasesService
     * @param SerializerInterface $serializer
     * @param ValidationDtoService $validateService
     * @return PhraseDto
     * @throws PhraseNotFoundException|NotHaveDiffForUpdate|PhraseHistoryNotFoundException|ImpossibleRevertPhaseAtHistory|ApiValidationFieldsException
     */
    #[Route('/revert-at-history', name: 'revert-at-history', methods: ["POST"])]
    #[OA\RequestBody(
        description: 'GetPhraseHistory',
        content: new Model(type: GetPhraseHistory::class)
    )]
    #[OA\Response(
        response: 200,
        description: 'Revert at history',
        content: new Model(type: PhraseDto::class)
    )]
    #[OA\Tag(name: 'Phrase')]
    #[Security(name: 'Bearer')]
    public function revertAtHistory(Request $request, PharasesLibrarianService $pharasesService, SerializerInterface $serializer, ValidationDtoService $validateService): PhraseDto
    {
        /** @var GetPhraseHistory $input */
        $input = $serializer->deserialize($request->getContent(), GetPhraseHistory::class, 'json');
        $validateService->validate($input);
        return $pharasesService->revertAtHistory($input);
    }
}