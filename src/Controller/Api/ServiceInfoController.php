<?php


namespace App\Controller\Api;


use App\Dto\Api\BaseList;
use App\Dto\Api\Service\ServiceList;
use App\Dto\Api\Service\Response\ServiceList as ResponseServiceList;
use App\Exceptions\ApiValidationFieldsException;
use App\Exceptions\Core\ScenarioNotFound;
use App\Exceptions\Core\SolverUnitsNotFound;
use App\Service\InfoService;
use App\Service\ValidationDtoService;
use JMS\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use OpenApi\Attributes as OA;

/**
 * Class ServiceInfoController
 * Справочная информация
 * @package App\Controller\Api
 */
#[Route('/api/service-info', name: 'service-info_')]
class ServiceInfoController extends AbstractController
{
    /**
     * List of available sites for work
     * Список доступных площадок для работы
     *
     * @param Request $request
     * @param SerializerInterface $serializer
     * @param InfoService $infoService
     * @param ValidationDtoService $validateService
     * @return ResponseServiceList
     * @throws ApiValidationFieldsException
     */
    #[Route('/integration-list', name: 'integration-list', methods: ["POST"])]
    #[OA\RequestBody(
        description: 'ServiceList',
        content: new Model(type: ServiceList::class)
    )]
    #[OA\Response(
        response: 200,
        description: 'Integration list',
        content: new Model(type: ResponseServiceList::class)
    )]
    #[OA\Tag(name: 'ServiceInfo')]
    #[Security(name: 'Bearer')]
    public function getIntegrationList(Request $request, SerializerInterface $serializer, InfoService $infoService,  ValidationDtoService $validateService): ResponseServiceList
    {
        /** @var ServiceList $input */
        $input = $serializer->deserialize($request->getContent(), ServiceList::class, 'json');
        $validateService->validate($input);
        return $infoService->getIntegrationList($input);
    }

    /**
     * List of response generator modules
     * Список модулей-генераторов ответа
     *
     * @param Request $request
     * @param SerializerInterface $serializer
     * @param InfoService $infoService
     * @param ValidationDtoService $validateService
     * @return BaseList
     * @throws ApiValidationFieldsException|SolverUnitsNotFound
     */
    #[Route('/module-list', name: 'module-list', methods: ["POST"])]
    #[OA\RequestBody(
        description: 'ServiceList',
        content: new Model(type: ServiceList::class)
    )]
    #[OA\Response(
        response: 200,
        description: 'Module list',
        content: new Model(type: ResponseServiceList::class)
    )]
    #[OA\Tag(name: 'ServiceInfo')]
    #[Security(name: 'Bearer')]
    public function getModuleList(Request $request, SerializerInterface $serializer, InfoService $infoService,  ValidationDtoService $validateService): BaseList
    {
        /** @var ServiceList $input */
        $input = $serializer->deserialize($request->getContent(), ServiceList::class, 'json');
        $validateService->validate($input);
        return $infoService->getModuleList($input);
    }

    /**
     * List of behavioral scenarios
     * Список поведенческих сценариев
     *
     * @param Request $request
     * @param SerializerInterface $serializer
     * @param InfoService $infoService
     * @param ValidationDtoService $validateService
     * @return BaseList
     * @throws ApiValidationFieldsException|ScenarioNotFound
     */
    #[Route('/scenarios-list', name: 'scenarios-list', methods: ["POST"])]
    #[OA\RequestBody(
        description: 'ServiceList',
        content: new Model(type: ServiceList::class)
    )]
    #[OA\Response(
        response: 200,
        description: 'Scenarios list',
        content: new Model(type: ResponseServiceList::class)
    )]
    #[OA\Tag(name: 'ServiceInfo')]
    #[Security(name: 'Bearer')]
    public function getScenariosList(Request $request, SerializerInterface $serializer, InfoService $infoService,  ValidationDtoService $validateService): BaseList
    {
        /** @var ServiceList $input */
        $input = $serializer->deserialize($request->getContent(), ServiceList::class, 'json');
        $validateService->validate($input);
        return $infoService->getScenariosList($input);
    }

}