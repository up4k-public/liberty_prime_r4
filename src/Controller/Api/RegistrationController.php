<?php

namespace App\Controller\Api;

use App\Dto\Api\User\Response\RegisterNew;
use App\Dto\Api\Register\RegisterNewUser;
use App\Dto\Api\User\Response\RegisterNew as RegisterNewDtoResponse;
use App\Exceptions\ApiValidationFieldsException;
use App\Exceptions\User\InvalidInviteException;
use App\Exceptions\User\InviteHasBeenUsedException;
use App\Exceptions\User\UserAlreadyExistException;
use App\Service\UserService;
use App\Service\ValidationDtoService;
use JMS\Serializer\SerializerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use OpenApi\Attributes as OA;

/**
 * Class RegistrationController
 * @package App\Controller\Api
 */
#[Route('/api', name: 'register_')]
class RegistrationController extends AbstractController
{
    /**
     *
     * Регистрация нового пользователя по инвайту
     *
     * @param Request $request
     * @param SerializerInterface $serializer
     * @param UserService $userService
     * @param ValidationDtoService $validateService
     * @return RegisterNew
     * @throws ApiValidationFieldsException
     * @throws InvalidInviteException
     * @throws InviteHasBeenUsedException
     * @throws UserAlreadyExistException
     */
    #[Route('/register', name: 'register', methods: ["POST"])]
    #[OA\RequestBody(
        description: 'RegisterNewUser',
        content: new Model(type: RegisterNewUser::class)
    )]
    #[OA\Response(
        response: 200,
        description: 'RegisterNewDtoResponse',
        content: new Model(type: RegisterNewDtoResponse::class)
    )]
    #[OA\Tag(name: 'Register')]
    public function index(Request $request, SerializerInterface $serializer, UserService $userService, ValidationDtoService $validateService): RegisterNewDtoResponse
    {
        /** @var RegisterNewUser $input */
        $input = $serializer->deserialize($request->getContent(), RegisterNewUser::class, 'json');
        $validateService->validate($input);
        return $userService->registerNew($input);
    }
}