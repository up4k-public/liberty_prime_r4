<?php


namespace App\Controller\Api;

use App\Dto\Api\Assault\Request\AssaultList;
use App\Dto\Api\Assault\Request\CreateAssault;
use App\Dto\Api\Assault\Request\GetOneAssault;
use App\Dto\Api\Assault\Request\UpdateAssault;
use App\Dto\Api\Assault\Response\Assault;
use App\Dto\Api\Assault\Response\Assault as AssaultDto;
use App\Dto\Api\Assault\Response\AssaultList as AssaultListResponse;
use App\Exceptions\ApiValidationFieldsException;
use App\Exceptions\Assault\AssaultNotFound;
use App\Exceptions\User\AccessDeniedToCartrige;
use App\Exceptions\User\NotFoundCartrigeById;
use App\Service\AssaultService;
use App\Service\ValidationDtoService;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use JMS\Serializer\SerializerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use OpenApi\Attributes as OA;

/**
 * Управление отаке. Наведение бота на цель, приостановка, смена поведения, лог процесса
 * Class AssaultController
 * @package App\Controller\Api
 */
#[Route('/api/assault', name: 'assault_')]
class AssaultController extends AbstractController
{
    /**
     * List assaults
     * Список отаке
     *
     * @param Request $request
     * @param AssaultService $assaultService
     * @param SerializerInterface $serializer
     * @param ValidationDtoService $validateService
     * @return AssaultListResponse
     * @throws NoResultException|NonUniqueResultException|ApiValidationFieldsException
     */
    #[Route('/list', name: 'list', methods: ["POST"])]
    #[OA\RequestBody(
        description: 'CreateAssault',
        content: new Model(type: AssaultList::class)
    )]
    #[OA\Response(
        response: 200,
        description: 'AssaultListResponse',
        content: new Model(type: AssaultListResponse::class)
    )]
    #[OA\Tag(name: 'Assault')]
    #[Security(name: 'Bearer')]
    public function assaultList(Request $request, AssaultService $assaultService, SerializerInterface $serializer, ValidationDtoService $validateService):AssaultListResponse
    {
        /** @var AssaultList $input */
        $input = $serializer->deserialize($request->getContent(), AssaultList::class, 'json');
        $validateService->validate($input);
        return $assaultService->list($input);
    }

    /**
     * Create assault
     * Создать отаке
     *
     * @param Request $request
     * @param AssaultService $assaultService
     * @param SerializerInterface $serializer
     * @param ValidationDtoService $validateService
     * @return Assault
     * @throws AccessDeniedToCartrige|NotFoundCartrigeById|ApiValidationFieldsException
     */
    #[Route('/create', name: 'create', methods: ["POST"])]
    #[OA\RequestBody(
        description: 'CreateAssault',
        content: new Model(type: CreateAssault::class)
    )]
    #[OA\Response(
        response: 200,
        description: 'Create',
        content: new Model(type: AssaultDto::class)
    )]
    #[OA\Tag(name: 'Assault')]
    #[Security(name: 'Bearer')]
    public function new(Request $request, AssaultService $assaultService, SerializerInterface $serializer, ValidationDtoService $validateService): AssaultDto
    {
        /** @var CreateAssault $input */
        $input = $serializer->deserialize($request->getContent(), CreateAssault::class, 'json');
        $validateService->validate($input);
        return $assaultService->createNew($input);
    }

    /**
     * Update assault
     * Обновить отаке
     *
     * @param Request $request
     * @param AssaultService $assaultService
     * @param SerializerInterface $serializer
     * @param ValidationDtoService $validateService
     * @return AssaultDto
     * @throws AssaultNotFound|NotFoundCartrigeById|ApiValidationFieldsException
     */
    #[Route('/update', name: 'update', methods: ["POST"])]
    #[OA\RequestBody(
        description: 'UpdateAssault',
        content: new Model(type: UpdateAssault::class)
    )]
    #[OA\Response(
        response: 200,
        description: 'Update',
        content: new Model(type: AssaultDto::class)
    )]
    #[OA\Tag(name: 'Assault')]
    #[Security(name: 'Bearer')]
    public function update(Request $request, AssaultService $assaultService, SerializerInterface $serializer, ValidationDtoService $validateService): AssaultDto
    {
        /** @var UpdateAssault $input */
        $input = $serializer->deserialize($request->getContent(), UpdateAssault::class, 'json');
        $validateService->validate($input);
        return $assaultService->update($input);
    }

    /**
     * Change assault status to stop
     * Сменить статус отаке на stop
     *
     * @param Request $request
     * @param AssaultService $assaultService
     * @param SerializerInterface $serializer
     * @param ValidationDtoService $validateService
     * @return AssaultDto
     * @throws ApiValidationFieldsException|AssaultNotFound
     */
    #[Route('/stop', name: 'stop', methods: ["POST"])]
    #[OA\RequestBody(
        description: 'GetOneAssault',
        content: new Model(type: GetOneAssault::class)
    )]
    #[OA\Response(
        response: 200,
        description: 'AssaultDto',
        content: new Model(type: AssaultDto::class)
    )]
    #[OA\Tag(name: 'Assault')]
    #[Security(name: 'Bearer')]
    public function stop(Request $request, AssaultService $assaultService, SerializerInterface $serializer, ValidationDtoService $validateService): AssaultDto
    {
        /** @var GetOneAssault $input */
        $input = $serializer->deserialize($request->getContent(), GetOneAssault::class, 'json');
        $validateService->validate($input);
        return $assaultService->stop($input);
    }

    /**
     * Change assault status to new
     * Сменить статус отаке на new
     *
     * @param Request $request
     * @param AssaultService $assaultService
     * @param SerializerInterface $serializer
     * @param ValidationDtoService $validateService
     * @return AssaultDto
     * @throws ApiValidationFieldsException|AssaultNotFound
     */
    #[Route('/restart', name: 'restart', methods: ["POST"])]
    #[OA\RequestBody(
        description: 'GetOneAssault',
        content: new Model(type: GetOneAssault::class)
    )]
    #[OA\Response(
        response: 200,
        description: 'AssaultDto',
        content: new Model(type: AssaultDto::class)
    )]
    #[OA\Tag(name: 'Assault')]
    #[Security(name: 'Bearer')]
    public function restart(Request $request, AssaultService $assaultService, SerializerInterface $serializer, ValidationDtoService $validateService): AssaultDto
    {
        /** @var GetOneAssault $input */
        $input = $serializer->deserialize($request->getContent(), GetOneAssault::class, 'json');
        $validateService->validate($input);
        return $assaultService->restart($input);
    }

    /**
     * Get assault log
     * Получить лог отаке
     * @param Request $request
     * @param AssaultService $assaultService
     * @param SerializerInterface $serializer
     * @return JsonResponse
     */
    #[Route('/log', name: 'log', methods: ["POST"])]
    #[OA\Tag(name: 'Assault')]
    #[Security(name: 'Bearer')]
    public function log(Request $request, AssaultService $assaultService, SerializerInterface $serializer): JsonResponse
    {

    }

}