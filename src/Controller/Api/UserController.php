<?php


namespace App\Controller\Api;

use App\Dto\Api\User\Request\CartrigeList;
use App\Dto\Api\User\Response\CartrigeList as ResponseCartrigeList;
use App\Dto\Api\User\Request\UpdateCartrige as InputUpdateCartrigeDto;
use App\Dto\Api\User\Response\GenerateInvite;
use App\Dto\Api\User\Request\AddCartrige as InputAddCartrige;
use App\Dto\Api\User\Response\AddCartrige as ResponseAddCartrige;
use App\Dto\Api\User\Request\InviteList;
use App\Dto\Api\User\Request\UserList;
use App\Dto\Api\User\Response\InviteList as ResponseInviteList;
use App\Dto\Api\User\Response\UserList as ResponseUserList;
use App\Exceptions\ApiValidationFieldsException;
use App\Exceptions\Core\BaseCoreException;
use App\Exceptions\Integration\InvalidAuthData;
use App\Exceptions\User\AttemptToUpdateAnotherAccount;
use App\Exceptions\User\CartrigeIsExistException;
use App\Exceptions\User\NotFoundCartrigeById;
use App\Service\Cartrige\CartrigeService;
use App\Service\Cartrige\CreateCartrigeChecker;
use App\Service\Cartrige\UpdateCartrigeChecker;
use App\Service\UserService;
use App\Service\ValidationDtoService;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use JMS\Serializer\SerializerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use OpenApi\Attributes as OA;

/**
 * Class UserController
 * @package App\Controller
 */
#[Route('/api/user', name: 'user_')]
class UserController extends AbstractController
{
    /**
     * Generate invite
     * Сгенерировать инвайт
     *
     * @param Request $request
     * @param UserService $userService
     * @return GenerateInvite
     */
    #[Route('/generate-invite', name: 'generate-list', methods: ["GET"])]
    #[OA\Response(
        response: 200,
        description: 'generate-invite',
        content: new Model(type: GenerateInvite::class)
    )]
    #[Security(name: 'Bearer')]
    #[OA\Tag(name: 'User')]
    public function generateInvite(Request $request, UserService $userService): GenerateInvite
    {
        return $userService->generateInvite();
    }


    /**
     * List of available cartrige
     * Список доступных ботэ
     * @param Request $request
     * @param CartrigeService $cartrigeService
     * @param SerializerInterface $serializer
     * @param ValidationDtoService $validateService
     * @return ResponseCartrigeList
     * @throws ApiValidationFieldsException
     */
    #[Route('/cartrige-list', name: 'cartrige-list', methods: ["POST"])]
    #[OA\RequestBody(
        description: 'CartrigeList',
        content: new Model(type: CartrigeList::class)
    )]
    #[OA\Response(
        response: 200,
        description: 'CartrigeListResponse',
        content: new Model(type: ResponseCartrigeList::class)
    )]
    #[Security(name: 'Bearer')]
    #[OA\Tag(name: 'User')]
    public function cartrigeList(Request $request, CartrigeService $cartrigeService, SerializerInterface $serializer, ValidationDtoService $validateService):ResponseCartrigeList
    {
        /** @var CartrigeList $input */
        $input = $serializer->deserialize($request->getContent(), CartrigeList::class, 'json');
        $validateService->validate($input);
        return $cartrigeService->cartrigeList($input);
    }

    /**
     * User List
     * Список воене
     *
     * @param Request $request
     * @param UserService $userService
     * @param SerializerInterface $serializer
     * @param ValidationDtoService $validateService
     * @return ResponseUserList
     * @throws NoResultException|NonUniqueResultException|ApiValidationFieldsException
     */
    #[Route('/list', name: 'list', methods: ["POST"])]
    #[OA\RequestBody(
        description: 'UserList',
        content: new Model(type: UserList::class)
    )]
    #[OA\Response(
        response: 200,
        description: 'ResponseUserList',
        content: new Model(type: ResponseUserList::class)
    )]
    #[OA\Tag(name: 'User')]
    #[Security(name: 'Bearer')]
    public function list(Request $request, UserService $userService, SerializerInterface $serializer, ValidationDtoService $validateService): ResponseUserList
    {
        /** @var UserList $input */
        $input = $serializer->deserialize($request->getContent(), UserList::class, 'json');
        $validateService->validate($input);
        return $userService->list($input);
    }

    /**
     * Add new cartrige
     * Добавление нового ботэ
     *
     * @param Request $request
     * @param CartrigeService $cartrigeService
     * @param SerializerInterface $serializer
     * @param ValidationDtoService $validateService
     * @param CreateCartrigeChecker $checker
     * @return ResponseAddCartrige
     * @throws NotFoundCartrigeById|InvalidAuthData|CartrigeIsExistException|BaseCoreException|AttemptToUpdateAnotherAccount|ApiValidationFieldsException
     */
    #[Route('/new-cartrige', name: 'new-cartrige', methods: ["POST"])]
    #[OA\RequestBody(
        description: 'InputAddCartrige',
        content: new Model(type: InputAddCartrige::class)
    )]
    #[OA\Response(
        response: 200,
        description: 'Cartrige list',
        content: new Model(type: ResponseAddCartrige::class)
    )]
    #[OA\Tag(name: 'User')]
    #[Security(name: 'Bearer')]
    public function newCartrige(Request $request, CartrigeService $cartrigeService, SerializerInterface $serializer, ValidationDtoService $validateService, CreateCartrigeChecker $checker): ResponseAddCartrige
    {
        /** @var InputAddCartrige $input */
        $input = $serializer->deserialize($request->getContent(), InputAddCartrige::class, 'json');
        $validateService->validate($input);
        return $cartrigeService->addCartrige($input, $checker);
    }

    /**
     * Update Cartrige
     * Обновление ботэ
     *
     * @param Request $request
     * @param CartrigeService $cartrigeService
     * @param SerializerInterface $serializer
     * @param ValidationDtoService $validateService
     * @param UpdateCartrigeChecker $checker
     * @return ResponseAddCartrige
     * @throws ApiValidationFieldsException|AttemptToUpdateAnotherAccount|BaseCoreException|CartrigeIsExistException|InvalidAuthData|NotFoundCartrigeById
     */
    #[Route('/update-cartrige', name: 'update-cartrige', methods: ["POST"])]
    #[OA\Response(
        response: 200,
        description: 'Cartrige list',
        content: new Model(type: ResponseAddCartrige::class)
    )]
    #[OA\Tag(name: 'User')]
    #[Security(name: 'Bearer')]
    public function updateCartrige(Request $request, CartrigeService $cartrigeService, SerializerInterface $serializer, ValidationDtoService $validateService, UpdateCartrigeChecker $checker): ResponseAddCartrige
    {
        /** @var InputUpdateCartrigeDto $input */
        $input = $serializer->deserialize($request->getContent(), InputUpdateCartrigeDto::class, 'json');
        $validateService->validate($input);
        return $cartrigeService->addCartrige($input, $checker);
    }

    /**
     * Invite List
     * Список инвайтов
     *
     * @param Request $request
     * @param UserService $userService
     * @param SerializerInterface $serializer
     * @param ValidationDtoService $validateService
     * @return ResponseInviteList
     * @throws ApiValidationFieldsException
     */
    #[Route('/invite-list', name: 'invite-list', methods: ["POST"])]
    #[OA\Response(
        response: 200,
        description: 'Invite list',
        content: new Model(type: ResponseInviteList::class)
    )]
    #[OA\Tag(name: 'User')]
    #[Security(name: 'Bearer')]
    public function inviteList(Request $request, UserService $userService, SerializerInterface $serializer, ValidationDtoService $validateService): ResponseInviteList
    {
        /** @var InviteList $input */
        $input = $serializer->deserialize($request->getContent(), InviteList::class, 'json');
        $validateService->validate($input);
        return $userService->getInviteList($input);
    }

}