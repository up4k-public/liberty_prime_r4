<?php


namespace App\Controller\Api;

use App\Dto\Api\Auth\Request\GetToken as RequestGetToke;
use App\Dto\Api\Auth\Request\RefreshToken;
use App\Dto\Api\Auth\Response\GetToken as ResponseGetToken;
use App\Service\AuthService;
use App\Service\ValidationDtoService;
use JMS\Serializer\SerializerInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Attributes as OA;

/**
 * Class UserController
 * @package App\Controller
 */
#[Route('/api/auth', name: 'auth_')]
class AuthController  extends AbstractController
{
    #[Route('/get-token', name: 'get-token', methods: ["POST"])]
    #[OA\RequestBody(
        description: 'GetToken',
        content: new Model(type: RequestGetToke::class)
    )]
    #[OA\Response(
        response: 200,
        description: 'get-token',
        content: new Model(type: ResponseGetToken::class)
    )]
    #[OA\Tag(name: 'Auth')]
    public function getToken(Request $request, AuthService $authService, SerializerInterface $serializer, ValidationDtoService $validateService)
    {
        /** @var RequestGetToke $input */
        $input = $serializer->deserialize($request->getContent(), RequestGetToke::class, 'json');
        $validateService->validate($input);
        return $authService->redirect($_SERVER['SERVER_NAME'] . '/api/login_check',
            $request->isMethod('POST'), $request->getContent(), $request->headers->all(), ResponseGetToken::class);
    }

    #[Route('/logout', name: 'invalidate', methods: ["GET"])]
    #[OA\Response(
        response: 200,
        description: 'logout',
    )]
    #[OA\Tag(name: 'Auth')]
    #[Security(name: 'Bearer')]
    public function logout(Request $request, AuthService $authService, SerializerInterface $serializer, ValidationDtoService $validateService)
    {
        return $authService->redirect($_SERVER['SERVER_NAME'] . '/api/logout',
            $request->isMethod('POST'), $request->getContent(), $request->headers->all());
    }

    #[Route('/refresh-token', name: 'logout', methods: ["POST"])]
    #[OA\RequestBody(
        description: 'GetToken',
        content: new Model(type: RefreshToken::class)
    )]
    #[OA\Response(
        response: 200,
        description: 'refresh-token',
        content: new Model(type: ResponseGetToken::class)
    )]
    #[OA\Tag(name: 'Auth')]
    public function refreshToken(Request $request, AuthService $authService, SerializerInterface $serializer, ValidationDtoService $validateService)
    {
        /** @var RefreshToken $input */
        $input = $serializer->deserialize($request->getContent(), RefreshToken::class, 'json');
        $validateService->validate($input);
        return $authService->redirect($_SERVER['SERVER_NAME'] . '/api/token/refresh',
            $request->isMethod('POST'), $request->getContent(), $request->headers->all(), ResponseGetToken::class);
    }
}